#include <catch2/catch.hpp>
#include "../server/game.h"
#include "../server/initializer.h"
#include "../server/player.h"
#include "../server/card.h"
#include "../server/cardterritoryconquer.h"
#include "../server/settings.h"
#include "../server/territory.h"
#include "../server/missioncard.h"
#include "../server/continent.h"
#include "../server/cardcontinentconquer.h"
#include "../server/carddestroytroops.h"

TEST_CASE("Exchange cards and Territory conquer mission", "[exchange][conquer]")
{
    Player *p1 = new Player("Player1", Color::RED, true);
    Player *p2 = new Player("Player2", Color::BLUE, false);

    QVector<Player*> players = {p1,p2};

    Goal gg = Goal::CARDS;

    Settings* s = new Settings(2, 5, gg, players);
    auto continents = Initializer::initializeContinents();
    auto cards = Initializer::initializeCards();

    auto mcard1 = new CardTerritoryConquer(":/resources/images/MissionCards/Conquer-18",18);
    auto mcard2 = new CardTerritoryConquer(":/resources/images/MissionCards/Conquer-24",24);

    p1->setMissionCard(mcard1);
    p2->setMissionCard(mcard2);

    Game *game = new Game(cards, continents, s);
    game->start();

    SECTION("Exchanging 3 infantry cards without owned territories, should return 4"){
        //arrange
        Card c1(Initializer::allTerritories["Alaska"], CardType::INFANTRY);
        Card c2(Initializer::allTerritories["Brazil"], CardType::INFANTRY);
        Card c3(Initializer::allTerritories["Peru"], CardType::INFANTRY);

        if(Initializer::allTerritories["Alaska"]->owner() == p1)
            p1->removeTerritory(Initializer::allTerritories["Alaska"]);
        if(Initializer::allTerritories["Brazil"]->owner() == p1)
            p1->removeTerritory(Initializer::allTerritories["Brazil"]);
        if(Initializer::allTerritories["Peru"]->owner() == p1)
            p1->removeTerritory(Initializer::allTerritories["Peru"]);

        p1->addCard(&c1);
        p1->addCard(&c2);
        p1->addCard(&c3);

        int expected_val = 4;

        //act
        int ret_val = p1->exchangeCards(&c1, &c2, &c3);

        //assert
        REQUIRE(expected_val == ret_val);
        if(Initializer::allTerritories["Alaska"]->owner() == p1)
            p1->addTerritory(Initializer::allTerritories["Alaska"]);
        if(Initializer::allTerritories["Brazil"]->owner() == p1)
            p1->addTerritory(Initializer::allTerritories["Brazil"]);
        if(Initializer::allTerritories["Peru"]->owner() == p1)
            p1->addTerritory(Initializer::allTerritories["Peru"]);
        p1->cards().remove(&c1);
        p1->cards().remove(&c2);
        p1->cards().remove(&c3);
    }
    SECTION("Exchanging 3 infantry cards with 1 territory owned, should return 5"){
        //arrange
        Card c1(Initializer::allTerritories["Alaska"], CardType::INFANTRY);
        Card c2(Initializer::allTerritories["Brazil"], CardType::INFANTRY);
        Card c3(Initializer::allTerritories["Peru"], CardType::INFANTRY);

        if(Initializer::allTerritories["Alaska"]->owner() != p1)
            p1->addTerritory(Initializer::allTerritories["Alaska"]);
        if(Initializer::allTerritories["Brazil"]->owner() == p1)
            p1->removeTerritory(Initializer::allTerritories["Brazil"]);
        if(Initializer::allTerritories["Peru"]->owner() == p1)
            p1->removeTerritory(Initializer::allTerritories["Peru"]);

        p1->addCard(&c1);
        p1->addCard(&c2);
        p1->addCard(&c3);

        int expected_val = 5;

        //act
        int ret_val = p1->exchangeCards(&c1, &c2, &c3);

        //assert
        REQUIRE(expected_val == ret_val);
        if(Initializer::allTerritories["Alaska"]->owner() != p1)
            p1->removeTerritory(Initializer::allTerritories["Alaska"]);
        if(Initializer::allTerritories["Brazil"]->owner() == p1)
            p1->addTerritory(Initializer::allTerritories["Brazil"]);
        if(Initializer::allTerritories["Peru"]->owner() == p1)
            p1->addTerritory(Initializer::allTerritories["Peru"]);
        p1->cards().remove(&c1);
        p1->cards().remove(&c2);
        p1->cards().remove(&c3);
    }
    SECTION("Exchanging 3 infantry cards with 2 territories owned, should return 6"){
        //arrange
        Card c1(Initializer::allTerritories["Alaska"], CardType::INFANTRY);
        Card c2(Initializer::allTerritories["Brazil"], CardType::INFANTRY);
        Card c3(Initializer::allTerritories["Peru"], CardType::INFANTRY);

        if(Initializer::allTerritories["Alaska"]->owner() != p1)
            p1->addTerritory(Initializer::allTerritories["Alaska"]);
        if(Initializer::allTerritories["Brazil"]->owner() != p1)
            p1->addTerritory(Initializer::allTerritories["Brazil"]);
        if(Initializer::allTerritories["Peru"]->owner() == p1)
            p1->removeTerritory(Initializer::allTerritories["Peru"]);

        p1->addCard(&c1);
        p1->addCard(&c2);
        p1->addCard(&c3);

        int expected_val = 6;

        //act
        int ret_val = p1->exchangeCards(&c1, &c2, &c3);

        //assert
        REQUIRE(expected_val == ret_val);
        if(Initializer::allTerritories["Alaska"]->owner() != p1)
            p1->removeTerritory(Initializer::allTerritories["Alaska"]);
        if(Initializer::allTerritories["Brazil"]->owner() != p1)
            p1->removeTerritory(Initializer::allTerritories["Brazil"]);
        if(Initializer::allTerritories["Peru"]->owner() == p1)
            p1->addTerritory(Initializer::allTerritories["Peru"]);
        p1->cards().remove(&c1);
        p1->cards().remove(&c2);
        p1->cards().remove(&c3);
    }
    SECTION("Exchanging 3 infantry cards with 3 territories owned, should return 7"){
        //arrange
        Card c1(Initializer::allTerritories["Alaska"], CardType::INFANTRY);
        Card c2(Initializer::allTerritories["Brazil"], CardType::INFANTRY);
        Card c3(Initializer::allTerritories["Peru"], CardType::INFANTRY);

        if(Initializer::allTerritories["Alaska"]->owner() != p1)
            p1->addTerritory(Initializer::allTerritories["Alaska"]);
        if(Initializer::allTerritories["Brazil"]->owner() != p1)
            p1->addTerritory(Initializer::allTerritories["Brazil"]);
        if(Initializer::allTerritories["Peru"]->owner() != p1)
            p1->addTerritory(Initializer::allTerritories["Peru"]);

        p1->addCard(&c1);
        p1->addCard(&c2);
        p1->addCard(&c3);

        int expected_val = 7;

        //act
        int ret_val = p1->exchangeCards(&c1, &c2, &c3);

        //assert
        REQUIRE(expected_val == ret_val);
        if(Initializer::allTerritories["Alaska"]->owner() != p1)
            p1->removeTerritory(Initializer::allTerritories["Alaska"]);
        if(Initializer::allTerritories["Brazil"]->owner() != p1)
            p1->removeTerritory(Initializer::allTerritories["Brazil"]);
        if(Initializer::allTerritories["Peru"]->owner() != p1)
            p1->removeTerritory(Initializer::allTerritories["Peru"]);
        p1->cards().remove(&c1);
        p1->cards().remove(&c2);
        p1->cards().remove(&c3);
    }

    SECTION("Exchanging 3 cavalry cards without owned territories, should return 6"){
        //arrange
        Card c1(Initializer::allTerritories["Alaska"], CardType::CAVALRY);
        Card c2(Initializer::allTerritories["Brazil"], CardType::CAVALRY);
        Card c3(Initializer::allTerritories["Peru"], CardType::CAVALRY);

        if(Initializer::allTerritories["Alaska"]->owner() == p1)
            p1->removeTerritory(Initializer::allTerritories["Alaska"]);
        if(Initializer::allTerritories["Brazil"]->owner() == p1)
            p1->removeTerritory(Initializer::allTerritories["Brazil"]);
        if(Initializer::allTerritories["Peru"]->owner() == p1)
            p1->removeTerritory(Initializer::allTerritories["Peru"]);

        p1->addCard(&c1);
        p1->addCard(&c2);
        p1->addCard(&c3);

        int expected_val = 6;

        //act
        int ret_val = p1->exchangeCards(&c1, &c2, &c3);

        //assert
        REQUIRE(expected_val == ret_val);
        if(Initializer::allTerritories["Alaska"]->owner() == p1)
            p1->addTerritory(Initializer::allTerritories["Alaska"]);
        if(Initializer::allTerritories["Brazil"]->owner() == p1)
            p1->addTerritory(Initializer::allTerritories["Brazil"]);
        if(Initializer::allTerritories["Peru"]->owner() == p1)
            p1->addTerritory(Initializer::allTerritories["Peru"]);
        p1->cards().remove(&c1);
        p1->cards().remove(&c2);
        p1->cards().remove(&c3);
    }
    SECTION("Exchanging 3 cavalry cards with 1 territory owned, should return 7"){
        //arrange
        Card c1(Initializer::allTerritories["Alaska"], CardType::CAVALRY);
        Card c2(Initializer::allTerritories["Brazil"], CardType::CAVALRY);
        Card c3(Initializer::allTerritories["Peru"], CardType::CAVALRY);

        if(Initializer::allTerritories["Alaska"]->owner() != p1)
            p1->addTerritory(Initializer::allTerritories["Alaska"]);
        if(Initializer::allTerritories["Brazil"]->owner() == p1)
            p1->removeTerritory(Initializer::allTerritories["Brazil"]);
        if(Initializer::allTerritories["Peru"]->owner() == p1)
            p1->removeTerritory(Initializer::allTerritories["Peru"]);

        p1->addCard(&c1);
        p1->addCard(&c2);
        p1->addCard(&c3);

        int expected_val = 7;

        //act
        int ret_val = p1->exchangeCards(&c1, &c2, &c3);

        //assert
        REQUIRE(expected_val == ret_val);
        if(Initializer::allTerritories["Alaska"]->owner() != p1)
            p1->removeTerritory(Initializer::allTerritories["Alaska"]);
        if(Initializer::allTerritories["Brazil"]->owner() == p1)
            p1->addTerritory(Initializer::allTerritories["Brazil"]);
        if(Initializer::allTerritories["Peru"]->owner() == p1)
            p1->addTerritory(Initializer::allTerritories["Peru"]);
        p1->cards().remove(&c1);
        p1->cards().remove(&c2);
        p1->cards().remove(&c3);
    }
    SECTION("Exchanging 3 cavalry cards with 2 territories owned, should return 8"){
        //arrange
        Card c1(Initializer::allTerritories["Alaska"], CardType::CAVALRY);
        Card c2(Initializer::allTerritories["Brazil"], CardType::CAVALRY);
        Card c3(Initializer::allTerritories["Peru"], CardType::CAVALRY);

        if(Initializer::allTerritories["Alaska"]->owner() != p1)
            p1->addTerritory(Initializer::allTerritories["Alaska"]);
        if(Initializer::allTerritories["Brazil"]->owner() != p1)
            p1->addTerritory(Initializer::allTerritories["Brazil"]);
        if(Initializer::allTerritories["Peru"]->owner() == p1)
            p1->removeTerritory(Initializer::allTerritories["Peru"]);

        p1->addCard(&c1);
        p1->addCard(&c2);
        p1->addCard(&c3);

        int expected_val = 8;

        //act
        int ret_val = p1->exchangeCards(&c1, &c2, &c3);

        //assert
        REQUIRE(expected_val == ret_val);
        if(Initializer::allTerritories["Alaska"]->owner() != p1)
            p1->removeTerritory(Initializer::allTerritories["Alaska"]);
        if(Initializer::allTerritories["Brazil"]->owner() != p1)
            p1->removeTerritory(Initializer::allTerritories["Brazil"]);
        if(Initializer::allTerritories["Peru"]->owner() == p1)
            p1->addTerritory(Initializer::allTerritories["Peru"]);
        p1->cards().remove(&c1);
        p1->cards().remove(&c2);
        p1->cards().remove(&c3);
    }
    SECTION("Exchanging 3 cavalry cards with 3 territories owned, should return 9"){
        //arrange
        Card c1(Initializer::allTerritories["Alaska"], CardType::CAVALRY);
        Card c2(Initializer::allTerritories["Brazil"], CardType::CAVALRY);
        Card c3(Initializer::allTerritories["Peru"], CardType::CAVALRY);

        if(Initializer::allTerritories["Alaska"]->owner() != p1)
            p1->addTerritory(Initializer::allTerritories["Alaska"]);
        if(Initializer::allTerritories["Brazil"]->owner() != p1)
            p1->addTerritory(Initializer::allTerritories["Brazil"]);
        if(Initializer::allTerritories["Peru"]->owner() != p1)
            p1->addTerritory(Initializer::allTerritories["Peru"]);

        p1->addCard(&c1);
        p1->addCard(&c2);
        p1->addCard(&c3);

        int expected_val = 9;

        //act
        int ret_val = p1->exchangeCards(&c1, &c2, &c3);

        //assert
        REQUIRE(expected_val == ret_val);
        if(Initializer::allTerritories["Alaska"]->owner() != p1)
            p1->removeTerritory(Initializer::allTerritories["Alaska"]);
        if(Initializer::allTerritories["Brazil"]->owner() != p1)
            p1->removeTerritory(Initializer::allTerritories["Brazil"]);
        if(Initializer::allTerritories["Peru"]->owner() != p1)
            p1->removeTerritory(Initializer::allTerritories["Peru"]);
        p1->cards().remove(&c1);
        p1->cards().remove(&c2);
        p1->cards().remove(&c3);
    }

    SECTION("Exchanging 3 cannon cards without owned territories, should return 8"){
        //arrange
        Card c1(Initializer::allTerritories["Alaska"], CardType::CANNON);
        Card c2(Initializer::allTerritories["Brazil"], CardType::CANNON);
        Card c3(Initializer::allTerritories["Peru"], CardType::CANNON);

        if(Initializer::allTerritories["Alaska"]->owner() == p1)
            p1->removeTerritory(Initializer::allTerritories["Alaska"]);
        if(Initializer::allTerritories["Brazil"]->owner() == p1)
            p1->removeTerritory(Initializer::allTerritories["Brazil"]);
        if(Initializer::allTerritories["Peru"]->owner() == p1)
            p1->removeTerritory(Initializer::allTerritories["Peru"]);

        p1->addCard(&c1);
        p1->addCard(&c2);
        p1->addCard(&c3);

        int expected_val = 8;

        //act
        int ret_val = p1->exchangeCards(&c1, &c2, &c3);

        //assert
        REQUIRE(expected_val == ret_val);
        if(Initializer::allTerritories["Alaska"]->owner() == p1)
            p1->addTerritory(Initializer::allTerritories["Alaska"]);
        if(Initializer::allTerritories["Brazil"]->owner() == p1)
            p1->addTerritory(Initializer::allTerritories["Brazil"]);
        if(Initializer::allTerritories["Peru"]->owner() == p1)
            p1->addTerritory(Initializer::allTerritories["Peru"]);
        p1->cards().remove(&c1);
        p1->cards().remove(&c2);
        p1->cards().remove(&c3);
    }
    SECTION("Exchanging 3 cannon cards with 1 territory owned, should return 9"){
        //arrange
        Card c1(Initializer::allTerritories["Alaska"], CardType::CANNON);
        Card c2(Initializer::allTerritories["Brazil"], CardType::CANNON);
        Card c3(Initializer::allTerritories["Peru"], CardType::CANNON);

        if(Initializer::allTerritories["Alaska"]->owner() != p1)
            p1->addTerritory(Initializer::allTerritories["Alaska"]);
        if(Initializer::allTerritories["Brazil"]->owner() == p1)
            p1->removeTerritory(Initializer::allTerritories["Brazil"]);
        if(Initializer::allTerritories["Peru"]->owner() == p1)
            p1->removeTerritory(Initializer::allTerritories["Peru"]);

        p1->addCard(&c1);
        p1->addCard(&c2);
        p1->addCard(&c3);

        int expected_val = 9;

        //act
        int ret_val = p1->exchangeCards(&c1, &c2, &c3);

        //assert
        REQUIRE(expected_val == ret_val);
        if(Initializer::allTerritories["Alaska"]->owner() != p1)
            p1->removeTerritory(Initializer::allTerritories["Alaska"]);
        if(Initializer::allTerritories["Brazil"]->owner() == p1)
            p1->addTerritory(Initializer::allTerritories["Brazil"]);
        if(Initializer::allTerritories["Peru"]->owner() == p1)
            p1->addTerritory(Initializer::allTerritories["Peru"]);
        p1->cards().remove(&c1);
        p1->cards().remove(&c2);
        p1->cards().remove(&c3);
    }
    SECTION("Exchanging 3 cannon cards with 2 territories owned, should return 10"){
        //arrange
        Card c1(Initializer::allTerritories["Alaska"], CardType::CANNON);
        Card c2(Initializer::allTerritories["Brazil"], CardType::CANNON);
        Card c3(Initializer::allTerritories["Peru"], CardType::CANNON);

        if(Initializer::allTerritories["Alaska"]->owner() != p1)
            p1->addTerritory(Initializer::allTerritories["Alaska"]);
        if(Initializer::allTerritories["Brazil"]->owner() != p1)
            p1->addTerritory(Initializer::allTerritories["Brazil"]);
        if(Initializer::allTerritories["Peru"]->owner() == p1)
            p1->removeTerritory(Initializer::allTerritories["Peru"]);

        p1->addCard(&c1);
        p1->addCard(&c2);
        p1->addCard(&c3);

        int expected_val = 10;

        //act
        int ret_val = p1->exchangeCards(&c1, &c2, &c3);

        //assert
        REQUIRE(expected_val == ret_val);
        if(Initializer::allTerritories["Alaska"]->owner() != p1)
            p1->removeTerritory(Initializer::allTerritories["Alaska"]);
        if(Initializer::allTerritories["Brazil"]->owner() != p1)
            p1->removeTerritory(Initializer::allTerritories["Brazil"]);
        if(Initializer::allTerritories["Peru"]->owner() == p1)
            p1->addTerritory(Initializer::allTerritories["Peru"]);
        p1->cards().remove(&c1);
        p1->cards().remove(&c2);
        p1->cards().remove(&c3);
    }
    SECTION("Exchanging 3 cannon cards with 3 territories owned, should return 11"){
        //arrange
        Card c1(Initializer::allTerritories["Alaska"], CardType::CANNON);
        Card c2(Initializer::allTerritories["Brazil"], CardType::CANNON);
        Card c3(Initializer::allTerritories["Peru"], CardType::CANNON);

        if(Initializer::allTerritories["Alaska"]->owner() != p1)
            p1->addTerritory(Initializer::allTerritories["Alaska"]);
        if(Initializer::allTerritories["Brazil"]->owner() != p1)
            p1->addTerritory(Initializer::allTerritories["Brazil"]);
        if(Initializer::allTerritories["Peru"]->owner() != p1)
            p1->addTerritory(Initializer::allTerritories["Peru"]);

        p1->addCard(&c1);
        p1->addCard(&c2);
        p1->addCard(&c3);

        int expected_val = 11;

        //act
        int ret_val = p1->exchangeCards(&c1, &c2, &c3);

        //assert
        REQUIRE(expected_val == ret_val);
        if(Initializer::allTerritories["Alaska"]->owner() != p1)
            p1->removeTerritory(Initializer::allTerritories["Alaska"]);
        if(Initializer::allTerritories["Brazil"]->owner() != p1)
            p1->removeTerritory(Initializer::allTerritories["Brazil"]);
        if(Initializer::allTerritories["Peru"]->owner() != p1)
            p1->removeTerritory(Initializer::allTerritories["Peru"]);
        p1->cards().remove(&c1);
        p1->cards().remove(&c2);
        p1->cards().remove(&c3);
    }

    SECTION("Exchanging 3 different type cards with none of them being joker, without owned territories, should return 10"){
        //arrange
        Card c1(Initializer::allTerritories["Alaska"], CardType::INFANTRY);
        Card c2(Initializer::allTerritories["Brazil"], CardType::CAVALRY);
        Card c3(Initializer::allTerritories["Peru"], CardType::CANNON);

        if(Initializer::allTerritories["Alaska"]->owner() == p1)
            p1->removeTerritory(Initializer::allTerritories["Alaska"]);
        if(Initializer::allTerritories["Brazil"]->owner() == p1)
            p1->removeTerritory(Initializer::allTerritories["Brazil"]);
        if(Initializer::allTerritories["Peru"]->owner() == p1)
            p1->removeTerritory(Initializer::allTerritories["Peru"]);

        p1->addCard(&c1);
        p1->addCard(&c2);
        p1->addCard(&c3);

        int expected_val = 10;

        //act
        int ret_val = p1->exchangeCards(&c1, &c2, &c3);

        //assert
        REQUIRE(expected_val == ret_val);
        if(Initializer::allTerritories["Alaska"]->owner() == p1)
            p1->addTerritory(Initializer::allTerritories["Alaska"]);
        if(Initializer::allTerritories["Brazil"]->owner() == p1)
            p1->addTerritory(Initializer::allTerritories["Brazil"]);
        if(Initializer::allTerritories["Peru"]->owner() == p1)
            p1->addTerritory(Initializer::allTerritories["Peru"]);
        p1->cards().remove(&c1);
        p1->cards().remove(&c2);
        p1->cards().remove(&c3);
    }
    SECTION("Exchanging 3 different type cards with none of them being joker, and 1 territory owned, should return 11"){
        //arrange
        Card c1(Initializer::allTerritories["Alaska"], CardType::INFANTRY);
        Card c2(Initializer::allTerritories["Brazil"], CardType::CAVALRY);
        Card c3(Initializer::allTerritories["Peru"], CardType::CANNON);

        if(Initializer::allTerritories["Alaska"]->owner() != p1)
            p1->addTerritory(Initializer::allTerritories["Alaska"]);
        if(Initializer::allTerritories["Brazil"]->owner() == p1)
            p1->removeTerritory(Initializer::allTerritories["Brazil"]);
        if(Initializer::allTerritories["Peru"]->owner() == p1)
            p1->removeTerritory(Initializer::allTerritories["Peru"]);

        p1->addCard(&c1);
        p1->addCard(&c2);
        p1->addCard(&c3);

        int expected_val = 11;

        //act
        int ret_val = p1->exchangeCards(&c1, &c2, &c3);

        //assert
        REQUIRE(expected_val == ret_val);
        if(Initializer::allTerritories["Alaska"]->owner() != p1)
            p1->removeTerritory(Initializer::allTerritories["Alaska"]);
        if(Initializer::allTerritories["Brazil"]->owner() == p1)
            p1->addTerritory(Initializer::allTerritories["Brazil"]);
        if(Initializer::allTerritories["Peru"]->owner() == p1)
            p1->addTerritory(Initializer::allTerritories["Peru"]);
        p1->cards().remove(&c1);
        p1->cards().remove(&c2);
        p1->cards().remove(&c3);
    }
    SECTION("Exchanging 3 different type cards with none of them being joker, and 2 territories owned, should return 12"){
        //arrange
        Card c1(Initializer::allTerritories["Alaska"], CardType::INFANTRY);
        Card c2(Initializer::allTerritories["Brazil"], CardType::CAVALRY);
        Card c3(Initializer::allTerritories["Peru"], CardType::CANNON);

        if(Initializer::allTerritories["Alaska"]->owner() != p1)
            p1->addTerritory(Initializer::allTerritories["Alaska"]);
        if(Initializer::allTerritories["Brazil"]->owner() != p1)
            p1->addTerritory(Initializer::allTerritories["Brazil"]);
        if(Initializer::allTerritories["Peru"]->owner() == p1)
            p1->removeTerritory(Initializer::allTerritories["Peru"]);

        p1->addCard(&c1);
        p1->addCard(&c2);
        p1->addCard(&c3);

        int expected_val = 12;

        //act
        int ret_val = p1->exchangeCards(&c1, &c2, &c3);

        //assert
        REQUIRE(expected_val == ret_val);
        if(Initializer::allTerritories["Alaska"]->owner() != p1)
            p1->removeTerritory(Initializer::allTerritories["Alaska"]);
        if(Initializer::allTerritories["Brazil"]->owner() != p1)
            p1->removeTerritory(Initializer::allTerritories["Brazil"]);
        if(Initializer::allTerritories["Peru"]->owner() == p1)
            p1->addTerritory(Initializer::allTerritories["Peru"]);
        p1->cards().remove(&c1);
        p1->cards().remove(&c2);
        p1->cards().remove(&c3);
    }
    SECTION("Exchanging 3 different type cards with none of them being joker, and 3 territories owned"
            " should return 13"){
        //arrange
        Card c1(Initializer::allTerritories["Alaska"], CardType::INFANTRY);
        Card c2(Initializer::allTerritories["Brazil"], CardType::CAVALRY);
        Card c3(Initializer::allTerritories["Peru"], CardType::CANNON);

        if(Initializer::allTerritories["Alaska"]->owner() != p1)
            p1->addTerritory(Initializer::allTerritories["Alaska"]);
        if(Initializer::allTerritories["Brazil"]->owner() != p1)
            p1->addTerritory(Initializer::allTerritories["Brazil"]);
        if(Initializer::allTerritories["Peru"]->owner() != p1)
            p1->addTerritory(Initializer::allTerritories["Peru"]);

        p1->addCard(&c1);
        p1->addCard(&c2);
        p1->addCard(&c3);

        int expected_val = 13;

        //act
        int ret_val = p1->exchangeCards(&c1, &c2, &c3);

        //assert
        REQUIRE(expected_val == ret_val);
        if(Initializer::allTerritories["Alaska"]->owner() != p1)
            p1->removeTerritory(Initializer::allTerritories["Alaska"]);
        if(Initializer::allTerritories["Brazil"]->owner() != p1)
            p1->removeTerritory(Initializer::allTerritories["Brazil"]);
        if(Initializer::allTerritories["Peru"]->owner() != p1)
            p1->removeTerritory(Initializer::allTerritories["Peru"]);
        p1->cards().remove(&c1);
        p1->cards().remove(&c2);
        p1->cards().remove(&c3);
    }
    SECTION("Exchanging 2 infantry cards and one joker, without owned territories"
            " should return 12"){
        //arrange
        Card c1(Initializer::allTerritories["Alaska"], CardType::INFANTRY);
        Card c2(Initializer::allTerritories["Brazil"], CardType::INFANTRY);
        Card c3(CardType::JOKER);

        if(Initializer::allTerritories["Alaska"]->owner() == p1)
            p1->removeTerritory(Initializer::allTerritories["Alaska"]);
        if(Initializer::allTerritories["Brazil"]->owner() == p1)
            p1->removeTerritory(Initializer::allTerritories["Brazil"]);

        p1->addCard(&c1);
        p1->addCard(&c2);
        p1->addCard(&c3);

        int expected_val = 12;

        //act
        int ret_val = p1->exchangeCards(&c1, &c2, &c3);

        //assert
        REQUIRE(expected_val == ret_val);
        if(Initializer::allTerritories["Alaska"]->owner() == p1)
            p1->addTerritory(Initializer::allTerritories["Alaska"]);
        if(Initializer::allTerritories["Brazil"]->owner() == p1)
            p1->addTerritory(Initializer::allTerritories["Brazil"]);
        p1->cards().remove(&c1);
        p1->cards().remove(&c2);
        p1->cards().remove(&c3);
    }
    SECTION("Exchanging 2 cavalry cards and one joker, without owned territories"
            " should return 12"){
        //arrange
        Card c1(Initializer::allTerritories["Alaska"], CardType::CAVALRY);
        Card c2(Initializer::allTerritories["Brazil"], CardType::CAVALRY);
        Card c3(CardType::JOKER);

        if(Initializer::allTerritories["Alaska"]->owner() == p1)
            p1->removeTerritory(Initializer::allTerritories["Alaska"]);
        if(Initializer::allTerritories["Brazil"]->owner() == p1)
            p1->removeTerritory(Initializer::allTerritories["Brazil"]);

        p1->addCard(&c1);
        p1->addCard(&c2);
        p1->addCard(&c3);

        int expected_val = 12;

        //act
        int ret_val = p1->exchangeCards(&c1, &c2, &c3);

        //assert
        REQUIRE(expected_val == ret_val);
        if(Initializer::allTerritories["Alaska"]->owner() == p1)
            p1->addTerritory(Initializer::allTerritories["Alaska"]);
        if(Initializer::allTerritories["Brazil"]->owner() == p1)
            p1->addTerritory(Initializer::allTerritories["Brazil"]);
        p1->cards().remove(&c1);
        p1->cards().remove(&c2);
        p1->cards().remove(&c3);
    }
    SECTION("Exchanging 2 cannon cards and one joker, without owned territories"
            " should return 12"){
        //arrange
        Card c1(Initializer::allTerritories["Alaska"], CardType::CANNON);
        Card c2(Initializer::allTerritories["Brazil"], CardType::CANNON);
        Card c3(CardType::JOKER);

        if(Initializer::allTerritories["Alaska"]->owner() == p1)
            p1->removeTerritory(Initializer::allTerritories["Alaska"]);
        if(Initializer::allTerritories["Brazil"]->owner() == p1)
            p1->removeTerritory(Initializer::allTerritories["Brazil"]);

        p1->addCard(&c1);
        p1->addCard(&c2);
        p1->addCard(&c3);

        int expected_val = 12;

        //act
        int ret_val = p1->exchangeCards(&c1, &c2, &c3);

        //assert
        REQUIRE(expected_val == ret_val);
        if(Initializer::allTerritories["Alaska"]->owner() == p1)
            p1->addTerritory(Initializer::allTerritories["Alaska"]);
        if(Initializer::allTerritories["Brazil"]->owner() == p1)
            p1->addTerritory(Initializer::allTerritories["Brazil"]);
        p1->cards().remove(&c1);
        p1->cards().remove(&c2);
        p1->cards().remove(&c3);
    }

    SECTION("Exchanging 2 cannon and one infantry card, should return -1"){
        //arrange
        Card c1(Initializer::allTerritories["Alaska"], CardType::CANNON);
        Card c2(Initializer::allTerritories["Brazil"], CardType::CANNON);
        Card c3(Initializer::allTerritories["Peru"], CardType::INFANTRY);

        p1->addCard(&c1);
        p1->addCard(&c2);
        p1->addCard(&c3);

        int expected_val = -1;

        //act
        int ret_val = p1->exchangeCards(&c1, &c2, &c3);

        //assert
        REQUIRE(expected_val == ret_val);

        p1->cards().remove(&c1);
        p1->cards().remove(&c2);
        p1->cards().remove(&c3);
    }

    SECTION("Territory conquer mission for 18 territories successful, should return true"){
        //arrange
        foreach(auto &t, p1->territories())
            t->increaseNumOfTanks(1);

        int expected_val = true;

        //act
        int ret_val = p1->missionCard()->checkAssignment(p1);

        //assert
        REQUIRE(expected_val == ret_val);
        foreach(auto &t, p1->territories())
            t->decreaseNumOfTanks(1);

    }
    SECTION("Territory conquer mission for 18 territories unsuccessful, should return false"){
        //arrange
        int expected_val = false;

        //act
        int ret_val = p1->missionCard()->checkAssignment(p1);

        //assert
        REQUIRE(expected_val == ret_val);
        foreach(auto &t, p1->territories())
            t->decreaseNumOfTanks(1);

    }
    SECTION("Territory conquer mission for 24 territories successful, should return true"){
        //arrange
        int i = 0;
        foreach(auto &t, p1->territories()){
            p2->addTerritory(t);
            i++;
            if(i == 3)
                break;
        }

        int expected_val = true;

        //act
        int ret_val = p2->missionCard()->checkAssignment(p2);

        //assert
        REQUIRE(expected_val == ret_val);
        foreach(auto &t, p1->territories())
            t->decreaseNumOfTanks(1);

    }
    SECTION("Territory conquer mission for 24 territories unsuccessful, should return false"){
        //arrange
        int i = 0;
        foreach(auto &t, p2->territories()){
            p2->removeTerritory(t);
            i++;
            if(i == 7)
                break;
        }

        int expected_val = false;

        //act
        int ret_val = p2->missionCard()->checkAssignment(p2);

        //assert
        REQUIRE(expected_val == ret_val);

    }

    delete game;
}
