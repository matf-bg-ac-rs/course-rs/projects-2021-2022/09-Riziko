TEMPLATE = app
QT += gui core widgets

CONFIG += c++17

isEmpty(CATCH_INCLUDE_DIR): CATCH_INCLUDE_DIR=$$(CATCH_INCLUDE_DIR)
# set by Qt Creator wizard
isEmpty(CATCH_INCLUDE_DIR): CATCH_INCLUDE_DIR=  $$PWD/../lib
!isEmpty(CATCH_INCLUDE_DIR): INCLUDEPATH *= $${CATCH_INCLUDE_DIR}

isEmpty(CATCH_INCLUDE_DIR): {
    message("CATCH_INCLUDE_DIR is not set, assuming Catch2 can be found automatically in your system")
}

SOURCES +=     main.cpp \
               ../server/game.cpp \
               ../server/player.cpp \
               ../server/initializer.cpp \
               ../server/card.cpp \
               ../server/cardterritoryconquer.cpp \
               ../server/settings.cpp \
               ../server/territory.cpp \
               ../server/missioncard.cpp \
               ../server/continent.cpp \
               ../server/cardcontinentconquer.cpp \
               ../server/carddestroytroops.cpp \
               tests1.cpp \
               tests2.cpp \
               tests3.cpp \
               tests4.cpp

HEADERS +=     ../server/game.h \
               ../server/player.h \
               ../server/initializer.h \
               ../server/card.h \
               ../server/cardterritoryconquer.h \
               ../server/settings.h \
               ../server/territory.h \
               ../server/missioncard.h \
               ../server/continent.h \
               ../server/cardcontinentconquer.h \
               ../server/carddestroytroops.h \

RESOURCES += ../res/txt.qrc \
            ../res/images.qrc \
