#include <catch2/catch.hpp>
#include "../server/game.h"
#include "../server/initializer.h"
#include "../server/player.h"
#include "../server/card.h"
#include "../server/cardterritoryconquer.h"
#include "../server/settings.h"
#include "../server/territory.h"
#include "../server/missioncard.h"
#include "../server/continent.h"
#include "../server/cardcontinentconquer.h"
#include "../server/carddestroytroops.h"

TEST_CASE("Initializer and continent owner check", "[initializer][continent]")
{

    Player *p1 = new Player("Player1", Color::RED, true);
    Player *p2 = new Player("Player2", Color::BLUE, false);

    QVector<Player*> players = {p1,p2};

    Goal gg = Goal::CARDS;

    Settings* s = new Settings(2, 5, gg, players);
    auto continents = Initializer::initializeContinents();
    auto cards = Initializer::initializeCards();
    auto missionCards = Initializer::initializeMissionCards(players);

    auto mcard1 = new CardTerritoryConquer(":/resources/images/MissionCards/Conquer-18",18);
    auto mcard2 = new CardTerritoryConquer(":/resources/images/MissionCards/Conquer-24",24);

    p1->setMissionCard(mcard1);
    p2->setMissionCard(mcard2);

    Game *game = new Game(cards, continents, s);
    game->start();

    QSet<Territory*> t_northAmerica;
    QSet<Territory*> t_southAmerica;
    QSet<Territory*> t_Europe;
    QSet<Territory*> t_Africa;
    QSet<Territory*> t_Asia;
    QSet<Territory*> t_Australia;

    // moraces da napravis prvo sve te teritorije
    // pa ces onda da napravis sve QSetove iznad
    // e onda ces da dodas ove qsetove u ova sr**a
    // onda ces da pises testove
    // koji ce da budu relativno kratki

    Territory Afghanistan("Afghanistan");
    Territory Alaska("Alaska");
    Territory Alberta("Alberta");
    Territory Argentina("Argentina");
    Territory Brazil("Brazil");
    Territory Central_Africa("Central_Africa");
    Territory Central_America("Central_America");
    Territory China("China");
    Territory East_Africa("East_Africa");
    Territory Eastern_Australia("Eastern_Australia");
    Territory Eastern_Canada("Eastern_Canada");
    Territory Eastern_United_States("Eastern_United_States");
    Territory Egypt("Egypt");
    Territory Great_Britain("Great_Britain");
    Territory Greenland("Greenland");
    Territory Iceland("Iceland");
    Territory India("India");
    Territory Indonesia("Indonesia");
    Territory Irkutsk("Irkutsk");
    Territory Japan("Japan");
    Territory Kamchatka("Kamchatka");
    Territory Madagascar("Madagascar");
    Territory Middle_East("Middle_East");
    Territory Mongolia("Mongolia");
    Territory New_Guinea("New_Guinea");
    Territory North_Africa("North_Africa");
    Territory Northern_Europe("Northern_Europe");
    Territory Northwest_Territory("Northwest_Territory");
    Territory Ontario("Ontario");
    Territory Peru("Peru");
    Territory Russia("Russia");
    Territory Scandinavia("Scandinavia");
    Territory Siberia("Siberia");
    Territory South_Africa("South_Africa");
    Territory Southeast_Asia("Southeast_Asia");
    Territory Southern_Europe("Southern_Europe");
    Territory Ural("Ural");
    Territory Venezuela("Venezuela");
    Territory Western_Australia("Western_Australia");
    Territory Western_Europe("Western_Europe");
    Territory Western_United_States("Western_United_States");
    Territory Yakutsk("Yakutsk");

    // North America
    t_northAmerica.insert(&Alaska);
    t_northAmerica.insert(&Alberta);
    t_northAmerica.insert(&Central_America);
    t_northAmerica.insert(&Eastern_Canada);
    t_northAmerica.insert(&Eastern_United_States);
    t_northAmerica.insert(&Greenland);
    t_northAmerica.insert(&Northwest_Territory);
    t_northAmerica.insert(&Ontario);
    t_northAmerica.insert(&Western_United_States);

    // South America
    t_southAmerica.insert(&Argentina);
    t_southAmerica.insert(&Brazil);
    t_southAmerica.insert(&Peru);
    t_southAmerica.insert(&Venezuela);

    // Europe
    t_Europe.insert(&Great_Britain);
    t_Europe.insert(&Iceland);
    t_Europe.insert(&Northern_Europe);
    t_Europe.insert(&Russia);
    t_Europe.insert(&Scandinavia);
    t_Europe.insert(&Southern_Europe);
    t_Europe.insert(&Western_Europe);

    // Africa
    t_Africa.insert(&Central_Africa);
    t_Africa.insert(&East_Africa);
    t_Africa.insert(&Egypt);
    t_Africa.insert(&Madagascar);
    t_Africa.insert(&North_Africa);
    t_Africa.insert(&South_Africa);

    // Asia
    t_Asia.insert(&Afghanistan);
    t_Asia.insert(&China);
    t_Asia.insert(&India);
    t_Asia.insert(&Irkutsk);
    t_Asia.insert(&Japan);
    t_Asia.insert(&Kamchatka);
    t_Asia.insert(&Middle_East);
    t_Asia.insert(&Mongolia);
    t_Asia.insert(&Siberia);
    t_Asia.insert(&Southeast_Asia);
    t_Asia.insert(&Ural);
    t_Asia.insert(&Yakutsk);

    // Australia
    t_Australia.insert(&Eastern_Australia);
    t_Australia.insert(&Indonesia);
    t_Australia.insert(&New_Guinea);
    t_Australia.insert(&Western_Australia);


    Continent *NorthAmerica = new Continent("North_America", t_northAmerica, 5);
    Continent *SouthAmerica = new Continent("South_America", t_southAmerica, 2);
    Continent *Europe = new Continent("Europe", t_Europe, 5);
    Continent *Africa = new Continent("Africa", t_Africa, 3);
    Continent *Asia = new Continent("Asia", t_Asia, 7);
    Continent *Australia = new Continent("Australia", t_Australia, 2);

    QSet<Continent*> testContinents;
    testContinents.insert(NorthAmerica);
    testContinents.insert(SouthAmerica);
    testContinents.insert(Europe);
    testContinents.insert(Africa);
    testContinents.insert(Asia);
    testContinents.insert(Australia);


    // Cards

    Card c_Afghanistan(&Afghanistan, CardType::CAVALRY);
    Card c_Alaska(&Alaska, CardType::INFANTRY);
    Card c_Alberta(&Alberta, CardType::CAVALRY);
    Card c_Argentina(&Argentina, CardType::INFANTRY);
    Card c_Brazil(&Brazil, CardType::CANNON);
    Card c_Central_Africa(&Central_Africa, CardType::INFANTRY);
    Card c_Central_America(&Central_America, CardType::CANNON);
    Card c_China(&China, CardType::INFANTRY);
    Card c_East_Africa(&East_Africa, CardType::INFANTRY);
    Card c_Eastern_Australia(&Eastern_Australia, CardType::CANNON);
    Card c_Eastern_Canada(&Eastern_Canada, CardType::CAVALRY);
    Card c_Eastern_United_States(&Eastern_United_States, CardType::CANNON);
    Card c_Egypt(&Egypt, CardType::INFANTRY);
    Card c_Great_Britain(&Great_Britain, CardType::CANNON);
    Card c_Greenland(&Greenland, CardType::CAVALRY);
    Card c_Iceland(&Iceland, CardType::INFANTRY);
    Card c_India(&India, CardType::CAVALRY);
    Card c_Indonesia(&Indonesia, CardType::CANNON);
    Card c_Irkutsk(&Irkutsk, CardType::CAVALRY);
    Card c_Japan(&Japan, CardType::CANNON);
    Card c_Kamchatka(&Kamchatka, CardType::INFANTRY);
    Card c_Madagascar(&Madagascar, CardType::CAVALRY);
    Card c_Middle_East(&Middle_East, CardType::INFANTRY);
    Card c_Mongolia(&Mongolia, CardType::INFANTRY);
    Card c_New_Guinea(&New_Guinea, CardType::INFANTRY);
    Card c_North_Africa(&North_Africa, CardType::CAVALRY);
    Card c_Northern_Europe(&Northern_Europe, CardType::CANNON);
    Card c_Northwest_Territory(&Northwest_Territory, CardType::CANNON);
    Card c_Ontario(&Ontario, CardType::CAVALRY);
    Card c_Peru(&Peru, CardType::INFANTRY);
    Card c_Russia(&Russia, CardType::CAVALRY);
    Card c_Scandinavia(&Scandinavia, CardType::CAVALRY);
    Card c_Siberia(&Siberia, CardType::CAVALRY);
    Card c_South_Africa(&South_Africa, CardType::CANNON);
    Card c_Southeast_Asia(&Southeast_Asia, CardType::INFANTRY);
    Card c_Southern_Europe(&Southern_Europe, CardType::CANNON);
    Card c_Ural(&Ural, CardType::CAVALRY);
    Card c_Venezuela(&Venezuela, CardType::INFANTRY);
    Card c_Western_Australia(&Western_Australia, CardType::CANNON);
    Card c_Western_Europe(&Western_Europe, CardType::CANNON);
    Card c_Western_United_States(&Western_United_States, CardType::CANNON);
    Card c_Yakutsk(&Yakutsk, CardType::CAVALRY);
    Card c_joker1(CardType::JOKER);
    Card c_joker2(CardType::JOKER);

    QSet<Card*> testCards;
    testCards.insert(&c_Afghanistan);
    testCards.insert(&c_Alaska);
    testCards.insert(&c_Alberta);
    testCards.insert(&c_Argentina);
    testCards.insert(&c_Brazil);
    testCards.insert(&c_Central_Africa);
    testCards.insert(&c_Central_America);
    testCards.insert(&c_China);
    testCards.insert(&c_East_Africa);
    testCards.insert(&c_Eastern_Australia);
    testCards.insert(&c_Eastern_Canada);
    testCards.insert(&c_Eastern_United_States);
    testCards.insert(&c_Egypt);
    testCards.insert(&c_Great_Britain);
    testCards.insert(&c_Greenland);
    testCards.insert(&c_Iceland);
    testCards.insert(&c_India);
    testCards.insert(&c_Indonesia);
    testCards.insert(&c_Irkutsk);
    testCards.insert(&c_Japan);
    testCards.insert(&c_joker1);
    testCards.insert(&c_joker2);
    testCards.insert(&c_Kamchatka);
    testCards.insert(&c_Madagascar);
    testCards.insert(&c_Middle_East);
    testCards.insert(&c_Mongolia);
    testCards.insert(&c_New_Guinea);
    testCards.insert(&c_North_Africa);
    testCards.insert(&c_Northern_Europe);
    testCards.insert(&c_Northwest_Territory);
    testCards.insert(&c_Ontario);
    testCards.insert(&c_Peru);
    testCards.insert(&c_Russia);
    testCards.insert(&c_Scandinavia);
    testCards.insert(&c_Siberia);
    testCards.insert(&c_South_Africa);
    testCards.insert(&c_Southeast_Asia);
    testCards.insert(&c_Southern_Europe);
    testCards.insert(&c_Ural);
    testCards.insert(&c_Venezuela);
    testCards.insert(&c_Western_Australia);
    testCards.insert(&c_Western_Europe);
    testCards.insert(&c_Western_United_States);
    testCards.insert(&c_Yakutsk);

    // MissionCards

    CardTerritoryConquer ctc_18(":/resources/images/MissionCards/Conquer-18", 18);
    CardTerritoryConquer ctc_24(":/resources/images/MissionCards/Conquer-24", 24);

    CardContinentConquer ccc_Asia_Afica(":/resources/images/MissionCards/Conquer-Asia-Africa", Asia, Africa);
    CardContinentConquer ccc_North_America_Africa(":/resources/images/MissionCards/Conquer-North_America-Africa", NorthAmerica, Africa);
    CardContinentConquer ccc_North_America_Australia(":/resources/images/MissionCards/Conquer-North_America-Australia", NorthAmerica, Australia);
    CardContinentConquer ccc_South_America_Asia(":/resources/images/MissionCards/Conquer-South_America-Asia", SouthAmerica, Asia);

    CardDestroyTroops cdt_0(":/resources/images/MissionCards/Destroy-0", p2);
    CardDestroyTroops cdt_1(":/resources/images/MissionCards/Destroy-1", p1);

    QSet<MissionCard*> testMissionCards;
    testMissionCards.insert(&ctc_18);
    testMissionCards.insert(&ctc_24);

    testMissionCards.insert(&ccc_Asia_Afica);
    testMissionCards.insert(&ccc_North_America_Africa);
    testMissionCards.insert(&ccc_North_America_Australia);
    testMissionCards.insert(&ccc_South_America_Asia);

    testMissionCards.insert(&cdt_0);
    testMissionCards.insert(&cdt_1);


    SECTION("Successful continent initialization, all continents should match."){
        //arrange
        int matchedCounter = 0;
        bool flag = false;

        int expected_val = 6;
        //act
        foreach(auto &con, continents){
            foreach(auto &t_con, testContinents){
                if(con->name() == t_con->name() && con->numOfBonusTanks() == t_con->numOfBonusTanks()){
                    foreach(auto &t, con->territories()){
                        foreach(auto &t_t, t_con->territories()){
                            if(t->name() == t_t->name()){
                                flag = true;
                                break;
                            }
                        }
                        if(!flag){
                            matchedCounter = -1;
                            break;
                        }
                        flag = false;
                    }
                    matchedCounter += 1;
                }
            }
        }
        //assert
        REQUIRE(matchedCounter == expected_val);
    }

    SECTION("Unsuccessful continent initialization, Australia is missing."){
        //arrange
        int matchedCounter = 0;
        bool flag = false;
        testContinents.remove(Australia);

        int expected_val = 6;
        //act
        foreach(auto &con, continents){
            foreach(auto &t_con, testContinents){
                if(con->name() == t_con->name() && con->numOfBonusTanks() == t_con->numOfBonusTanks()){
                    foreach(auto &t, con->territories()){
                        foreach(auto &t_t, t_con->territories()){
                            if(t->name() == t_t->name()){
                                flag = true;
                                break;
                            }
                        }
                        if(!flag){
                            matchedCounter = -1;
                            break;
                        }
                        flag = false;
                    }
                    matchedCounter += 1;
                }
            }
        }
        //assert
        REQUIRE(matchedCounter != expected_val);
    }


    SECTION("Checking if player1 owns Australia, should return true"){
        //arrange
        bool expected_val = true;
        foreach(auto &t, Initializer::allContinents["Australia"]->territories()){
            t->setOwner(p1);
        }

        //act
        bool value = Initializer::allContinents["Australia"]->CheckOwner(p1);
        //assert
        REQUIRE(expected_val == value);
    }

    SECTION("Checking if player1 owns Australia, but he doesn't should return false"){
        //arrange
        bool expected_val = false;
        auto t = Initializer::allContinents["Australia"]->territories().begin();
        if((*t)->owner() == p1){
            (*t)->setOwner(p2);
        }

        //act
        bool value = Initializer::allContinents["Australia"]->CheckOwner(p1);
        //assert
        REQUIRE(expected_val == value);
    }

    SECTION("Checking if player1 owns Asia, should return true"){
        //arrange
        bool expected_val = true;
        foreach(auto &t, Initializer::allContinents["Asia"]->territories()){
            t->setOwner(p1);
        }

        //act
        bool value = Initializer::allContinents["Asia"]->CheckOwner(p1);
        //assert
        REQUIRE(expected_val == value);
    }

    SECTION("Checking if player1 owns Asia, but he doesn't should return false"){
        //arrange
        bool expected_val = false;
        auto t = Initializer::allContinents["Asia"]->territories().begin();
        if((*t)->owner() == p1){
            (*t)->setOwner(p2);
        }

        //act
        bool value = Initializer::allContinents["Asia"]->CheckOwner(p1);
        //assert
        REQUIRE(expected_val == value);
    }

    SECTION("Checking if player1 owns North_America, should return true"){
        //arrange
        bool expected_val = true;
        foreach(auto &t, Initializer::allContinents["North_America"]->territories()){
            t->setOwner(p1);
        }

        //act
        bool value = Initializer::allContinents["North_America"]->CheckOwner(p1);
        //assert
        REQUIRE(expected_val == value);
    }

    SECTION("Checking if player1 owns North_America, but he doesn't should return false"){
        //arrange
        bool expected_val = false;
        auto t = Initializer::allContinents["North_America"]->territories().begin();
        if((*t)->owner() == p1){
            (*t)->setOwner(p2);
        }

        //act
        bool value = Initializer::allContinents["North_America"]->CheckOwner(p1);
        //assert
        REQUIRE(expected_val == value);
    }

    SECTION("Checking if player1 owns South_America, should return true"){
        //arrange
        bool expected_val = true;
        foreach(auto &t, Initializer::allContinents["South_America"]->territories()){
            t->setOwner(p1);
        }

        //act
        bool value = Initializer::allContinents["South_America"]->CheckOwner(p1);
        //assert
        REQUIRE(expected_val == value);
    }

    SECTION("Checking if player1 owns South_America, but he doesn't should return false"){
        //arrange
        bool expected_val = false;
        auto t = Initializer::allContinents["South_America"]->territories().begin();
        if((*t)->owner() == p1){
            (*t)->setOwner(p2);
        }

        //act
        bool value = Initializer::allContinents["South_America"]->CheckOwner(p1);
        //assert
        REQUIRE(expected_val == value);
    }

    SECTION("Checking if player1 owns Africa, should return true"){
        //arrange
        bool expected_val = true;
        foreach(auto &t, Initializer::allContinents["Africa"]->territories()){
            t->setOwner(p1);
        }

        //act
        bool value = Initializer::allContinents["Africa"]->CheckOwner(p1);
        //assert
        REQUIRE(expected_val == value);
    }

    SECTION("Checking if player1 owns Africa, but he doesn't should return false"){
        //arrange
        bool expected_val = false;
        auto t = Initializer::allContinents["Africa"]->territories().begin();
        if((*t)->owner() == p1){
            (*t)->setOwner(p2);
        }

        //act
        bool value = Initializer::allContinents["Africa"]->CheckOwner(p1);
        //assert
        REQUIRE(expected_val == value);
    }

    SECTION("Checking if player1 owns Europe, should return true"){
        //arrange
        bool expected_val = true;
        foreach(auto &t, Initializer::allContinents["Europe"]->territories()){
            t->setOwner(p1);
        }

        //act
        bool value = Initializer::allContinents["Europe"]->CheckOwner(p1);
        //assert
        REQUIRE(expected_val == value);
    }

    SECTION("Checking if player1 owns Europe, but he doesn't should return false"){
        //arrange
        bool expected_val = false;
        auto t = Initializer::allContinents["Europe"]->territories().begin();
        if((*t)->owner() == p1){
            (*t)->setOwner(p2);
        }

        //act
        bool value = Initializer::allContinents["Europe"]->CheckOwner(p1);
        //assert
        REQUIRE(expected_val == value);
    }

    SECTION("Successful cards initialization, all cards should match."){
        //arrange
        int matchedCounter = 0;

        int expected_val = 44;
        //act
        foreach(auto &c, cards){
            foreach(auto &t_c, testCards){
                if(c->cardType() == CardType::JOKER || t_c->cardType() == CardType::JOKER){
                    if(c->cardType() == CardType::JOKER && t_c->cardType() == CardType::JOKER)
                        matchedCounter += 1;
                }
                else if(c->territory()->name() == t_c->territory()->name() && c->cardType() == t_c->cardType()){
                    matchedCounter += 1;
                }
            }
        }

        matchedCounter -= 2;
        //assert
        REQUIRE(matchedCounter == expected_val);
    }

    SECTION("Unsuccessful cards initialization, Ural is missing."){
        //arrange
        int matchedCounter = 0;
        testCards.remove(&c_Ural);

        int expected_val = 44;
        //act
        foreach(auto &c, cards){
            foreach(auto &t_c, testCards){
                if(c->cardType() == CardType::JOKER || t_c->cardType() == CardType::JOKER){
                    if(c->cardType() == CardType::JOKER && t_c->cardType() == CardType::JOKER)
                        matchedCounter += 1;
                }
                else if(c->territory()->name() == t_c->territory()->name() && c->cardType() == t_c->cardType()){
                    matchedCounter += 1;
                }
            }
        }

        matchedCounter -= 2;
        //assert
        REQUIRE(matchedCounter != expected_val);
    }

    SECTION("Successful mission cards initialization, all cards should match."){
        //arrange
        int matchedCounter = 0;

        int expected_val = 8;
        //act
        foreach(auto &c, missionCards){
            foreach(auto &t_c, testMissionCards){
                if(c->path() == t_c->path()){
                    matchedCounter += 1;
                }
            }
        }

        //assert
        REQUIRE(matchedCounter == expected_val);
    }


    SECTION("Unsuccessful mission cards initialization, Conquer continents Asia Africa missing."){
        //arrange
        int matchedCounter = 0;
        testMissionCards.remove(&ccc_Asia_Afica);

        int expected_val = 8;
        //act
        foreach(auto &c, missionCards){
            foreach(auto &t_c, testMissionCards){
                if(c->path() == t_c->path()){
                    matchedCounter += 1;
                }
            }
        }

        //assert
        REQUIRE(matchedCounter != expected_val);
    }

    delete game;
}
