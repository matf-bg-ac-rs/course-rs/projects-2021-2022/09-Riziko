#include <catch2/catch.hpp>
#include "../server/game.h"
#include "../server/initializer.h"
#include "../server/player.h"
#include "../server/card.h"
#include "../server/cardterritoryconquer.h"
#include "../server/settings.h"
#include "../server/territory.h"
#include "../server/missioncard.h"
#include "../server/continent.h"
#include "../server/cardcontinentconquer.h"
#include "../server/carddestroytroops.h"

TEST_CASE("Card destroy troops, card continent conquer and check goal for player", "[destroy][continentConquer][checkGoal]")
{
    Player *p1 = new Player("Player1", Color::RED, true);
    Player *p2 = new Player("Player2", Color::BLUE, false);

    QVector<Player*> players = {p1,p2};

    Goal gg = Goal::CARDS;

    Settings* s = new Settings(2, 5, gg, players);
    auto continents = Initializer::initializeContinents();
    auto cards = Initializer::initializeCards();

    auto mcard1 = new CardDestroyTroops(":/resources/images/MissionCards/Destroy-0", p2);
    auto mcard2 = new CardDestroyTroops(":/resources/images/MissionCards/Destroy-1", p1);
    auto mcard3 = new CardContinentConquer(":/resources/images/MissionCards/Conquer-Asia-Africa", Initializer::allContinents["Asia"], Initializer::allContinents["Africa"]);
    auto mcard4 = new CardContinentConquer(":/resources/images/MissionCards/Conquer-North_America-Africa", Initializer::allContinents["North_America"], Initializer::allContinents["Africa"]);
    auto mcard5 = new CardContinentConquer(":/resources/images/MissionCards/Conquer-North_America-Australia", Initializer::allContinents["North_America"], Initializer::allContinents["Australia"]);
    auto mcard6 = new CardContinentConquer(":/resources/images/MissionCards/Conquer-South_America-Australia", Initializer::allContinents["South_America"], Initializer::allContinents["Australia"]);


    Game *game = new Game(cards, continents, s);
    game->start();

    SECTION("Checking if blue player is destroyed by the current player, which he is, should return true") {
        // Arrange
        p1->setMissionCard(mcard1);
        p2->setDestroyer(p1);
        bool expected_val = true;

        // Act
        bool ret_val = p1->missionCard()->checkAssignment(p1);

        // Assert
        REQUIRE(ret_val == expected_val);

    }

    SECTION("Checking if blue player is destroyed by the current player, which he isn't, should return false") {
        // Arrange
        p1->setMissionCard(mcard1);
        bool expected_val = false;

        // Act
        bool ret_val = p1->missionCard()->checkAssignment(p1);

        // Assert
        REQUIRE(ret_val == expected_val);

    }

    SECTION("Checking if the current player is destroyed by the current player, which implies 24 territory goal"
            "that is satisfied, should return true") {
        // Arrange
        p1->setMissionCard(mcard2);
        int i = 0;
        foreach(auto &t, p2->territories()) {
            p1->addTerritory(t);
            i++;
            if(i == 3)
                break;
        }

        bool expected_val = true;

        // Act
        bool ret_val = p1->missionCard()->checkAssignment(p1);

        // Assert
        REQUIRE(ret_val == expected_val);

        i = 0;
        foreach(auto &t, p2->territories()) {
            p1->removeTerritory(t);
            i++;
            if(i == 3)
                break;
        }

    }

    SECTION("Checking if the current player is destroyed by the current player, which implies 24 territory goal"
            "that isn't satisfied, should return false") {
        // Arrange
        p1->setMissionCard(mcard2);
        bool expected_val = false;

        // Act
        bool ret_val = p1->missionCard()->checkAssignment(p1);

        // Assert
        REQUIRE(ret_val == expected_val);

    }

    SECTION("Checking if the current player owns Asia and Africa, which he does, should return true") {
        // Arrange
        p1->setMissionCard(mcard3);
        foreach(auto &t, Initializer::allContinents["Asia"]->territories()) {
            t->setOwner(p1);
        }
        foreach(auto &t, Initializer::allContinents["Africa"]->territories()) {
            t->setOwner(p1);
        }

        bool expected_val = true;

        // Act
        bool ret_val = p1->missionCard()->checkAssignment(p1);

        // Assert
        REQUIRE(ret_val == expected_val);
    }

    SECTION("Checking if the current player owns Asia and Africa, which he doesn't, should return false") {
        // Arrange
        p1->setMissionCard(mcard3);
        foreach(auto &t, Initializer::allContinents["Asia"]->territories()) {
            t->setOwner(p2);
        }

        bool expected_val = false;

        // Act
        bool ret_val = p1->missionCard()->checkAssignment(p1);

        // Assert
        REQUIRE(ret_val == expected_val);
    }

    SECTION("Checking if the current player owns North America and Africa, which he does, should return true") {
        // Arrange
        p1->setMissionCard(mcard4);
        foreach(auto &t, Initializer::allContinents["North_America"]->territories()) {
            t->setOwner(p1);
        }
        foreach(auto &t, Initializer::allContinents["Africa"]->territories()) {
            t->setOwner(p1);
        }

        bool expected_val = true;

        // Act
        bool ret_val = p1->missionCard()->checkAssignment(p1);

        // Assert
        REQUIRE(ret_val == expected_val);
    }

    SECTION("Checking if the current player owns North America and Africa, which he doesn't, should return false") {
        // Arrange
        p1->setMissionCard(mcard4);
        foreach(auto &t, Initializer::allContinents["Africa"]->territories()) {
            t->setOwner(p2);
        }

        bool expected_val = false;

        // Act
        bool ret_val = p1->missionCard()->checkAssignment(p1);

        // Assert
        REQUIRE(ret_val == expected_val);
    }
    SECTION("Checking if the current player owns North America and Australia, which he does, should return true") {
        // Arrange
        p1->setMissionCard(mcard5);
        foreach(auto &t, Initializer::allContinents["North_America"]->territories()) {
            t->setOwner(p1);
        }
        foreach(auto &t, Initializer::allContinents["Australia"]->territories()) {
            t->setOwner(p1);
        }

        bool expected_val = true;

        // Act
        bool ret_val = p1->missionCard()->checkAssignment(p1);

        // Assert
        REQUIRE(ret_val == expected_val);
    }

    SECTION("Checking if the current player owns North America and Australia, which he doesn't, should return false") {
        // Arrange
        p1->setMissionCard(mcard5);
        foreach(auto &t, Initializer::allContinents["Australia"]->territories()) {
            t->setOwner(p2);
        }

        bool expected_val = false;

        // Act
        bool ret_val = p1->missionCard()->checkAssignment(p1);

        // Assert
        REQUIRE(ret_val == expected_val);
    }

    SECTION("Checking if the current player owns South America and Australia, which he does, should return true") {
        // Arrange
        p1->setMissionCard(mcard6);
        foreach(auto &t, Initializer::allContinents["South_America"]->territories()) {
            t->setOwner(p1);
        }
        foreach(auto &t, Initializer::allContinents["Australia"]->territories()) {
            t->setOwner(p1);
        }

        bool expected_val = true;

        // Act
        bool ret_val = p1->missionCard()->checkAssignment(p1);

        // Assert
        REQUIRE(ret_val == expected_val);
    }

    SECTION("Checking if the current player owns South America and Australia, which he doesn't, should return false") {
        // Arrange
        p1->setMissionCard(mcard6);
        foreach(auto &t, Initializer::allContinents["Australia"]->territories()) {
            t->setOwner(p2);
        }

        bool expected_val = false;

        // Act
        bool ret_val = p1->missionCard()->checkAssignment(p1);

        // Assert
        REQUIRE(ret_val == expected_val);
    }


    SECTION("Checking if the current player fulfilled his goal, which is a continent conquer card, should return true") {
        // Arrange
        p1->setMissionCard(mcard6);
        foreach(auto &t, Initializer::allContinents["South_America"]->territories()) {
            t->setOwner(p1);
        }
        foreach(auto &t, Initializer::allContinents["Australia"]->territories()) {
            t->setOwner(p1);
        }

        bool expected_val = true;

        // Act
        bool ret_val = game->checkGoal(p1);

        // Assert
        REQUIRE(ret_val == expected_val);
    }

    SECTION("Checking if the current player fulfilled his goal, which is a continent conquer card, should return false") {
        // Arrange
        p1->setMissionCard(mcard6);
        foreach(auto &t, Initializer::allContinents["South_America"]->territories()) {
            t->setOwner(p2);
        }


        bool expected_val = false;

        // Act
        bool ret_val = game->checkGoal(p1);

        // Assert
        REQUIRE(ret_val == expected_val);
    }

    SECTION("Checking if the current player fulfilled his goal, which is a continent conquer card, should return true") {
        // Arrange
        p1->setMissionCard(mcard5);
        foreach(auto &t, Initializer::allContinents["North_America"]->territories()) {
            t->setOwner(p1);
        }
        foreach(auto &t, Initializer::allContinents["Australia"]->territories()) {
            t->setOwner(p1);
        }

        bool expected_val = true;

        // Act
        bool ret_val = game->checkGoal(p1);

        // Assert
        REQUIRE(ret_val == expected_val);
    }

    SECTION("Checking if the current player fulfilled his goal, which is a continent conquer card, should return false") {
        // Arrange
        p1->setMissionCard(mcard5);
        foreach(auto &t, Initializer::allContinents["North_America"]->territories()) {
            t->setOwner(p2);
        }


        bool expected_val = false;

        // Act
        bool ret_val = game->checkGoal(p1);

        // Assert
        REQUIRE(ret_val == expected_val);
    }

    SECTION("Checking if the current player fulfilled his goal, which is a continent conquer card, should return true") {
        // Arrange
        p1->setMissionCard(mcard4);
        foreach(auto &t, Initializer::allContinents["North_America"]->territories()) {
            t->setOwner(p1);
        }
        foreach(auto &t, Initializer::allContinents["Africa"]->territories()) {
            t->setOwner(p1);
        }

        bool expected_val = true;

        // Act
        bool ret_val = game->checkGoal(p1);

        // Assert
        REQUIRE(ret_val == expected_val);
    }

    SECTION("Checking if the current player fulfilled his goal, which is a continent conquer card, should return false") {
        // Arrange
        p1->setMissionCard(mcard4);
        foreach(auto &t, Initializer::allContinents["North_America"]->territories()) {
            t->setOwner(p2);
        }


        bool expected_val = false;

        // Act
        bool ret_val = game->checkGoal(p1);

        // Assert
        REQUIRE(ret_val == expected_val);
    }

    SECTION("Checking if the current player fulfilled his goal, which is a destroy troops card, should return true") {
        // Arrange
        p1->setMissionCard(mcard1);
        p2->setDestroyer(p1);

        bool expected_val = true;

        // Act
        bool ret_val = game->checkGoal(p1);

        // Assert
        REQUIRE(ret_val == expected_val);
    }

    SECTION("Checking if the current player fulfilled his goal, which is a destroy troops card, should return false") {
        // Arrange
        p1->setMissionCard(mcard1);

        bool expected_val = false;

        // Act
        bool ret_val = game->checkGoal(p1);

        // Assert
        REQUIRE(ret_val == expected_val);
    }


    delete game;
}
