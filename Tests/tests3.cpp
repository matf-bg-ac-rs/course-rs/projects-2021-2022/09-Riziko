#include <catch2/catch.hpp>
#include "../server/game.h"
#include "../server/initializer.h"
#include "../server/player.h"
#include "../server/card.h"
#include "../server/cardterritoryconquer.h"
#include "../server/settings.h"
#include "../server/territory.h"
#include "../server/missioncard.h"
#include "../server/continent.h"
#include "../server/cardcontinentconquer.h"
#include "../server/carddestroytroops.h"

TEST_CASE("Calculate bonus tanks and decrease number of tanks", "[bonus][decrease]")
{

    Player *p1 = new Player("Player1", Color::RED, true);
    Player *p2 = new Player("Player2", Color::BLUE, false);

    QVector<Player*> players = {p1,p2};

    Goal gg = Goal::CARDS;

    Settings* s = new Settings(2, 5, gg, players);
    auto continents = Initializer::initializeContinents();
    auto cards = Initializer::initializeCards();

    auto mcard1 = new CardTerritoryConquer(":/resources/images/MissionCards/Conquer-18",18);
    auto mcard2 = new CardTerritoryConquer(":/resources/images/MissionCards/Conquer-24",24);

    p1->setMissionCard(mcard1);
    p2->setMissionCard(mcard2);

    Game *game = new Game(cards, continents, s);
    game->start();

    foreach(auto t, p1->territories()) {
       p1->removeTerritory(t);
       t->setOwner(p2);
    }

    SECTION("Successful calculate bonus tanks having Australia, result should be 4."){
        // arrange
        int32_t expectedBonusTanks = 4;
        foreach(auto c, continents) {
            if(c->name() == "Australia") {
                foreach(auto t, c->territories()) {
                    t->setOwner(p1);
                    p1->addTerritory(t);
                }
                break;
            }
        }

        // act
        int32_t result = game->calculateBonusTanks(p1);

        // assert
        REQUIRE(result == expectedBonusTanks);

        foreach(auto t, p1->territories()) {
           p1->removeTerritory(t);
           t->setOwner(p2);
        }
    }

    SECTION("Successful calculate bonus tanks having Asia, result should be 11."){
        // arrange
        int32_t expectedBonusTanks = 11;
        foreach(auto c, continents) {
            if(c->name() == "Asia") {
                foreach(auto t, c->territories()) {
                    t->setOwner(p1);
                    p1->addTerritory(t);
                }
                break;
            }
        }

        // act
        int32_t result = game->calculateBonusTanks(p1);

        // assert
        REQUIRE(result == expectedBonusTanks);

        foreach(auto t, p1->territories()) {
           p1->removeTerritory(t);
           t->setOwner(p2);
        }
    }

    SECTION("Successful calculate bonus tanks having North America, result should be 8."){
        // arrange
        int32_t expectedBonusTanks = 8;
        foreach(auto c, continents) {
            if(c->name() == "North_America") {
                foreach(auto t, c->territories()) {
                    t->setOwner(p1);
                    p1->addTerritory(t);
                }
                break;
            }
        }

        // act
        int32_t result = game->calculateBonusTanks(p1);

        // assert
        REQUIRE(result == expectedBonusTanks);

        foreach(auto t, p1->territories()) {
           p1->removeTerritory(t);
           t->setOwner(p2);
        }
    }

    SECTION("Successful calculate bonus tanks having Europe, result should be 8."){
        // arrange
        int32_t expectedBonusTanks = 8;
        foreach(auto c, continents) {
            if(c->name() == "Europe") {
                foreach(auto t, c->territories()) {
                    t->setOwner(p1);
                    p1->addTerritory(t);
                }
                break;
            }
        }

        // act
        int32_t result = game->calculateBonusTanks(p1);

        // assert
        REQUIRE(result == expectedBonusTanks);

        foreach(auto t, p1->territories()) {
           p1->removeTerritory(t);
           t->setOwner(p2);
        }
    }

    SECTION("Successful calculate bonus tanks having Africa, result should be 5."){
        // arrange
        int32_t expectedBonusTanks = 5;
        foreach(auto c, continents) {
            if(c->name() == "Africa") {
                foreach(auto t, c->territories()) {
                    t->setOwner(p1);
                    p1->addTerritory(t);
                }
                break;
            }
        }

        // act
        int32_t result = game->calculateBonusTanks(p1);

        // assert
        REQUIRE(result == expectedBonusTanks);

        foreach(auto t, p1->territories()) {
           p1->removeTerritory(t);
           t->setOwner(p2);
        }
    }

    SECTION("Successful calculate bonus tanks having South America, result should be 4."){
        // arrange
        int32_t expectedBonusTanks = 4;
        foreach(auto c, continents) {
            if(c->name() == "South_America") {
                foreach(auto t, c->territories()) {
                    t->setOwner(p1);
                    p1->addTerritory(t);
                }
                break;
            }
        }

        // act
        int32_t result = game->calculateBonusTanks(p1);

        // assert
        REQUIRE(result == expectedBonusTanks);

        foreach(auto t, p1->territories()) {
           p1->removeTerritory(t);
           t->setOwner(p2);
        }
    }

    SECTION("Successful calculate bonus tanks having Australia and South America, result should be 7."){
        // arrange
        int32_t expectedBonusTanks = 7;
        foreach(auto c, continents) {
            if(c->name() == "Australia" || c->name() == "South_America") {
                foreach(auto t, c->territories()) {
                    t->setOwner(p1);
                    p1->addTerritory(t);
                }
            }
        }

        // act
        int32_t result = game->calculateBonusTanks(p1);

        // assert
        REQUIRE(result == expectedBonusTanks);

        foreach(auto t, p1->territories()) {
           p1->removeTerritory(t);
           t->setOwner(p2);
        }
    }

    SECTION("Successful calculate bonus tanks having Africa and Europe, result should be 13."){
        // arrange
        int32_t expectedBonusTanks = 13;
        foreach(auto c, continents) {
            if(c->name() == "Africa" || c->name() == "Europe") {
                foreach(auto t, c->territories()) {
                    t->setOwner(p1);
                    p1->addTerritory(t);
                }
            }
        }

        // act
        int32_t result = game->calculateBonusTanks(p1);

        // assert
        REQUIRE(result == expectedBonusTanks);

        foreach(auto t, p1->territories()) {
           p1->removeTerritory(t);
           t->setOwner(p2);
        }
    }

    SECTION("Successful calculate bonus tanks having North America and Asia, result should be 19."){
        // arrange
        int32_t expectedBonusTanks = 19;
        foreach(auto c, continents) {
            if(c->name() == "North_America" || c->name() == "Asia") {
                foreach(auto t, c->territories()) {
                    t->setOwner(p1);
                    p1->addTerritory(t);
                }
            }
        }

        // act
        int32_t result = game->calculateBonusTanks(p1);

        // assert
        REQUIRE(result == expectedBonusTanks);

        foreach(auto t, p1->territories()) {
           p1->removeTerritory(t);
           t->setOwner(p2);
        }
    }

    SECTION("Successful calculate bonus tanks having Australia and Asia, result should be 15."){
        // arrange
        int32_t expectedBonusTanks = 15;
        foreach(auto c, continents) {
            if(c->name() == "Australia" || c->name() == "Asia") {
                foreach(auto t, c->territories()) {
                    t->setOwner(p1);
                    p1->addTerritory(t);
                }
            }
        }

        // act
        int32_t result = game->calculateBonusTanks(p1);

        // assert
        REQUIRE(result == expectedBonusTanks);

        foreach(auto t, p1->territories()) {
           p1->removeTerritory(t);
           t->setOwner(p2);
        }
    }

    SECTION("Successful calculate bonus tanks having South America and North America, result should be 12."){
        // arrange
        int32_t expectedBonusTanks = 12;
        foreach(auto c, continents) {
            if(c->name() == "South_America" || c->name() == "North_America") {
                foreach(auto t, c->territories()) {
                    t->setOwner(p1);
                    p1->addTerritory(t);
                }
            }
        }

        // act
        int32_t result = game->calculateBonusTanks(p1);

        // assert
        REQUIRE(result == expectedBonusTanks);

        foreach(auto t, p1->territories()) {
           p1->removeTerritory(t);
           t->setOwner(p2);
        }
    }

    SECTION("Successful calculate bonus tanks having Africa and Europe, result should be 13."){
        // arrange
        int32_t expectedBonusTanks = 13;
        foreach(auto c, continents) {
            if(c->name() == "Africa" || c->name() == "Europe") {
                foreach(auto t, c->territories()) {
                    t->setOwner(p1);
                    p1->addTerritory(t);
                }
            }
        }

        // act
        int32_t result = game->calculateBonusTanks(p1);

        // assert
        REQUIRE(result == expectedBonusTanks);

        foreach(auto t, p1->territories()) {
           p1->removeTerritory(t);
           t->setOwner(p2);
        }
    }

    SECTION("Successful calculate bonus tanks having Europe, Asia and Australia, result should be 22."){
        // arrange
        int32_t expectedBonusTanks = 22;
        foreach(auto c, continents) {
            if(c->name() == "Europe" || c->name() == "Asia" || c->name() == "Australia") {
                foreach(auto t, c->territories()) {
                    t->setOwner(p1);
                    p1->addTerritory(t);
                }
            }
        }

        // act
        int32_t result = game->calculateBonusTanks(p1);

        // assert
        REQUIRE(result == expectedBonusTanks);

        foreach(auto t, p1->territories()) {
           p1->removeTerritory(t);
           t->setOwner(p2);
        }
    }

    SECTION("Successful calculate bonus tanks having Africa, North America and South America, result should be 17."){
        // arrange
        int32_t expectedBonusTanks = 17;
        foreach(auto c, continents) {
            if(c->name() == "Africa" || c->name() == "North_America" || c->name() == "South_America") {
                foreach(auto t, c->territories()) {
                    t->setOwner(p1);
                    p1->addTerritory(t);
                }
            }
        }

        // act
        int32_t result = game->calculateBonusTanks(p1);

        // assert
        REQUIRE(result == expectedBonusTanks);

        foreach(auto t, p1->territories()) {
           p1->removeTerritory(t);
           t->setOwner(p2);
        }
    }

    SECTION("Successful calculate bonus having random 12 territories from all continents, result should be 4"){
        // arrange
        int32_t expectedBonusTanks = 4;
        for (int i = 0; i < 2; ++i) {
            foreach(auto c, continents) {
                foreach(auto t, c->territories()) {
                    if (t->owner() != p1) {
                        p1->addTerritory(t);
                        t->setOwner(p1);
                        break;
                    }
                }
            }
       }

        // act
        int32_t result = game->calculateBonusTanks(p1);

        // assert
        REQUIRE(result == expectedBonusTanks);

        foreach(auto t, p1->territories()) {
           p1->removeTerritory(t);
           t->setOwner(p2);
        }
    }

    SECTION("Successful calculate bonus having 1 territory, result should be 1"){
        // arrange
        int32_t expectedBonusTanks = 1;
        Territory Alaska("Alaska");
        p1->addTerritory(&Alaska);

        // act
        int32_t result = game->calculateBonusTanks(p1);

        // assert
        REQUIRE(result == expectedBonusTanks);

        p1->removeTerritory(&Alaska);
    }

    SECTION("Successful calculate bonus having random 4 territories, result should be 2, but because all of them are from Australia it gives +2 bonus tanks for that"){
        // arrange
        int32_t expectedBonusTanks = 4;
        int32_t i = 4;
        foreach(auto c, continents) {
            if (c->name() == "Australia") {
                foreach(auto t, c->territories()) {
                    p1->addTerritory(t);
                    t->setOwner(p1);
                    --i;
                }
            }
            if (!i)
                break;
        }

        // act
        int32_t result = game->calculateBonusTanks(p1);

        // assert
        REQUIRE(result == expectedBonusTanks);

        foreach(auto t, p1->territories()) {
           p1->removeTerritory(t);
           t->setOwner(p2);
        }
    }

    SECTION("Successful calculate bonus having Australia, Europe, Asia and Africa, result should be 28"){
        // arrange
        int32_t expectedBonusTanks = 28;
        foreach(auto c, continents) {
            if(c->name() == "Australia" || c->name() == "Europe" || c->name() == "Asia" || c->name() == "Africa") {
                foreach(auto t, c->territories()) {
                    t->setOwner(p1);
                    p1->addTerritory(t);
                }
            }
        }

        // act
        int32_t result = game->calculateBonusTanks(p1);

        // assert
        REQUIRE(result != expectedBonusTanks);

        foreach(auto t, p1->territories()) {
           p1->removeTerritory(t);
           t->setOwner(p2);
        }
    }

    SECTION("Successful decreasing number of tanks from 10 to 9, result should be true"){
        // arrange
        Territory Alaska("Alaska");
        Alaska.setNumberOfTanks(10);
        bool expectedResult = true;

        // act
        bool result = Alaska.decreaseNumOfTanks(1);

        // assert
        REQUIRE(result == expectedResult);
    }

    SECTION("Unsuccessful decreasing number of tanks from 1 to -1, result should be false"){
        // arrange
        Territory Alaska("Alaska");
        Alaska.setNumberOfTanks(1);
        bool expectedResult = false;

        // act
        bool result = Alaska.decreaseNumOfTanks(2);

        // assert
        REQUIRE(result == expectedResult);
    }

    delete game;
}
