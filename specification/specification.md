# Dijagram komponenti

[Dijagram komponenti](component_diagram.pdf)

# Dijagram slucajeva upotrebe

[Dijagram slucajeva upotrebe](use_case_diagram.pdf)

# Igranje jedne partije igre

**Kratak opis**: <br />
Vlasnik sobe zapocinje partiju iz glavnog menija nakon sto je prisutan potreban broj igraca. Aplikacija ucitava podesavanja i zapocinje partiju. Partija traje dok jedan od igraca ne ispuni cilj igre, a zatim se igraci vracaju u pocetnu sobu.

**Akteri**: Igraci.

**Preduslovi**: Aplikacija je pokrenuta i prikazuje glavni meni.

**Postuslovi**: Igraci su vraceni u pocetnu sobu.

**Osnovni tok**:
1. Vlasnik sobe bira dugme "Zapocni igru" iz glavnog menija
2. Aplikacija dohvata informacije o podesavanjima partije
3. Aplikacija konstruise novu partiju i zapocinje je
4. Aplikacija dodeljuje pocetne teritorije igracima i bira redosled odigravanja poteza
5. Igraci naizmenicno po zadatom redosledu rasporedjuju pocetne tenkove na svoje teritorije
6. Ukoliko je cilj igre izvrsavanje zadatka, svaki igrac dobija svoju karticu sa zadatkom
7. Sve dok jedan od igraca ne ispuni cilj igre, ponavljaju se naredni koraci:
	* 7.1. Ukoliko je prva runda igre, ova faza se preskace. Inace, prelazi se na SU Dodeljivanje i rasporedjivanje armija
    * 7.2. Ukoliko zeli, igrac na potezu bira sa koje svoje teritorije zeli da          napadne i koju teritoriju zeli da napadne. One se moraju graniciti ili        biti povezane linijom preko mora. Potrebno je da na teritoriji sa koje        se napada budu bar 2 tenka.
		* 7.2.1. Igrac koji napada u zavisnosti od broja tenkova baca odredjeni broj kockica. Ako ima 2 tenka to je jedna kockica, ako ima 3 to su dve, a ako ima 4 ili vise baca tri kockice.
		* 7.2.2. Igrac koji se brani u zavisnosti od broja tenkova baca odredjeni broj kockica. Ako ima 1 tenk to je jedna kockica, ako ima 2 to su dve, a ako ima 3 ili vise baca tri kockice.
		* 7.2.3. Kockice se sortiraju i uparuju, pri cemu ukoliko jedna strana ima vise kockica njene najnize kockice se odbacuju.
		* 7.2.4. Za svaku vecu kockicu u paru protivnickom igracu se skida jedan tenk sa teritorije. Ukoliko su upareni brojevi isti, napadacu se skida jedan tenk.
		* 7.2.5. Ukoliko igrac zeli da prekine napad, moze to uraditi nakon svakog bacanja kockica.	
		* 7.2.6. Ukoliko protivnickom igracu u nekom trenutku ostane 0 tenkova na teritoriji, napadac zauzima tu teritoriju i na nju mora staviti barem onoliko tenkova sa koliko je napao.
	* 7.3. Nakon zavrsetka faze napada, igrac na potezu moze sa jedne od svojih teritorija premestiti na sve njoj susedne teritorije odredjeni broj tenkova. 
	* 7.4. Ukoliko je u fazi napada igrac na potezu osvojio barem jednu teritoriju, on vuce karticu koju moze zameniti za dodatne tenkove.
	* 7.5. Zavrsava se potez tekuceg igraca i na potez dolazi sledeci igrac po redosledu sa pocetka partije.
	* 7.6. Ukoliko neki od igraca nema vise tenkova, njegov potez se preskace.	
8. Ispisuje se ime pobednika i igraci se vracaju u pocetnu sobu.


**Alternativni tokovi**: <br />
-A1: Neocekivani izlaz iz aplikacije. Ukoliko neki od igraca u toku partije izadje iz aplikacije, njegove poteze odigrava bot koji nasumicno dodaje tenkove na pocetku svog poteza, a zatim ga zavrsava.
-A2: Izlazak iz aplikacije vlasnika sobe. Ukoliko vlasnik sobe u bilo kom trenutku izadje iz aplikacije, od preostalih igraca se bira nasumicno novi vlasnik sobe.

**Podtokovi**: /

**Specijalni zahtevi**: <br />
Svaki od igraca mora biti povezan na internet i posedovati kod sebe klijent aplikacije. Server aplikacije mora biti pokrenut u trenutku pokretanja partije.

**Dodatne informacije**: <br />
Cilj igre može biti:
- World domination ( Zauzeti sve teritorije )
- Ispunjavanje zadatka sa kartice
- Osvajanje zadatog broja teritorija ( 24 ili 18 sa po dve armije )
- Eliminisanje određenog igrača
- Osvajanje zadatih kontinenata ( npr. Azija i Afrika )

[Dijagram sekvence: `Igranje jedne partije`](playing_one_game_sequence_diagram.pdf)

# Dodeljivanje i rasporedjivanje armija

**Kratak opis**: Igracu se dodeljuje odredjeni broj tenkova na osnovu teritorija koje poseduje. 
            	 Daje mu se mogucnost da zameni kartice za dodatne tenkove, a zatim se od njega trazi da dodeljene tenkove rasporedi na svoje teritorije.

**Akteri**:
Igrac na potezu.

**Preduslovi**: Aplikacija je pokrenuta i partija je u toku.

**Postuslovi**: Igrac je rasporedio svoje tenkove i spreman je za fazu napada.

**Osnovni tok**:
1. Igrac na potezu rasporedjuje na svoje teritorije dodatne tenkove. 
    * 1.1. Za svake 3 teritorije dodeljuje se jedan dodatni tenk.
    * 1.2. Za svaki kontinent koji igrac poseduje u celosti dodeljuje se broj tenkova odredjen tabelom u dodatnim informacijama.
    * 1.3. Ukoliko igrac poseduje kartice koje moze da zameni za dodatne tenkove, ta zamena se vrsi po tabeli u dodatnim informacijama. 
        *  1.3.1. Ukoliko igrac poseduje neku od teritorija sa kartica za nju dobija jos jedan dodatni tenk.


**Alternativni tokovi**: /

**Podtokovi**: /

**Specijalni zahtevi**: <br />
Igrac na potezu mora biti povezan na internet i posedovati kod sebe klijent aplikacije. 
Server aplikacije mora biti pokrenut u trenutku pokretanja partije. 
Igrac mora biti u partiji.

**Dodatne informacije**: 


Tabela kontinenata:


| Kontinent       	| Broj tenkova 	 |
|-----------------	|:--------------:|
| Australija      	| 2            	 |
| Juzna Amerika   	| 2            	 |
| Afrika          	| 3            	 |
| Evropa          	| 5            	 |
| Severna Amerika 	| 5            	 |
| Azija           	| 7            	 |


Tabela kartica:

| Kartice                 	| Broj tenkova 	|
|-------------------------	|:------------:	|
| 3 pesadinca             	|       4      	|
| 3 konjanika             	|       6      	|
| 3 topa                  	|       8      	|
| 3 razlicite kartice     	|      10      	|
| 2 iste kartice i dzoker 	|      12      	|

[Dijagram sekvence: `Dodeljivanje i rasporedjivanje armija`](assigning_deploying_tanks_sequence_diagram.pdf)

# Podesavanje jedne partije igre

**Kratak opis**: Vlasnik sobe podesava broj igraca, pocetni broj tenkova i cilj igre. Svaki pojedinacni igrac bira boju i ime.

**Akteri**:
Igraci.

**Preduslovi**: Aplikacija je pokrenuta i prikazuje glavni meni.

**Postuslovi**: Izvrsena su podesavanja jedne partije i vlasnik sobe moze da pokrene igru.

**Osnovni tok**:
1. Vlasnik sobe bira dugme "Podesi broj igraca" iz glavnog menija ukoliko zeli da promeni broj igraca (podrazumevano je to 2, moze biti od 2 do 5). 
   Potrebno je da unese taj broj u textBox pored dugmeta.
2. Vlasnik sobe bira dugme "Podesi pocetni broj tenkova" iz glavnog menija ukoliko zeli da promeni 
   broj tenkova (podrazumevano je to 40). Potrebno je da unese taj broj u textBox pored dugmeta.
3. Vlasnik sobe bira dugme "Podesi cilj igre" iz glavnog menija ukoliko zeli da promeni 
   cilj igre (podrazumevano je to World domination). Potrebno je da izabere opciju u meniju pored dugmeta.	
4. Igracima redom izlazi prozor za unos boje armije (podrazumevane boje su plava, crvena, zuta, zelena i crna po redosledu ulaska u sobu) i postavljanje korisnickog imena.

**Alternativni tokovi**:<br />
A1: Izlazak iz aplikacije vlasnika sobe. Ukoliko vlasnik sobe u bilo kom trenutku izadje iz aplikacije, od preostalih igraca se bira nasumicno novi vlasnik sobe.

**Podtokovi**: /

**Specijalni zahtevi**:<br />
Svaki od igraca mora biti povezan na internet i posedovati kod sebe klijent aplikacije. Server aplikacije mora biti pokrenut u trenutku podesavanja partije.

**Dodatne informacije**: /

[Dijagram sekvence: `Podesavanje jedne partije igre`](settings_sequence_diagram.pdf)

# Povezivanje igraca na server

**Kratak opis**: Pri pokretanju aplikacije, klijenti salju TCP zahtev za povezivanje na server i nakon uspesnog povezivanja pokrece se glavni meni.

**Akteri**: Klijenti.

**Preduslovi**: /

**Postuslovi**:Igrac je povezan na server i otvoren je glavni meni.

**Osnovni tok**:
1. Pri pokretanju aplikacije klijent salje serveru TCP zahtev za povezivanje.
2. Server odobrava taj zahtev i proverava da li je klijent prvi koji se povezuje na server. Ukoliko jeste taj igrac dobija status vlasnika sobe.
3. Uspostavlja se TCP konekcija i otvara se glavni meni.


**Alternativni tokovi**: <br />
A1: Prikazuje se prozor sa porukom o neuspesnom povezivanju i dugmetom za ponovni pokusaj uspostavljanja konekcije.

**Podtokovi**: /

**Specijalni zahtevi**: <br />
Svaki od igraca mora biti povezan na internet i posedovati kod sebe klijent aplikacije. Server aplikacije mora biti pokrenut u trenutku zahtevanja konekcije.

**Dodatne informacije**: /

[Dijagram sekvence: `Povezivanje igraca na server`](connecting_player_to_server_sequence_diagram.pdf)

# Dijagram klasa

[Pocetni dijagram klasa](class_diagram.pdf)

[Finalni dijagram klasa](final_class_diagram.pdf)


