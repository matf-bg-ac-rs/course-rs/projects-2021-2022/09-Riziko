# Podesavanje jedne partije igre

**Kratak opis**: Vlasnik sobe podesava broj igraca, pocetni broj tenkova i cilj igre. Svaki pojedinacni igrac bira boju i ime.

**Akteri**:
Igraci.

**Preduslovi**: Aplikacija je pokrenuta i prikazuje glavni meni.

**Postuslovi**: Izvrsena su podesavanja jedne partije i vlasnik sobe moze da pokrene igru.

**Osnovni tok**:
1. Vlasnik sobe bira dugme "Podesi broj igraca" iz glavnog menija ukoliko zeli da promeni broj igraca (podrazumevano je to 2, moze biti od 2 do 5). 
   Potrebno je da unese taj broj u textBox pored dugmeta.
2. Vlasnik sobe bira dugme "Podesi pocetni broj tenkova" iz glavnog menija ukoliko zeli da promeni 
   broj tenkova (podrazumevano je to 40). Potrebno je da unese taj broj u textBox pored dugmeta.
3. Vlasnik sobe bira dugme "Podesi cilj igre" iz glavnog menija ukoliko zeli da promeni 
   cilj igre (podrazumevano je to World domination). Potrebno je da izabere opciju u meniju pored dugmeta.	
4. Igracima redom izlazi prozor za unos boje armije (podrazumevane boje su plava, crvena, zuta, zelena i crna po redosledu ulaska u sobu) i postavljanje korisnickog imena.


**Alternativni tokovi**:<br />
A1: Izlazak iz aplikacije vlasnika sobe. Ukoliko vlasnik sobe u bilo kom trenutku izadje iz aplikacije, od preostalih igraca se bira nasumicno novi vlasnik sobe.

**Podtokovi**: /

**Specijalni zahtevi**:<br />
Svaki od igraca mora biti povezan na internet i posedovati kod sebe klijent aplikacije. Server aplikacije mora biti pokrenut u trenutku podesavanja partije.

**Dodatne informacije**: /

