# Dodeljivanje i rasporedjivanje armija

**Kratak opis**: Igracu se dodeljuje odredjeni broj tenkova na osnovu teritorija koje poseduje. 
                 Daje mu se mogucnost da zameni kartice za dodatne tenkove, a zatim se od njega trazi da dodeljene tenkove rasporedi na svoje teritorije.

**Akteri**:
Igrac na potezu.

**Preduslovi**: Aplikacija je pokrenuta i partija je u toku.

**Postuslovi**: Igrac je rasporedio svoje tenkove i spreman je za fazu napada.

**Osnovni tok**:
1. Igrac na potezu rasporedjuje na svoje teritorije dodatne tenkove. 
    * 1.1. Za svake 3 teritorije dodeljuje se jedan dodatni tenk.
    * 1.2. Za svaki kontinent koji igrac poseduje u celosti dodeljuje se broj tenkova odredjen tabelom u dodatnim informacijama.
    * 1.3. Ukoliko igrac poseduje kartice koje moze da zameni za dodatne tenkove, ta zamena se vrsi po tabeli u dodatnim informacijama. 
        * 1.3.1. Ukoliko igrac poseduje neku od teritorija sa kartica za nju dobija jos jedan dodatni tenk.


**Alternativni tokovi**: /

**Podtokovi**: /

**Specijalni zahtevi**: Igrac na potezu mora biti povezan na internet i posedovati kod sebe klijent aplikacije. 
                        Server aplikacije mora biti pokrenut u trenutku pokretanja partije.
                        Igrac mora biti u partiji.

**Dodatne informacije**: 


Tabela kontinenata:


| Kontinent       	| Broj tenkova 	 |
|-----------------	|:--------------:|
| Australija      	| 2            	 |
| Juzna Amerika   	| 2            	 |
| Afrika          	| 3            	 |
| Evropa          	| 5            	 |
| Severna Amerika 	| 5            	 |
| Azija           	| 7            	 |


Tabela kartica:

| Kartice                 	| Broj tenkova 	|
|-------------------------	|:------------:	|
| 3 pesadinca             	|       4      	|
| 3 konjanika             	|       6      	|
| 3 topa                  	|       8      	|
| 3 razlicite kartice     	|      10      	|
| 2 iste kartice i dzoker 	|      12      	|
