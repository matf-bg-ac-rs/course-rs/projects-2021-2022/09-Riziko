# Igranje jedne partije igre

**Kratak opis**: <br />
Vlasnik sobe zapocinje partiju iz glavnog menija nakon sto je prisutan potreban broj igraca. Aplikacija ucitava podesavanja i zapocinje partiju. Partija traje dok jedan od igraca ne ispuni cilj igre, a zatim se igraci vracaju u pocetnu sobu.

**Akteri**: Igraci.

**Preduslovi**: Aplikacija je pokrenuta i prikazuje glavni meni.

**Postuslovi**: Igraci su vraceni u pocetnu sobu.

**Osnovni tok**:
1. Vlasnik sobe bira dugme "Zapocni igru" iz glavnog menija
2. Aplikacija dohvata informacije o podesavanjima partije
3. Aplikacija konstruise novu partiju i zapocinje je
4. Aplikacija dodeljuje pocetne teritorije igracima i bira redosled odigravanja poteza
5. Igraci naizmenicno po zadatom redosledu rasporedjuju pocetne tenkove na svoje teritorije
6. Ukoliko je cilj igre izvrsavanje zadatka, svaki igrac dobija svoju karticu sa zadatkom
7. Sve dok jedan od igraca ne ispuni cilj igre, ponavljaju se naredni koraci:
	* 7.1. Ukoliko je prva runda igre, ova faza se preskace. Inace, prelazi se na SU Dodeljivanje i rasporedjivanje armija
    * 7.2. Ukoliko zeli, igrac na potezu bira sa koje svoje teritorije zeli da          napadne i koju teritoriju zeli da napadne. One se moraju graniciti ili        biti povezane linijom preko mora. Potrebno je da na teritoriji sa koje        se napada budu bar 2 tenka.
		* 7.2.1. Igrac koji napada u zavisnosti od broja tenkova baca odredjeni broj kockica. Ako ima 2 tenka to je jedna kockica, ako ima 3 to su dve, a ako ima 4 ili vise baca tri kockice.
		* 7.2.2. Igrac koji se brani u zavisnosti od broja tenkova baca odredjeni broj kockica. Ako ima 1 tenk to je jedna kockica, ako ima 2 to su dve, a ako ima 3 ili vise baca tri kockice.
		* 7.2.3. Kockice se sortiraju i uparuju, pri cemu ukoliko jedna strana ima vise kockica njene najnize kockice se odbacuju.
		* 7.2.4. Za svaku vecu kockicu u paru protivnickom igracu se skida jedan tenk sa teritorije. Ukoliko su upareni brojevi isti, napadacu se skida jedan tenk.
		* 7.2.5. Ukoliko igrac zeli da prekine napad, moze to uraditi nakon svakog bacanja kockica.	
		* 7.2.6. Ukoliko protivnickom igracu u nekom trenutku ostane 0 tenkova na teritoriji, napadac zauzima tu teritoriju i na nju mora staviti barem onoliko tenkova sa koliko je napao.
	* 7.3. Nakon zavrsetka faze napada, igrac na potezu moze sa jedne od svojih teritorija premestiti na sve njoj susedne teritorije odredjeni broj tenkova. 
	* 7.4. Ukoliko je u fazi napada igrac na potezu osvojio barem jednu teritoriju, on vuce karticu koju moze zameniti za dodatne tenkove.
	* 7.5. Zavrsava se potez tekuceg igraca i na potez dolazi sledeci igrac po redosledu sa pocetka partije.
	* 7.6. Ukoliko neki od igraca nema vise tenkova, njegov potez se preskace.	
8. Ispisuje se ime pobednika i igraci se vracaju u pocetnu sobu.


**Alternativni tokovi**: <br />
-A1: Neocekivani izlaz iz aplikacije. Ukoliko neki od igraca u toku partije izadje iz aplikacije, njegove poteze odigrava bot koji nasumicno dodaje tenkove na pocetku svog poteza, a zatim ga zavrsava.
-A2: Izlazak iz aplikacije vlasnika sobe. Ukoliko vlasnik sobe u bilo kom trenutku izadje iz aplikacije, od preostalih igraca se bira nasumicno novi vlasnik sobe.

**Podtokovi**: /

**Specijalni zahtevi**: <br />
Svaki od igraca mora biti povezan na internet i posedovati kod sebe klijent aplikacije. Server aplikacije mora biti pokrenut u trenutku pokretanja partije.

**Dodatne informacije**: <br />
Cilj igre može biti:
- World domination ( Zauzeti sve teritorije )
- Ispunjavanje zadatka sa kartice
- Osvajanje zadatog broja teritorija ( 24 ili 18 sa po dve armije )
- Eliminisanje određenog igrača
- Osvajanje zadatih kontinenata ( npr. Azija i Afrika )
