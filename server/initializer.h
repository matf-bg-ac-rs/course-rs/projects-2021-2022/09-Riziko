#ifndef INITIALIZER_H
#define INITIALIZER_H

#include "continent.h"
#include "card.h"
#include "missioncard.h"
#include<algorithm>
#include<fstream>
#include<iostream>
#include<QHash>
#include<QVector>
#include <stdio.h>

#include <QString>
#include <QFile>
#include <QTextStream>


class Initializer
{
private:
    static QSet<Territory*> makeAdjTerritories(QStringList&, QHash<QString, Territory*>&);

public:
    Initializer();
    inline static QHash<QString, Territory*> allTerritories;
    inline static QHash<QString, Continent*> allContinents;
    inline static QHash<QString, Card*> allCards;

    static QSet<Continent*> initializeContinents();
    static QSet<Card*> initializeCards();
    static QSet<MissionCard*> initializeMissionCards(QVector<Player*>&);

};

#endif // INITIALIZER_H
