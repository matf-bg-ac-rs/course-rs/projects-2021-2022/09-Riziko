#ifndef SETTINGS_H
#define SETTINGS_H

#include<iostream>
#include<QVector>

class Player;

enum class Goal {
    WORLD_DOMINATION,
    CARDS
};

class Settings
{
private:
    std::int32_t m_numOfPlayers;
    std::int32_t m_numOfStartingTanks;
    Goal m_goal;
    QVector<Player*> m_players;

public:
    Settings();
    Settings(std::int32_t numOfPlayers, std::int32_t numOfStartingTanks, Goal goal, QVector<Player *> &players);
    ~Settings();

    std::int32_t numOfPlayers() const;
    std::int32_t numOfStartingTanks() const;
    Goal goal() const;
    QVector<Player *> &players();
};

#endif // SETTINGS_H
