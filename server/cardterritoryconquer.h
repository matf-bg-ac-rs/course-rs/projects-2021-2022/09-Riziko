#ifndef CARDTERRITORYCONQUER_H
#define CARDTERRITORYCONQUER_H

#include "missioncard.h"

class CardTerritoryConquer : public MissionCard
{
private:
    std::int32_t m_numberOfTerritories;
public:
    CardTerritoryConquer();
    CardTerritoryConquer(const QString&, std::int32_t numberOfTerritories);
    bool checkAssignment(Player* p) const override;
    std::int32_t numberOfTerritories() const;
    ~CardTerritoryConquer() { }
};

#endif // CARDTERRITORYCONQUER_H
