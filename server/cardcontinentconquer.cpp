#include "cardcontinentconquer.h"
#include "player.h"

CardContinentConquer::CardContinentConquer()
{

}

CardContinentConquer::~CardContinentConquer(){ }

bool CardContinentConquer::checkAssignment(Player* p) const
{
    if (m_c1->CheckOwner(p) && m_c2->CheckOwner(p)) {
        return true;
    }
    return false;
}

CardContinentConquer::CardContinentConquer(const QString &path, Continent *c1, Continent *c2) :
    MissionCard(path),
    m_c1(c1),
    m_c2(c2)
{}


