#ifndef CARD_H
#define CARD_H
#include "territory.h"
#include <iostream>
#include <QLabel>

enum class CardType:int {
    CANNON,
    CAVALRY,
    INFANTRY,
    JOKER
};

const QString cardTypeToStr(CardType);

class Card
{
public:
    Card();
    explicit Card(CardType cardType);
    Card(Territory *territory, const CardType &cardType);

    ~Card();

    Territory *territory() const;
    CardType cardType() const;

private:
    Territory *m_territory;
    CardType m_cardType;
};


#endif // CARD_H
