#include "initializer.h"
#include "cardterritoryconquer.h"
#include "cardcontinentconquer.h"
#include "carddestroytroops.h"
#include "player.h"

class MissionCard;
class CardDestroyTroops;

QSet<Territory *> Initializer::makeAdjTerritories(QStringList& words,
                                                                QHash<QString, Territory*>& all)
{
    QSet<Territory*> adj;
    for(auto &word : words){
        adj.insert(all[word]);
    }
    return adj;
}

Initializer::Initializer()
{

}

QSet<Continent *> Initializer::initializeContinents()
{
    QString terr_path(":/resources/territories/territories.txt");

    QFile inputFile(terr_path);

    QString line;
    QStringList words;

    QSet<Territory*> t_northAmerica;
    QSet<Territory*> t_southAmerica;
    QSet<Territory*> t_Europe;
    QSet<Territory*> t_Africa;
    QSet<Territory*> t_Asia;
    QSet<Territory*> t_Australia;
    QString name;
    std::int32_t cont;

    QSet<Continent *> continents;

    if(inputFile.open(QIODevice::ReadOnly)){
        QTextStream input(&inputFile);
        while(!input.atEnd()){
             line = input.readLine();
             words = line.split(',');

             name = words[1];

             Territory *t = new Territory(name);
             allTerritories[name] = t;
        }

        input.seek(0);

        while(!input.atEnd()){
             QString line = input.readLine();
             words = line.split(',');
             cont = words[0].toInt();
             name = words[1];
             words.erase(words.begin(), words.begin() + 2);

             QSet<Territory*> adjTerritories = makeAdjTerritories(words, allTerritories);

             Territory *t = allTerritories[name];
             t->setAdjTerritories(adjTerritories);

             switch (cont) {
             case 0:
                t_northAmerica.insert(t);
                break;
             case 1:
                t_southAmerica.insert(t);
                break;
             case 2:
                  t_Europe.insert(t);
                  break;
             case 3:
                  t_Africa.insert(t);
                  break;
             case 4:
                  t_Asia.insert(t);
                  break;
             case 5:
                  t_Australia.insert(t);
                  break;
            }
        }

        Continent *NorthAmerica = new Continent("North_America", t_northAmerica, 5);
        Continent *SouthAmerica = new Continent("South_America", t_southAmerica, 2);
        Continent *Europe = new Continent("Europe", t_Europe, 5);
        Continent *Africa = new Continent("Africa", t_Africa, 3);
        Continent *Asia = new Continent("Asia", t_Asia, 7);
        Continent *Australia = new Continent("Australia", t_Australia, 2);
        allContinents["North_America"] = NorthAmerica;
        allContinents["South_America"] = SouthAmerica;
        allContinents["Asia"] = Asia;
        allContinents["Africa"] = Africa;
        allContinents["Australia"] = Australia;
        allContinents["Europe"] = Europe;
        continents << NorthAmerica << SouthAmerica << Europe << Africa << Asia << Australia;
        inputFile.close();

    }
    else{
        std::cout << "Ne postoji fajl" << std::endl;
    }

    return continents;
}

QSet<Card *> Initializer::initializeCards()
{
    QString terr_path(":/resources/images/Cards/cards.txt");

    QFile inputFile(terr_path);


    QStringList words;
    QSet<Card *> cards;

    if(inputFile.open(QIODevice::ReadOnly)){
        QTextStream input(&inputFile);
        while(!input.atEnd()){
             QString line = input.readLine();
             words = line.split('-');

             QString name = words[0];
             QString type = words[1];
             CardType ct = CardType::JOKER;
             if(type == "cavalry")
                 ct = CardType::CAVALRY;
             else if (type == "infantry")
                 ct = CardType::INFANTRY;
             else if (type == "cannon")
                 ct = CardType::CANNON;

             Card* c;
             if (type == "joker")
                c = new Card(ct);
             else
                c = new Card(allTerritories[name], ct);

             allCards[name] = c;
             cards.insert(c);
        }
        inputFile.close();
    }
    else {
        std::cout << "Ne postoji fajl" << std::endl;
    }
    return cards;
}

QSet<MissionCard *> Initializer::initializeMissionCards(QVector<Player*>& m_players)
{
    QString terr_path(":/resources/images/MissionCards/MissionCards.txt");

    QFile inputFile(terr_path);

    QStringList words;
    QSet<MissionCard *> missionCards;

    if(inputFile.open(QIODevice::ReadOnly)){
        QTextStream input(&inputFile);
        while(!input.atEnd()){
             QString line = input.readLine();
             words = line.split('-');

             QString card_path = ":/resources/images/MissionCards/";

             MissionCard* mcard = nullptr;
             std::int32_t ct = words[0].toInt();
             if(ct == 0) {
                 std::int32_t num = words[2].toInt();
                 card_path += words[1] + "-" + words[2];
                 mcard = new CardTerritoryConquer(card_path,num);
                 missionCards.insert(mcard);
             }
             else if(ct == 1) {
               card_path += words[1] + "-" + words[2] + "-" + words[3];
                 mcard = new CardContinentConquer(card_path,allContinents[words[2]], allContinents[words[3]]);
                 missionCards.insert(mcard);
             }
             else {
                 for(int i = 0; i < m_players.size();i++) {
                     std::int32_t c = static_cast<int>(m_players[i]->color());
                     std::int32_t nc = words[2].toInt();
                     if(c == nc) {
                         card_path += words[1] + "-" + words[2];
                         mcard = new CardDestroyTroops(card_path,m_players[i]);
                         missionCards.insert(mcard);
                         break;
                     }
                 }
             }
        }
        inputFile.close();
    }
    else {
        std::cout << "Ne postoji fajl" << std::endl;
    }
    return missionCards;
}



