#ifndef PLAYER_H
#define PLAYER_H
#include<QString>
#include<QVector>
#include<QSet>

#include "card.h"
#include "territory.h"
#include "missioncard.h"

enum class Color{
    BLUE,
    RED,
    GREEN,
    YELLOW,
    BLACK,
    GRAY,
    WHITE
};

class Player
{
private:
    QString m_name;
    Color m_color;
    bool m_ownerInd;
    QSet<Card*> m_cards;
    Player* m_destroyer = nullptr;

    QSet<Territory*> m_territories;
    MissionCard* m_missionCard;
public:
    Player(const QString &name = "unnamed", Color color = Color::BLACK, bool ownerInd = false);
    ~Player();

    const QString &name() const;

    void addTerritory(Territory*);
    void removeTerritory(Territory*);
    void addCard(Card*);
    int exchangeCards(Card*, Card*, Card*);


    void setName(const QString &newName);
    Color color() const;
    void setColor(Color newColor);
    std::int32_t numOfTerritories() const;
    bool ownerInd() const;
    void setOwnerInd(bool newOwnerInd);
    Player* destroyer() const;
    void setDestroyer(Player* newDestroyer);

    const QSet<Territory *> &territories() const;
    void setTerritories(const QSet<Territory *> &newTerritories);
    QSet<Card *> &cards();
    void setCards(QSet<Card *> &newCards);
    MissionCard *missionCard() const;
    void setMissionCard(MissionCard *newMissionCard);
};

const QString colorToStr(Color);

#endif // PLAYER_H
