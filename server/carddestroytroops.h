#ifndef CARDDESTROYTROOPS_H
#define CARDDESTROYTROOPS_H

#include "missioncard.h"

class CardDestroyTroops : public MissionCard
{
private:
    Player* m_enemy;
    CardDestroyTroops(const CardDestroyTroops&);
    CardDestroyTroops& operator=(const CardDestroyTroops&);
public:
    CardDestroyTroops();
    CardDestroyTroops(const QString&, Player *enemy);
    bool checkAssignment(Player* p) const override;
    ~CardDestroyTroops();

    Player *enemy() const;
};

#endif // CARDDESTROYTROOPS_H
