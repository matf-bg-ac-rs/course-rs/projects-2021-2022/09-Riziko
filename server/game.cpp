#include "game.h"

const QVector<Player *> &Game::players() const
{
    return m_players;
}

QSet<Card *> &Game::cards()
{
    return m_cards;
}

const QSet<Continent *> &Game::continents() const
{
    return m_continents;
}

Player *Game::currentPlayer() const
{
    return m_currentPlayer;
}

void Game::setCurrentPlayer(Player *newCurrentPlayer)
{
    m_currentPlayer = newCurrentPlayer;
}

Settings *Game::settings() const
{
    return m_settings;
}

const QString &Game::currentlyAttacking() const
{
    return m_currentlyAttacking;
}

void Game::setCurrentlyAttacking(const QString &newCurrentlyAttacking)
{
    m_currentlyAttacking = newCurrentlyAttacking;
}

const QString &Game::currentlyAttacked() const
{
    return m_currentlyAttacked;
}

void Game::setCurrentlyAttacked(const QString &newCurrentlyAttacked)
{
    m_currentlyAttacked = newCurrentlyAttacked;
}

const QString &Game::currentlyTransfering() const
{
    return m_currentlyTransfering;
}

void Game::setCurrentlyTransfering(const QString &newCurrentlyTransfering)
{
    m_currentlyTransfering = newCurrentlyTransfering;
}

const QString &Game::currentlyTransfered() const
{
    return m_currentlyTransfered;
}

void Game::setCurrentlyTransfered(const QString &newCurrentlyTransfered)
{
    m_currentlyTransfered = newCurrentlyTransfered;
}

int Game::tanksToAdd() const
{
    return m_tanksToAdd;
}

void Game::setTanksToAdd(int newTanksToAdd)
{
    m_tanksToAdd = newTanksToAdd;
}

bool Game::startingPhase() const
{
    return m_startingPhase;
}

void Game::setStartingPhase(bool newStartingPhase)
{
    m_startingPhase = newStartingPhase;
}

bool Game::territoryConquered() const
{
    return m_territoryConquered;
}

void Game::setTerritoryConquered(bool newTerritoryConquered)
{
    m_territoryConquered = newTerritoryConquered;
}

int Game::numOfPlayersThatMoved() const
{
    return m_numOfPlayersThatMoved;
}

void Game::setNumOfPlayersThatMoved(int newNumOfPlayersThatMoved)
{
    m_numOfPlayersThatMoved = newNumOfPlayersThatMoved;
}

Game::Game()
{

}

void Game::start()
{
    std::int32_t currentlyAssigning = 0;

    foreach(auto &c , m_continents) {
        foreach(auto &t , c->territories()) {
            m_players[currentlyAssigning]->addTerritory(t);
            t->setOwner(m_players[currentlyAssigning]);
            if(currentlyAssigning == m_settings->numOfPlayers()-1)
                currentlyAssigning = 0;
            else
                currentlyAssigning++;
        }
    }
}

bool Game::checkGoal(Player *p)
{
    if (m_settings->goal() == Goal::WORLD_DOMINATION) {
        foreach(auto &c, m_continents) {
            foreach(auto &t, c->territories())
                if(t->owner() != p)
                    return false;
        }
        return true;
    }
    else {
        return (p->missionCard()->checkAssignment(p));
    }
}

int32_t Game::addTanksToTerritory(int32_t numOfTanks, Territory *t)
{
    t->increaseNumOfTanks(numOfTanks);
    return numOfTanks;
}

void Game::attack(Territory *attacker, Territory *defender, QStringList& dice)
{
    std::int32_t numOfTanksAttacker = attacker->numberOfTanks();
    std::int32_t numOfTanksDefender = defender->numberOfTanks();

    std::int32_t attackDie1 = QRandomGenerator::global()->bounded(1,7);
    std::int32_t attackDie2 = QRandomGenerator::global()->bounded(1,7);
    std::int32_t attackDie3 = QRandomGenerator::global()->bounded(1,7);
    std::int32_t defenseDie1 = QRandomGenerator::global()->bounded(1,7);
    std::int32_t defenseDie2 = QRandomGenerator::global()->bounded(1,7);
    std::int32_t defenseDie3 = QRandomGenerator::global()->bounded(1,7);

    if(numOfTanksAttacker == 2){
        attackDie2 = 0;
        attackDie3 = 0;
    }
    if(numOfTanksAttacker == 3){
        attackDie3 = 0;
    }
    if(numOfTanksDefender == 1){
        defenseDie2 = 0;
        defenseDie3 = 0;
    }
    if(numOfTanksDefender == 2){
        defenseDie3 = 0;
    }

    std::vector<std::int32_t> attackDice({attackDie1, attackDie2, attackDie3});
    std::vector<std::int32_t> defenseDice({defenseDie1, defenseDie2, defenseDie3});
    sort(attackDice.begin(), attackDice.end(), std::greater<int>());
    sort(defenseDice.begin(), defenseDice.end(), std::greater<int>());

    dice.push_back("crvena_kocka" + QString::number(attackDice[0]) + ".gif");
    dice.push_back("plava_kocka" + QString::number(defenseDice[0]) + ".gif");
    if(attackDice[1] > 0)
        dice.push_back("crvena_kocka" + QString::number(attackDice[1]) + ".gif");
    else
        dice.push_back("noDie.gif");
    if(defenseDice[1] > 0)
        dice.push_back("plava_kocka" + QString::number(defenseDice[1]) + ".gif");
    else
        dice.push_back("noDie.gif");
    if(attackDice[2] > 0)
        dice.push_back("crvena_kocka" + QString::number(attackDice[2]) + ".gif");
    else
        dice.push_back("noDie.gif");
    if(defenseDice[2] > 0)
        dice.push_back("plava_kocka" + QString::number(defenseDice[2]) + ".gif");
    else
        dice.push_back("noDie.gif");

    if(numOfTanksAttacker >= 2 && numOfTanksDefender >= 1) {
        if(attackDice[0] > defenseDice[0])
            defender->decreaseNumOfTanks(1);
        else
            attacker->decreaseNumOfTanks(1);
    }
    if(numOfTanksAttacker >= 3 && numOfTanksDefender >= 2) {
        if(attackDice[1] > defenseDice[1])
            defender->decreaseNumOfTanks(1);
        else
            attacker->decreaseNumOfTanks(1);
    }
    if(numOfTanksAttacker >= 4 && numOfTanksDefender >= 3) {
        if(attackDice[2] > defenseDice[2])
            defender->decreaseNumOfTanks(1);
        else
            attacker->decreaseNumOfTanks(1);
    }
}


int Game::calculateBonusTanks(Player *p)
{
    std::int32_t numOfTanks = 0;
    numOfTanks += ceil(p->numOfTerritories() / 3.0);
    foreach(auto &c , m_continents) {
        if(c->CheckOwner(p))
            numOfTanks += c->numOfBonusTanks();
    }
    return numOfTanks;
}

void Game::moveBetweenTerritories(int32_t numOfTanks, Territory *transfering, Territory *transfered)
{
    transfering->decreaseNumOfTanks(numOfTanks);
    transfered->increaseNumOfTanks(numOfTanks);
}

QString Game::makeTerritoryList()
{
    QString list = "";
    foreach(auto &c , m_continents) {
        foreach(auto &t , c->territories()) {
            list.append(t->name() + " ");
            list.append(QString::number(t->numberOfTanks()) + " ");
            list.append(colorToStr(t->owner()->color()));
            list.append(" ");
        }
    }
    list = list.remove(list.size()-1);

    return list;
}

QString Game::makePlayerList()
{
    QString message;
    for(int32_t i = 0; i < m_settings->numOfPlayers(); i++){
        message += m_players[i]->name() + " " + colorToStr(m_players[i]->color()) + " ";
    }
    return message;
}

Game::Game(QSet<Card *> &cards, const QSet<Continent *> &continents, Settings *settings) :
    m_cards(cards),
    m_continents(continents),
    m_settings(settings)
{
    m_players = m_settings->players();
    m_currentPlayer = *(m_players.begin());
    m_tanksToAdd = m_settings->numOfPlayers() * m_settings->numOfStartingTanks();
}

Game::~Game()
{
    delete m_settings;
    for (auto it = m_cards.begin(); it != m_cards.end(); ++it) {
        delete *it;
    }
    for (auto it = m_continents.begin(); it != m_continents.end(); ++it) {
        delete *it;
    }
}
