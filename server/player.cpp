#include "player.h"

const QString colorToStr(Color c)
{
    switch (c) {
        case Color::BLUE:                 return "BLUE";
        case Color::RED:                  return "RED";
        case Color::GREEN:                return "GREEN";
        case Color::YELLOW:               return "YELLOW";
        case Color::BLACK:                return "BLACK";
        case Color::GRAY:                 return "GRAY";
        case Color::WHITE:                return "WHITE";
        default:
            break;
    }
    return "";
}

const QString &Player::name() const
{
    return m_name;
}

void Player::addTerritory(Territory *t)
{
    m_territories.insert(t);
}

void Player::removeTerritory(Territory *t)
{
    auto wanted = m_territories.find(t);
    m_territories.erase(wanted);
}

void Player::setName(const QString &newName)
{
    m_name = newName;
}

Color Player::color() const
{
    return m_color;
}

void Player::setColor(Color newColor)
{
    m_color = newColor;
}

std::int32_t Player::numOfTerritories() const
{
    return m_territories.size();
}

bool Player::ownerInd() const
{
    return m_ownerInd;
}

void Player::setOwnerInd(bool newOwnerInd)
{
    m_ownerInd = newOwnerInd;
}

Player* Player::destroyer() const
{
    return m_destroyer;
}

void Player::setDestroyer(Player* newDestroyer)
{
    m_destroyer = newDestroyer;
}

void Player::addCard(Card* newCard)
{
    m_cards.insert(newCard);
}

int Player::exchangeCards(Card *c1, Card *c2, Card *c3)
{
    int bonusTanks = 0;
    if(this->territories().find(c1->territory()) != this->territories().end())
        bonusTanks++;
    if(this->territories().find(c2->territory()) != this->territories().end())
        bonusTanks++;
    if(this->territories().find(c3->territory()) != this->territories().end())
        bonusTanks++;

    if(c1->cardType() == c2->cardType() && c1->cardType() == c3->cardType() && c1->cardType() == CardType::INFANTRY){
        bonusTanks += 4;
    }
    else if(c1->cardType() == c2->cardType() && c1->cardType() == c3->cardType() && c1->cardType() == CardType::CAVALRY){
        bonusTanks += 6;
    }
    else if(c1->cardType() == c2->cardType() && c1->cardType() == c3->cardType() && c1->cardType() == CardType::CANNON){
        bonusTanks += 8;
    }
    else if(c1->cardType() != c2->cardType() && c2->cardType() != c3->cardType() && c1->cardType() != c3->cardType()
       && c1->cardType() != CardType::JOKER && c2->cardType() != CardType::JOKER && c3->cardType() != CardType::JOKER){
        bonusTanks += 10;
    }
    else if((c1->cardType() == CardType::JOKER && c2->cardType() == c3->cardType()) ||
       (c2->cardType() == CardType::JOKER && c1->cardType() == c3->cardType()) ||
       (c3->cardType() == CardType::JOKER && c1->cardType() == c2->cardType())){
        bonusTanks += 12;
    }
    else
        return -1;

    return bonusTanks;
}

const QSet<Territory *> &Player::territories() const
{
    return m_territories;
}

void Player::setTerritories(const QSet<Territory *> &newTerritories)
{
    m_territories = newTerritories;
}

QSet<Card *> &Player::cards()
{
    return m_cards;
}

void Player::setCards(QSet<Card *> &newCards)
{
    m_cards = newCards;
}

MissionCard *Player::missionCard() const
{
    return m_missionCard;
}

void Player::setMissionCard(MissionCard *newMissionCard)
{
    m_missionCard = newMissionCard;
}

Player::Player(const QString &name, Color color, bool ownerInd) : m_name(name),
    m_color(color),
    m_ownerInd(ownerInd),
    m_cards(QSet<Card*>()),
    m_territories(QSet<Territory*>()),
    m_missionCard(nullptr)
{
}

Player::~Player()
{
//    if (m_missionCard != nullptr) {
//        delete m_missionCard;
//    }
    for (auto it = m_cards.begin(); it != m_cards.end(); ++it) {
        delete *it;
    }
}
