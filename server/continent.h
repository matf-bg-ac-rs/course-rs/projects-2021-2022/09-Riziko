#ifndef CONTINENT_H
#define CONTINENT_H
#include<QString>
#include<QSet>

#include "territory.h"
class Player;

class Continent
{
private:
    QString m_name;
    QSet<Territory*> m_territories;
    std::int32_t m_numOfBonusTanks;

public:
    bool CheckOwner(Player*);
    Continent();
    Continent(const QString &name, const QSet<Territory *> &territories, std::int32_t numOfBonusTanks);
    ~Continent();

    const QString &name() const;
    std::int32_t numOfBonusTanks() const;
    const QSet<Territory *> &territories() const;
};

#endif // CONTINENT_H
