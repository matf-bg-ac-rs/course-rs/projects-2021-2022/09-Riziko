#include "carddestroytroops.h"
#include "player.h"

CardDestroyTroops::CardDestroyTroops()
{

}

bool CardDestroyTroops::checkAssignment(Player *p) const
{
    if (m_enemy == p) {
        if (p->numOfTerritories() >= 24)
            return true;
    }
    else if (m_enemy->destroyer() == p)
        return true;

    return false;
}

Player *CardDestroyTroops::enemy() const
{
    return m_enemy;
}

CardDestroyTroops::CardDestroyTroops(const QString &path, Player *enemy) :
    MissionCard(path),
    m_enemy(enemy)
{}

CardDestroyTroops::~CardDestroyTroops(){}
