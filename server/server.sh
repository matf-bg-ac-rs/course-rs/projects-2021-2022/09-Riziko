#!/usr/bin/bash
rm -r build
mkdir build
cd build
qmake -makefile -o Makefile "CONFIG += core gui widgets network testlib" ../TcpServer.pro
make
