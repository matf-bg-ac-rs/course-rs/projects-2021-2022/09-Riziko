#ifndef TCPSERVER_H
#define TCPSERVER_H

#include <QWidget>
#include <QAbstractSocket>
#include <QMessageBox>
#include "game.h"
#include "initializer.h"
#include <algorithm>
#include <QtGlobal>
#include <QSignalSpy>
#include <QTest>

namespace Ui {
class TcpServer;
}

class QTcpServer;
class QTcpSocket;

class TcpServer : public QWidget
{
    Q_OBJECT

public:
    explicit TcpServer(QWidget *parent = 0);
    ~TcpServer();
signals:
    void attemptConnection(QTcpSocket *con);
    void attemptStartGame(QTcpSocket *con, int);
    void setupPlayer(QTcpSocket *con, const QString&, const QString&);
    void setupGame(int, const QString&, int);
    void addedTank(QTcpSocket *, const QString&);
    void tankAddedSuccessfully();
    void startAddingTanks();
    void mainWindowDrawn();
    void numOfTanksChanged();
    void attackFirstTerritory(QTcpSocket*, const QString&);
    void attackSecondTerritory(QTcpSocket*, const QString&);
    void drawnAfterAttack();
    void transferAfterAttack(QTcpSocket*, const QString&);
    void doneAttacking(QTcpSocket*);
    void transferFirst(QTcpSocket*, const QString&);
    void transferSecond(QTcpSocket*, const QString&);
    void transfer(QTcpSocket*, const QString&);
    void drawnAfterTransfer();
    void doneTransfering(QTcpSocket*);
    void cards(QTcpSocket*);
    void mission(QTcpSocket*);
    void diceDrawn();
    void exchangeInitialized(QTcpSocket*);
    void tryExchange(QTcpSocket*, const QStringList&);
    void winnerDrawn();

private slots:
    void newConnection();
    void removeConnection();
    void newMessage();
    void displayError(QAbstractSocket::SocketError);

    void disconnectClients_clicked();

private:
    Ui::TcpServer *ui;
    QTcpServer *m_server;

    enum class ClientType{
        ADMIN,
        CLIENT,
        CHECKEDCLIENT
    };

    bool allReady = false;
    bool wantToStart = false;
    int32_t m_numOfValidPlayers = 0;
    int32_t m_numberOfAddedTanks = 0;
    int32_t m_mainWindowsDrawn = 0;
    int32_t m_numOfTanksChangedCounter = 0;
    int32_t m_afterAttackDrawn = 0;
    int32_t m_afterTransferDrawn = 0;
    int32_t m_diceDrawn = 0;
    int32_t m_winnersDrawn = 0;

    QHash<QTcpSocket*, ClientType> m_clients;
    QHash<Player*, QTcpSocket*> m_playerToClient;
    QHash<QTcpSocket*, Player*> m_clientToPlayer;
    QVector<Player*> m_players;
    Game* m_game = nullptr;

    void parseMessage(const QString&, QTcpSocket*);
    void sendMessage(QTcpSocket*, const QString&);

    void reset();

};

#endif // TCPSERVER_H
