#ifndef TERRITORY_H
#define TERRITORY_H
#include <QString>
#include <QSet>
#include <iostream>
#include "settings.h"

class Player;

class Territory
{
private:
    QString m_name;
    std::int32_t m_numberOfTanks;
    QSet<Territory*> m_adjTerritories;
    Player* m_owner;

public:
    Territory();
    explicit Territory(const QString &name);
    ~Territory();

    void increaseNumOfTanks(int32_t num);
    bool decreaseNumOfTanks(int32_t num);

    const QString &name() const;
    std::int32_t numberOfTanks() const;
    const QSet<Territory *> &adjTerritories() const;

    void setNumberOfTanks(std::int32_t newNumberOfTanks);

    void setAdjTerritories(const QSet<Territory *> &newAdjTerritories);

    Player *owner() const;
    void setOwner(Player *newOwner);
};

#endif // TERRITORY_H
