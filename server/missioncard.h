#ifndef MISSIONCARD_H
#define MISSIONCARD_H

#include "continent.h"
#include <QLabel>

class MissionCard
{
public:
    MissionCard();
    MissionCard(const QString &);
    virtual bool checkAssignment(Player* p) const = 0;
    virtual ~MissionCard() { };

    const QString &path() const;

private:
    QString m_path;

};

#endif // MISSIONCARD_H
