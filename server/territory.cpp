#include "territory.h"
#include "player.h"

Player *Territory::owner() const
{
    return m_owner;
}

void Territory::setOwner(Player *newOwner)
{
    m_owner = newOwner;
}

Territory::Territory()
{

}

const QString &Territory::name() const
{
    return m_name;
}

std::int32_t Territory::numberOfTanks() const
{
    return m_numberOfTanks;
}

const QSet<Territory *> &Territory::adjTerritories() const
{
    return m_adjTerritories;
}

void Territory::setNumberOfTanks(std::int32_t newNumberOfTanks)
{
    m_numberOfTanks = newNumberOfTanks;
}

void Territory::setAdjTerritories(const QSet<Territory *> &newAdjTerritories)
{
    m_adjTerritories = newAdjTerritories;
}

Territory::Territory(const QString &name) : m_name(name),
    m_numberOfTanks(1), m_owner(nullptr)
{}

Territory::~Territory()
{
}

void Territory::increaseNumOfTanks(int32_t num)
{
    m_numberOfTanks += num;
}

bool Territory::decreaseNumOfTanks(int32_t num)
{
    if(num > m_numberOfTanks)
        return false;
    m_numberOfTanks -= num;
    return true;
}

