#ifndef GAME_H
#define GAME_H

#include<iostream>
#include<QVector>
#include<QSet>
#include<algorithm>
#include <QRandomGenerator>

#include "settings.h"
#include "continent.h"
#include "player.h"

class Card;

class Territory;


class Game
{
private:
    QVector<Player*> m_players;
    QSet<Card*> m_cards;
    QSet<Continent*> m_continents;
    Player* m_currentPlayer;
    Settings* m_settings;
    QString m_currentlyAttacking;
    QString m_currentlyAttacked;
    QString m_currentlyTransfering;
    QString m_currentlyTransfered;
    int m_tanksToAdd;
    bool m_startingPhase = true;
    bool m_territoryConquered = false;
    int m_numOfPlayersThatMoved = 0;


public:
    Game();
    Game(QSet<Card *> &cards, const QSet<Continent *> &continents, Settings *settings);
    ~Game();

    void start();
    bool checkGoal(Player*);
    int calculateBonusTanks(Player*);
    std::int32_t addTanksToTerritory(std::int32_t, Territory*);
    void attack(Territory*, Territory*, QStringList&);
    void moveBetweenTerritories(std::int32_t, Territory*, Territory*);
    QString makeTerritoryList();
    QString makePlayerList();

    const QVector<Player *> &players() const;
    QSet<Card *> &cards();
    const QSet<Continent *> &continents() const;
    Player *currentPlayer() const;
    void setCurrentPlayer(Player *newCurrentPlayer);
    Settings *settings() const;
    const QString &currentlyAttacking() const;
    void setCurrentlyAttacking(const QString &newCurrentlyAttacking);
    const QString &currentlyAttacked() const;
    void setCurrentlyAttacked(const QString &newCurrentlyAttacked);
    const QString &currentlyTransfering() const;
    void setCurrentlyTransfering(const QString &newCurrentlyTransfering);
    const QString &currentlyTransfered() const;
    void setCurrentlyTransfered(const QString &newCurrentlyTransfered);
    int tanksToAdd() const;
    void setTanksToAdd(int newTanksToAdd);
    bool startingPhase() const;
    void setStartingPhase(bool newStartingPhase);
    bool territoryConquered() const;
    void setTerritoryConquered(bool newTerritoryConquered);
    int numOfPlayersThatMoved() const;
    void setNumOfPlayersThatMoved(int newNumOfPlayersThatMoved);
};

#endif // GAME_H
