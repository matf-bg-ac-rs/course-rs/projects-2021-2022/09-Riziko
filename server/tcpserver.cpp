#include "tcpserver.h"
#include "ui_tcpserver.h"

#include <QtNetwork>
#include <QPlainTextEdit>
#include <iostream>
#include <random>

TcpServer::TcpServer(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TcpServer)
  , m_server(new QTcpServer(this))
{
    ui->setupUi(this);

    if (!m_server->listen(QHostAddress::LocalHost, 52693)) {
        ui->log->setPlainText("Failure while starting server: "+ m_server->errorString());
        return;
    }

    connect(ui->disconnectClients, &QPushButton::clicked, this, &TcpServer::disconnectClients_clicked);
    connect(m_server, &QTcpServer::newConnection, this, &TcpServer::newConnection);
    connect(this, &TcpServer::attemptConnection,this, [this](QTcpSocket *con){

        QString msg = QString("/NEWCONN %1").arg(m_clients.size());
        if(m_clients.size() > 5)
            msg = "/FULL";

        sendMessage(con, msg);
    });
    connect(this, &TcpServer::attemptStartGame, this, [this](QTcpSocket *con, int numOfPlayers){
        if(m_clients[con] != ClientType::ADMIN) {
            m_clients[con] = ClientType::CHECKEDCLIENT;
            int i = 1;
            if(!wantToStart)
                i = 0;
            foreach(ClientType ct, m_clients) {
                if(ct == ClientType::CLIENT)
                    break;
                i++;
            }
            if(m_clients.size() <= i)
                allReady = true;

        }
        else if(numOfPlayers != m_clients.size()) {
            sendMessage(con, "/BADNUMOFPLAYERS");
        }
        else
            wantToStart = true;

        foreach(ClientType ct, m_clients) {
            if(ct == ClientType::CLIENT) {
                sendMessage(con, "/NOTREADY");
                return;
            }
        }

        if(wantToStart && allReady) {
            foreach(QTcpSocket* con, m_clients.keys()) {
                sendMessage(con, "/PREFERENCES");
            }
        }

    });
    connect(this, &TcpServer::setupPlayer,this, [this](QTcpSocket *con, const QString &name, const QString &color){
        bool validDetails = true;
        Color c = Color::BLACK;
        if(color == "BLUE")
            c = Color::BLUE;
        else if(color == "RED")
            c = Color::RED;
        else if(color == "GREEN")
            c = Color::GREEN;
        else if(color == "YELLOW")
            c = Color::YELLOW;
        else if(color == "GRAY")
            c = Color::GRAY;
        else if(color == "WHITE")
            c = Color::WHITE;

        foreach(auto player, m_players) {
            if(player->name() == name) {
                validDetails = false;
                sendMessage(con, "/NAMETAKEN");
                break;
            }
            if(player->color() == c) {
                validDetails = false;
                sendMessage(con, "/COLORTAKEN");
                break;
            }
        }

        bool isOwner = false;
        if(m_clients[con] == ClientType::ADMIN)
            isOwner = true;

        if(validDetails) {
            Player* p = new Player(name, c, isOwner);
            m_players.push_back(p);
            m_playerToClient[p] = con;
            m_clientToPlayer[con] = p;
            m_numOfValidPlayers++;
        }
        if(m_numOfValidPlayers == m_clients.size()) {
            foreach(QTcpSocket* c, m_clients.keys()) {
                if(m_clients[c] == ClientType::ADMIN)
                    sendMessage(c, "/SETTINGS");
            }
        }
    });

    connect(this, &TcpServer::setupGame,this, [this](int numOfPlayers, const QString &gameGoal, int numOfTanks){
        Goal gg = Goal::WORLD_DOMINATION;
        if(gameGoal == "CARDS")
            gg = Goal::CARDS;
        Settings* s = new Settings(numOfPlayers, numOfTanks, gg, m_players);
        auto continents = Initializer::initializeContinents();
        auto cards = Initializer::initializeCards();
        if (s->goal() == Goal::CARDS) {
            auto mCards = Initializer::initializeMissionCards(m_players);
            foreach(Player* p, m_players) {
                std::mt19937 rng(time(0));

                std::uniform_int_distribution<unsigned> d(0, mCards.size() - 1);

                unsigned r = d(rng);

                auto it = mCards.begin();
                for(; it != mCards.end() && r > 0; ++it, r--);

                p->setMissionCard(*it);
                mCards.remove(*it);
            }
        }

        m_game = new Game(cards, continents, s);
        m_game->start();

        foreach(auto conn, m_clients.keys())
            if(gg == Goal::CARDS)
                sendMessage(conn, "/DRAWMAINWINDOW MISSION " + QString::number(s->numOfPlayers()) + " " + m_game->makePlayerList() + m_game->makeTerritoryList());
            else
                sendMessage(conn, "/DRAWMAINWINDOW WORLD " + QString::number(s->numOfPlayers()) + " " + m_game->makePlayerList() + m_game->makeTerritoryList());
    });

    connect(this, &TcpServer::mainWindowDrawn, this, [this](){
        m_mainWindowsDrawn += 1;
        if(m_mainWindowsDrawn == m_game->settings()->numOfPlayers()){
            auto t = m_playerToClient[m_game->currentPlayer()];

            sendMessage(t, "/ADDTANK " + QString::number(m_game->settings()->numOfStartingTanks()) + " " + m_game->currentPlayer()->name());
            foreach(auto &con, m_clients.keys()) {
                if(con != t) {
                    sendMessage(con, "/MOVING " + m_game->currentPlayer()->name());
                }
            }
        }
    });

    connect(this, &TcpServer::drawnAfterAttack, this, [this](){
        m_afterAttackDrawn += 1;
        if(m_afterAttackDrawn == m_game->settings()->numOfPlayers()){

            m_game->setTanksToAdd(0);

            auto t = m_playerToClient[m_game->currentPlayer()];

            sendMessage(t, "/ATTACKFIRST " + m_game->currentPlayer()->name());
            foreach(auto &con, m_clients.keys()) {
                if(con != t) {
                    sendMessage(con, "/MOVING " + m_game->currentPlayer()->name());
                }
            }
            m_afterAttackDrawn = 0;
        }
    });

    connect(this, &TcpServer::numOfTanksChanged, this, [this](){
        m_numOfTanksChangedCounter += 1;
        if(m_numOfTanksChangedCounter == m_game->settings()->numOfPlayers()){
            if(m_game->tanksToAdd() == m_numberOfAddedTanks){

                auto t = m_playerToClient[m_game->currentPlayer()];

                sendMessage(t, "/ATTACKFIRST " + m_game->currentPlayer()->name());
                foreach(auto &con, m_clients.keys()) {
                    if(con != t) {
                        sendMessage(con, "/MOVING " + m_game->currentPlayer()->name());
                    }
                }

                m_numberOfAddedTanks = 0;
                m_numOfTanksChangedCounter = 0;
            }
            else{
                auto t = m_playerToClient[m_game->currentPlayer()];
                int tanksLeft;
                if(m_game->startingPhase())
                    tanksLeft = m_game->settings()->numOfStartingTanks() - (m_numberOfAddedTanks / m_game->settings()->numOfPlayers());
                else
                    tanksLeft = m_game->tanksToAdd() - m_numberOfAddedTanks;
                sendMessage(t, "/ADDTANK " + QString::number(tanksLeft) + " " + m_game->currentPlayer()->name());
                foreach(auto &con, m_clients.keys()) {
                    if(con != t) {
                        sendMessage(con, "/MOVING " + m_game->currentPlayer()->name());
                    }
                }
                m_numOfTanksChangedCounter = 0;
            }
        }

    });


    connect(this,&TcpServer::addedTank, this, [this](QTcpSocket* con, const QString &territory){
        auto t = Initializer::allTerritories[territory];
        if (m_clientToPlayer[con] != m_game->currentPlayer()) {
            sendMessage(con,"/NOTYOURMOVE");
        }
        else if(m_game->currentPlayer() != t->owner()) {
            sendMessage(con,"/WRONGTERRITORY");
        }
        else {
            m_game->addTanksToTerritory(1,t);
            foreach(QTcpSocket *c, m_clients.keys())
                sendMessage(c,"/ADDED " + territory);

            if(m_game->startingPhase()){
                auto it = std::find(m_players.begin(),m_players.end(),m_game->currentPlayer());
                if (it != m_players.end() - 1) {
                    it = it + 1;
                    m_game->setCurrentPlayer(*it);
                }
                else {
                    it = m_players.begin();
                    m_game->setCurrentPlayer(*it);
                }
            }

            m_numberOfAddedTanks += 1;

        }
     });
    connect(this,&TcpServer::attackFirstTerritory, this, [this](QTcpSocket* con, const QString &territory){
        auto t = Initializer::allTerritories[territory];
        if (m_clientToPlayer[con] != m_game->currentPlayer()) {
            sendMessage(con,"/NOTYOURMOVE");
        }
        else if(m_game->currentPlayer() != t->owner()) {
            sendMessage(con,"/WRONGTERRITORY");
        }
        else if(t->numberOfTanks() == 1){
            sendMessage(con, "/NOTENOUGHTANKS");
        }
        else {
            m_game->setCurrentlyAttacking(territory);
            sendMessage(con, "/ATTACKSECOND");
        }
    });
    connect(this,&TcpServer::attackSecondTerritory, this, [this](QTcpSocket* con, const QString &territory){
        auto attacked = Initializer::allTerritories[territory];
        auto attacking = Initializer::allTerritories[m_game->currentlyAttacking()];
        if (m_clientToPlayer[con] != m_game->currentPlayer()) {
            sendMessage(con,"/NOTYOURMOVE");
        }
        else if(m_game->currentPlayer() == attacked->owner()) {
            sendMessage(con, "/ATTACKFIRST " + m_game->currentPlayer()->name());
            foreach(auto &conn, m_clients.keys()) {
                if(con != conn) {
                    sendMessage(conn, "/MOVING " + m_game->currentPlayer()->name());
                }
            }
        }
        else if(attacked->adjTerritories().find(attacking) == attacked->adjTerritories().end()){
            sendMessage(con,"/NOTADJACENT");
        }
        else {
            QStringList dice;
            m_game->setCurrentlyAttacked(territory);
            m_game->attack(attacking, attacked, dice);
            foreach(QTcpSocket *c, m_clients.keys())
                sendMessage(c, "/DICE " + dice.join(" "));


       }
    });

    connect(this, &TcpServer::transferAfterAttack, this, [this](QTcpSocket* con, const QString &numOfTanks){
        auto attacking = Initializer::allTerritories[m_game->currentlyAttacking()];
        auto attacked = Initializer::allTerritories[m_game->currentlyAttacked()];
        if(attacking->numberOfTanks() - numOfTanks.toInt() < 1 || numOfTanks.toInt() <= 0){
            sendMessage(con, "/HOWMANYAGAINATTACK");
        }
        else{
            m_game->moveBetweenTerritories(numOfTanks.toInt(), attacking, attacked);

            foreach(QTcpSocket *c, m_clients.keys())
                sendMessage(c, "/ATTACKFINISHED " + m_game->currentlyAttacking() + " "
                            + QString::number(attacking->numberOfTanks()) + " " + m_game->currentlyAttacked() + " "
                            + QString::number(attacked->numberOfTanks()) + " " + colorToStr(attacked->owner()->color()));
        }
    });

    connect(this, &TcpServer::doneAttacking, this, [this](QTcpSocket* con){
       if (m_clientToPlayer[con] != m_game->currentPlayer()) {
           sendMessage(con,"/NOTYOURMOVE");
       }
       sendMessage(con, "/TRANSFERFIRST");
    });

    connect(this,&TcpServer::transferFirst, this, [this](QTcpSocket* con, const QString &territory){
        auto t = Initializer::allTerritories[territory];
        if (m_clientToPlayer[con] != m_game->currentPlayer()) {
            sendMessage(con,"/NOTYOURMOVE");
        }
        else if(m_game->currentPlayer() != t->owner()) {
            sendMessage(con,"/WRONGTERRITORY");
        }
        else if(t->numberOfTanks() == 1){
            sendMessage(con, "/NOTENOUGHTANKS");
        }
        else {
            m_game->setCurrentlyTransfering(territory);
            sendMessage(con, "/TRANSFERSECOND");
        }
    });

    connect(this,&TcpServer::transferSecond, this, [this](QTcpSocket* con, const QString &territory){
        auto transfered = Initializer::allTerritories[territory];
        auto transfering = Initializer::allTerritories[m_game->currentlyTransfering()];
        if (m_clientToPlayer[con] != m_game->currentPlayer()) {
            sendMessage(con,"/NOTYOURMOVE");
        }
        else if(m_game->currentPlayer() != transfered->owner()) {
            sendMessage(con,"/WRONGTERRITORY");
        }
        else if(transfered->adjTerritories().find(transfering) == transfered->adjTerritories().end()){
            sendMessage(con,"/NOTADJACENT");
        }
        else {
            m_game->setCurrentlyTransfered(territory);
            sendMessage(con, "/HOWMANYTRANSFER");
        }
    });

    connect(this, &TcpServer::transfer, this, [this](QTcpSocket* con, const QString &numOfTanks){
        auto transfering = Initializer::allTerritories[m_game->currentlyTransfering()];
        auto transfered = Initializer::allTerritories[m_game->currentlyTransfered()];
        if(transfering->numberOfTanks() - numOfTanks.toInt() < 1 || numOfTanks.toInt() <= 0){
            sendMessage(con, "/HOWMANYAGAINTRANSFER");
        }
        else{
            m_game->moveBetweenTerritories(numOfTanks.toInt(), transfering, transfered);

            foreach(QTcpSocket *c, m_clients.keys())
                sendMessage(c, "/TRANSFERFINISHED " + m_game->currentlyTransfering() + " "
                            + QString::number(transfering->numberOfTanks()) + " " + m_game->currentlyTransfered() + " "
                            + QString::number(transfered->numberOfTanks()));
        }
    });

    connect(this, &TcpServer::drawnAfterTransfer, this, [this](){

        m_afterTransferDrawn += 1;
        if(m_afterTransferDrawn == m_game->settings()->numOfPlayers()){
            auto t = m_playerToClient[m_game->currentPlayer()];

            sendMessage(t, "/TRANSFERSECOND");
            m_afterTransferDrawn = 0;
        }


    });

    connect(this, &TcpServer::doneTransfering, this, [this](QTcpSocket* con){
        if (m_clientToPlayer[con] != m_game->currentPlayer()) {
            sendMessage(con,"/NOTYOURMOVE");
        }
        if(m_game->checkGoal(m_game->currentPlayer())){
            foreach(QTcpSocket *c, m_clients.keys())
                sendMessage(c, "/WINNER " + m_game->currentPlayer()->name());
        }
        else{
            if(m_game->startingPhase())
                m_game->setNumOfPlayersThatMoved(m_game->numOfPlayersThatMoved() + 1);

            if (m_game->numOfPlayersThatMoved() == m_game->settings()->numOfPlayers()) {
                m_game->setStartingPhase(false);
                m_game->setNumOfPlayersThatMoved(0);
            }

            if (m_game->territoryConquered() && !m_game->cards().empty()) {

                std::mt19937 rng(time(0));

                std::uniform_int_distribution<unsigned> d(0, m_game->cards().size() - 1);

                unsigned r = d(rng);

                auto it = m_game->cards().begin();
                for(; it != m_game->cards().end() && r > 0; ++it, r--);

                m_game->currentPlayer()->addCard(*it);
                m_game->cards().remove(*it);
                m_game->setTerritoryConquered(false);
            }
            auto it = std::find(m_players.begin(),m_players.end(),m_game->currentPlayer());
            if (it != m_players.end() - 1) {
                it = it + 1;
                m_game->setCurrentPlayer(*it);
            }
            else {
                it = m_players.begin();
                m_game->setCurrentPlayer(*it);
            }
            auto t = m_playerToClient[m_game->currentPlayer()];
            if (m_game->startingPhase()) {
                sendMessage(t, "/ATTACKFIRST " + m_game->currentPlayer()->name());
                foreach(auto &con, m_clients.keys()) {
                    if(con != t) {
                        sendMessage(con, "/MOVING " + m_game->currentPlayer()->name());
                    }
                }
            }
            else {
                m_game->setTanksToAdd(m_game->calculateBonusTanks(m_game->currentPlayer()));
                int tanksLeft = m_game->tanksToAdd() - m_numberOfAddedTanks;
                sendMessage(t, "/ADDTANK " + QString::number(tanksLeft) + " " + m_game->currentPlayer()->name());
                foreach(auto &con, m_clients.keys()) {
                    if(con != t) {
                        sendMessage(con, "/MOVING " + m_game->currentPlayer()->name());
                    }
                }
            }
        }
    });

    connect(this, &TcpServer::cards, this, [this](QTcpSocket* con){
        auto player = m_clientToPlayer[con];
        QString cards = "/CARDS";
        foreach(Card* c, player->cards()) {
           if(c->cardType() == CardType::JOKER) {
                cards += " ";
                cards += cardTypeToStr(c->cardType());
           }
           else {
               cards += " ";
               cards += c->territory()->name();
               cards += "-";
               cards += cardTypeToStr(c->cardType());
           }
        }
        sendMessage(con, cards);
    });

    connect(this, &TcpServer::mission, this, [this](QTcpSocket* con){
        auto player = m_clientToPlayer[con];
        QString mission = "/MISSION " + player->missionCard()->path();
        sendMessage(con, mission);
    });

    connect(this, &TcpServer::diceDrawn, this, [this](){
        m_diceDrawn++;
        if(m_diceDrawn == m_game->settings()->numOfPlayers()) {
            auto attacked = Initializer::allTerritories[m_game->currentlyAttacked()];
            auto attacking = Initializer::allTerritories[m_game->currentlyAttacking()];
            if(attacked->numberOfTanks() == 0) {
                m_game->setTerritoryConquered(true);
                if (attacked->owner()->numOfTerritories() == 1) {
                    attacked->owner()->setDestroyer(attacking->owner());
                }
                attacked->owner()->removeTerritory(attacked);
                attacked->setOwner(attacking->owner());
                attacked->owner()->addTerritory(attacked);
                sendMessage(m_playerToClient[m_game->currentPlayer()], "/HOWMANYATTACK");
            }
            else{
            foreach(QTcpSocket *c, m_clients.keys())
                sendMessage(c, "/ATTACKFINISHED " + m_game->currentlyAttacking() + " "
                            + QString::number(attacking->numberOfTanks()) + " " + m_game->currentlyAttacked() + " "
                            + QString::number(attacked->numberOfTanks()) + " " + colorToStr(attacked->owner()->color()));
            }
            m_diceDrawn = 0;
        }

    });

    connect(this, &TcpServer::exchangeInitialized, this, [this](QTcpSocket* con){
        auto player = m_clientToPlayer[con];
        QString cards = "/EXCHANGE";
        foreach(Card* c, player->cards()) {
           if(c->cardType() == CardType::JOKER) {
                cards += " ";
                cards += cardTypeToStr(c->cardType());
           }
           else {
               cards += " ";
               cards += c->territory()->name();
               cards += "-";
               cards += cardTypeToStr(c->cardType());
           }
        }
        sendMessage(con, cards);
    });

    connect(this, &TcpServer::tryExchange, this, [this](QTcpSocket* con, const QStringList &cards){
        if (m_clientToPlayer[con] != m_game->currentPlayer()) {
            sendMessage(con,"/NOTYOURMOVE");
        }
        else if(cards.size() != 4){
            sendMessage(con, "/WRONGNUMBEROFCARDS");
        }
        else{
            auto c1 = m_clientToPlayer[con]->cards().find(Initializer::allCards[cards[1]]);
            auto c2 = m_clientToPlayer[con]->cards().find(Initializer::allCards[cards[2]]);
            auto c3 = m_clientToPlayer[con]->cards().find(Initializer::allCards[cards[3]]);
            int bonusTanks = m_clientToPlayer[con]->exchangeCards(*c1, *c2, *c3);
            if(bonusTanks == -1)
                sendMessage(con, "/WRONGTYPEOFCARDS");
            else{
                m_game->setTanksToAdd(m_game->tanksToAdd() + bonusTanks);
                m_clientToPlayer[con]->cards().erase(c1);
                m_clientToPlayer[con]->cards().erase(c2);
                m_clientToPlayer[con]->cards().erase(c3);
                m_game->cards().insert(*c1);
                m_game->cards().insert(*c2);
                m_game->cards().insert(*c3);

                int tanksLeft = m_game->tanksToAdd() - m_numberOfAddedTanks;
                sendMessage(con, "/ADDTANK " + QString::number(tanksLeft) + " " + m_game->currentPlayer()->name());
                foreach(auto &conn, m_clients.keys()) {
                    if(con != conn) {
                        sendMessage(conn, "/MOVING " + m_game->currentPlayer()->name());
                    }
                }
            }
        }

    });

    connect(this, &TcpServer::winnerDrawn, this, [this](){
        m_winnersDrawn++;
        if(m_winnersDrawn == m_game->settings()->numOfPlayers()){
            for(int i = 0; i < m_players.size(); i++){
                sendMessage(m_playerToClient[m_players[i]], "/NEWCONN " + QString::number(i+1));
            }
            reset();
            m_winnersDrawn = 0;
        }
    });


    ui->address->setText(m_server->serverAddress().toString());
    ui->port->setText(QString::number(m_server->serverPort()));
}

TcpServer::~TcpServer()
{
    delete ui;
    delete m_server;
    if(m_game)
        delete m_game;

    m_clients.clear();
}

void TcpServer::newConnection()
{
    while (m_server->hasPendingConnections()) {
        QTcpSocket *con = m_server->nextPendingConnection();

        if(m_clients.size() == 0)
            m_clients[con] = ClientType::ADMIN;
        else
            m_clients[con] = ClientType::CLIENT;

        ui->disconnectClients->setEnabled(true);

        connect(con, &QTcpSocket::disconnected, this, &TcpServer::removeConnection);
        connect(con, &QTcpSocket::readyRead, this, &TcpServer::newMessage);

        ui->log->insertPlainText(QString("* New connection: %1, port %2\n")
                                 .arg(con->peerAddress().toString(), QString::number(con->peerPort())));

    }
}

void TcpServer::removeConnection()
{
    if (QTcpSocket *con = qobject_cast<QTcpSocket*>(sender())) {
        ui->log->insertPlainText(QString("* Connection removed: %1, port %2\n")
                                 .arg(con->peerAddress().toString(), QString::number(con->peerPort())));


        if (m_clients[con] == ClientType::ADMIN && m_clients.size() > 1) {
            m_clients.remove(con);
            auto it = m_clients.begin();
            m_clients[it.key()] = ClientType::ADMIN;
        }
        else {
            m_clients.remove(con);
        }

        con->deleteLater();
        ui->disconnectClients->setEnabled(!m_clients.isEmpty());
    }
}

void TcpServer::newMessage()
{
    if (QTcpSocket *con = qobject_cast<QTcpSocket*>(sender())) {
        QByteArray buffer;

        QDataStream socketStream(con);
        socketStream.setVersion(QDataStream::Qt_5_12);

        socketStream.startTransaction();
        socketStream >> buffer;

        if(!socketStream.commitTransaction()){
            return;
        }

        QString message = QString("%1").arg(QString::fromStdString(buffer.toStdString()));

        ui->log->insertPlainText("Sending message: " + message + "\n");

        parseMessage(message, con);

    }
}

void TcpServer::disconnectClients_clicked()
{
    foreach (QTcpSocket *socket, m_clients.keys()) {
        socket->close();
    }
    ui->disconnectClients->setEnabled(false);
}

void TcpServer::parseMessage(const QString &message, QTcpSocket* con)
{
    auto split_msg = message.split(' ');
    auto command = split_msg[0];
    if(command == "/CONNECT"){
        emit attemptConnection(con);
    }
    else if (command == "/START"){
        emit attemptStartGame(con, split_msg[1].toInt());
    }
    else if (command == "/PREFERENCES"){
        emit setupPlayer(con, split_msg[1], split_msg[2]);
    }
    else if (command == "/SETTINGS"){
        emit setupGame(split_msg[1].toInt(), split_msg[2], split_msg[3].toInt());
    }
    else if (command == "/DRAWNMAINWINDOW"){
        emit mainWindowDrawn();
    }
    else if (command == "/CHANGEDNUMOFTANKS"){
        emit numOfTanksChanged();
    }
    else if (command == "/ADDED") {
        emit addedTank(con, split_msg[1]);
    }
    else if(command == "/ATTACKFIRST"){
        emit attackFirstTerritory(con, split_msg[1]);
    }
    else if(command == "/ATTACKSECOND"){
        emit attackSecondTerritory(con, split_msg[1]);
    }
    else if(command == "/DRAWNAFTERATTACK"){
        emit drawnAfterAttack();
    }
    else if(command == "/HOWMANYATTACK"){
        emit transferAfterAttack(con, split_msg[1]);
    }
    else if(command == "/DONEATTACKING"){
        emit doneAttacking(con);
    }
    else if(command == "/TRANSFERFIRST"){
        emit transferFirst(con, split_msg[1]);
    }
    else if(command == "/TRANSFERSECOND"){
        emit transferSecond(con, split_msg[1]);
    }
    else if(command == "/HOWMANYTRANSFER"){
        emit transfer(con, split_msg[1]);
    }
    else if(command == "/DRAWNAFTERTRANSFER"){
        emit drawnAfterTransfer();
    }
    else if(command == "/DONETRANSFERING"){
        emit doneTransfering(con);
    }
    else if (command == "/CARDS") {
        emit cards(con);
    }
    else if (command == "/MISSION") {
        emit mission(con);
    }
    else if (command == "/DICEDRAWN") {
        emit diceDrawn();
    }
    else if (command == "/EXCHANGE") {
        emit exchangeInitialized(con);
    }
    else if (command == "/EXCHANGECARDS") {
        emit tryExchange(con, split_msg);
    }
    else if (command == "/WINNERDRAWN") {
        emit winnerDrawn();
    }
}

void TcpServer::sendMessage(QTcpSocket *con, const QString &msg)
{
    QByteArray data;
    QDataStream stream(&data, QIODevice::WriteOnly);

    stream << (quint32)0;
    stream << msg.toUtf8();
    stream.device()->seek(0);
    stream << (quint32)data.size();

    con->write(data);
}

void TcpServer::reset()
{
    delete m_game;
    m_playerToClient.clear();
    m_clientToPlayer.clear();
    m_players.clear();

    allReady = false;
    wantToStart = false;
    m_numOfValidPlayers = 0;
    m_numberOfAddedTanks = 0;
    m_mainWindowsDrawn = 0;
    m_numOfTanksChangedCounter = 0;
    m_afterAttackDrawn = 0;
    m_afterTransferDrawn = 0;
    m_diceDrawn = 0;
    m_winnersDrawn = 0;

    foreach(auto &c, m_clients.keys())
        if(m_clients[c] == ClientType::CHECKEDCLIENT)
            m_clients[c] = ClientType::CLIENT;
}

void TcpServer::displayError(QAbstractSocket::SocketError socketError)
{
    switch (socketError) {
        case QAbstractSocket::RemoteHostClosedError:
        break;
        case QAbstractSocket::HostNotFoundError:
            QMessageBox::information(this, "QTCPServer", "The host was not found. Please check the host name and port settings.");
        break;
        case QAbstractSocket::ConnectionRefusedError:
            QMessageBox::information(this, "QTCPServer", "The connection was refused by the peer. Make sure QTCPServer is running, and check that the host name and port settings are correct.");
        break;
        default:
            QTcpSocket* socket = qobject_cast<QTcpSocket*>(sender());
            QMessageBox::information(this, "QTCPServer", QString("The following error occurred: %1.").arg(socket->errorString()));
        break;
    }
}
