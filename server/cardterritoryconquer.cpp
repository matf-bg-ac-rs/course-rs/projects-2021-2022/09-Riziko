#include "cardterritoryconquer.h"
#include "player.h"

std::int32_t CardTerritoryConquer::numberOfTerritories() const
{
    return m_numberOfTerritories;
}

CardTerritoryConquer::CardTerritoryConquer()
{

}

bool CardTerritoryConquer::checkAssignment(Player *p) const
{
    int territoryCounter = 0;
    if (m_numberOfTerritories == 18) {
        for (Territory* t : p->territories()) {
            if (t->numberOfTanks() >= 2)
                territoryCounter += 1;
        }
        if (territoryCounter >= 18) {
            return true;
        }
    }
    else if (p->numOfTerritories() >= m_numberOfTerritories) {
        return true;
    }
    return false;
}

CardTerritoryConquer::CardTerritoryConquer(const QString &path, int32_t numberOfTerritories) :
    MissionCard(path),
    m_numberOfTerritories(std::move(numberOfTerritories))
{}
