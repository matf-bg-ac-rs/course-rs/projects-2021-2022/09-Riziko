QT += core gui widgets network testlib

CONFIG += c++17

HEADERS  += card.h \
            cardcontinentconquer.h \
            carddestroytroops.h \
            cardterritoryconquer.h \
            continent.h \
            game.h \
            initializer.h \
            missioncard.h \
            player.h \
            settings.h \
            territory.h \
            tcpserver.h

SOURCES += card.cpp \
            cardcontinentconquer.cpp \
            carddestroytroops.cpp \
            cardterritoryconquer.cpp \
            continent.cpp \
            game.cpp \
            initializer.cpp \
            missioncard.cpp \
            player.cpp \
            main.cpp \
            settings.cpp \
            territory.cpp \
            tcpserver.cpp

FORMS    += tcpserver.ui

RESOURCES += ../res/txt.qrc \
            ../res/images.qrc \
            ../res/animations.qrc \
            ../res/fonts.qrc \
            ../res/sound.qrc
