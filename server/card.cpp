#include "card.h"

Card::Card()
{

}

const QString cardTypeToStr(CardType t)
{
    switch (t) {
        case CardType::CANNON:              return "cannon";
        case CardType::CAVALRY:             return "cavalry";
        case CardType::INFANTRY:            return "infantry";
        case CardType::JOKER:               return "joker1";
        default:
            break;
    }
    return "";
}

Territory *Card::territory() const
{
    return m_territory;
}

CardType Card::cardType() const
{
    return m_cardType;
}

Card::Card(Territory *territory, const CardType &cardType) : m_territory(territory),
    m_cardType(cardType)
{}

Card::~Card()
{
}

Card::Card(CardType cardType) : m_territory(nullptr), m_cardType(cardType)
{}
