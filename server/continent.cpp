#include "continent.h"

const QString &Continent::name() const
{
    return m_name;
}

std::int32_t Continent::numOfBonusTanks() const
{
    return m_numOfBonusTanks;
}

const QSet<Territory *> &Continent::territories() const
{
    return m_territories;
}

bool Continent::CheckOwner(Player *pl)
{
    foreach(auto &territory , m_territories){
        if(territory->owner() != pl)
            return false;
    }
    return true;
}

Continent::Continent()
{

}

Continent::Continent(const QString &name, const QSet<Territory *> &territories, int32_t numOfBonusTanks) : m_name(name),
    m_territories(territories),
    m_numOfBonusTanks(numOfBonusTanks)
{}

Continent::~Continent()
{
    for (auto it = m_territories.begin(); it != m_territories.end(); ++it) {
        delete *it;
    }
}
