#ifndef CARDCONTINENTCONQUER_H
#define CARDCONTINENTCONQUER_H

#include "missioncard.h"

class CardContinentConquer : public MissionCard
{
private:
    Continent *m_c1, *m_c2;
    CardContinentConquer(const CardContinentConquer&);
    CardContinentConquer& operator=(const CardContinentConquer&);
public:
    CardContinentConquer();
    CardContinentConquer(const QString&, Continent *c1, Continent *c2);
    bool checkAssignment(Player* p) const override;
    ~CardContinentConquer();

};

#endif // CARDCONTINENTCONQUER_H
