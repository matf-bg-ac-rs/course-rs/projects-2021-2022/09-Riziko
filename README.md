# Project Riziko

Igra riziko za vise igraca, preko online konekcije.

# Korišćene biblioteke
* [Qt 5.12.8](https://www.qt.io/)

## Build-ovanje server koda
1. `$ cd 09-Riziko/server/server.sh`
2. `$ cd build`
3. `$ ./TcpServer`

## Build-ovanje izvornog koda
1. `$ cd 09-Riziko/code/client.sh`
2. `$ cd build`
3. `$ ./Risk`

# Pokretanje igre
## Build-ovanjem izvornog koda
1. U Qt Creator-u otvoriti `09-Riziko/code/Risk.pro`.
2. Pritisnuti dugme **Run** u donjem levom uglu ili `Ctrl + R` na tastaturi.

## Pokretanjem release verzije
1. Preuzeti poslednju verziju igre sa sledećeg [linka](https://alas.matf.bg.ac.rs/~mi18169/) i pokrenuti izvršivi fajl.

# Pokretanje servera
## Build-ovanjem izvornog koda
1. U Qt Creator-u otvoriti `09-Riziko/server/TcpServer.pro`.
2. Pritisnuti dugme **Run** u donjem levom uglu ili `Ctrl + R` na tastaturi.

# Pravila igre
- Pravila igre Riziko se mogu pronaći na sledećoj [adresi](https://www.scribd.com/document/423114605/Riziko-pravila-igre). 

# Demo snimak
Na narednoj [adresi](https://www.youtube.com/watch?v=VGgZAijubGg) se nalazi YouTube snimak koji predstavlja demo snimak projekta.

# Licence za dodatne resurse
- Slike avatara su preuzete iz igre [Risk Online](https://store.steampowered.com/app/1128810/RISK_Global_Domination/) kao i animacija pri ulasku u partiju, ostale slike su autorske i ne podležu nikakvim licencama.
- Animacije bacanja kockice su ručno pravljene pomoću alata [Blender](https://www.blender.org/).
- Muzika je preuzeta sa sledećih linkova [mainmenu music](https://www.youtube.com/watch?v=vQyj6QDS-xc), [mainwindow music](https://youtu.be/UwxatzcYf9Q) .

## Developers

- [Miljan Bakic, 240/2018](https://gitlab.com/miljan_b)
- [Matija Lojovic, 45/2018](https://gitlab.com/Lojovic)
- [Nikola Subotic, 169/2018](https://gitlab.com/bob9952)
- [Nemanja Jankovic, 213/2018](https://gitlab.com/bambino325)
