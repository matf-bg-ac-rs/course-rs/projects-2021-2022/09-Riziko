#/usr/bin/bash
rm -r build
mkdir build
cd build
qmake -makefile -o Makefile ../Risk.pro
make -j$(nproc)
