#include <QApplication>
#include "gui/tcpclient.h"

int main(int argc, char **argv)
{
    QApplication a(argc, argv);
    a.setQuitOnLastWindowClosed(false);

    TcpClient tc;

    QObject::connect(&tc, &TcpClient::exited, &a, &QApplication::quit);
    tc.show();

    return a.exec();
}
