#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    m_gamePlayList = new QMediaPlaylist();
    m_gameMusic = new QMediaPlayer();

    m_gamePlayList->addMedia(QUrl("qrc:/resources/sound/main_window_music.mp3"));
    m_gamePlayList->setPlaybackMode(QMediaPlaylist::CurrentItemInLoop);
    m_gameMusic->setPlaylist(m_gamePlayList);
    m_gameMusic->setVolume(10);

    QImage defaultCursorImage(":/resources/images/Cursors/default.png");
    defaultCursorImage = defaultCursorImage.scaled(32,32,Qt::KeepAspectRatio,Qt::SmoothTransformation);
    QCursor cursor(QPixmap::fromImage(defaultCursorImage), 0, 0);
    setCursor(cursor);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete m_gameMusic;
    delete m_gamePlayList;
}

void MainWindow::hide() {
    sleep(1);
    ui->label_2->hide();
    delete animation;
}

Ui::MainWindow *MainWindow::getUi() const
{
    return ui;
}

void MainWindow::startAnimation() {
    animation = new QPropertyAnimation(ui->label_2,"geometry");
    animation->setDuration(4000);
    animation->setStartValue(QRect(250, 0, 300, 100));
    animation->setEndValue(QRect(250, 250, 300, 100));
    animation->start();
    connect(animation, &QPropertyAnimation::finished, this, &MainWindow::hide);
}

void MainWindow::closeEvent(QCloseEvent *event){
    QWidget::closeEvent(event);
    emit exited();
}

QMediaPlayer *MainWindow::gameMusic() const
{
    return m_gameMusic;
}
