#include "tcpclient.h"
#include "ui_tcpclient.h"
#include "ui_mainmenu.h"


TcpClient::TcpClient(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TcpClient)
  , m_socket(new QTcpSocket(this)) 
{
    ui->setupUi(this);
    m_menu = new MainMenu(this);
    m_menu->setAttribute(Qt::WA_DeleteOnClose);

    m_window = new MainWindow(this);
    m_window->setAttribute(Qt::WA_DeleteOnClose);

    ui->address->setText(QHostAddress(QHostAddress::LocalHost).toString());
    ui->port->setValue(52693);

    m_signalMapper = new QSignalMapper(this);
    connect(m_signalMapper, SIGNAL(mapped(QString)), this, SIGNAL(digitClicked(QString)));

    auto buttons = m_window->centralWidget()->findChildren<QPushButton*>();
    buttons.removeOne(m_window->getUi()->pb_finish);
    buttons.removeOne(m_window->getUi()->pb_cards);
    buttons.removeOne(m_window->getUi()->pb_missionCard);
    for (int i = 0; i < buttons.size(); ++i) {
        m_signalMapper->setMapping(buttons[i], buttons[i]->objectName());
        connect(buttons[i], SIGNAL(clicked()), m_signalMapper, SLOT(map()));
    }

    connect(m_menu, &MainMenu::exited, this, &QWidget::close);
    connect(m_window, &MainWindow::exited, this, &QDialog::close);
    connect(m_socket, &QTcpSocket::readyRead, this, &TcpClient::readMessage);
    connect(m_socket, &QTcpSocket::connected, this, &TcpClient::connectedToServer);
    connect(m_socket, &QTcpSocket::disconnected, this, &TcpClient::disconnectByServer);
    connect(this, &TcpClient::showMainMenu, this, [this](int numOfPlayers) {
        m_window->show();
        m_window->setVisible(false);
        this->hide();

        m_menu->introMusic()->play();

        m_menu->getUi()->pb_startGame->setStyleSheet("border: 2px solid rgb(78, 154, 6);\
                                                       border-radius: 15px;\
                                                       background-color: rgba(78, 154, 6, 0.7);");
        if (m_menu->getUi()->pb_startGame->text() != "Start game") {
             m_menu->getUi()->pb_startGame->setText("Check in");
        }
        m_menu->getUi()->pb_startGame->setDisabled(false);

        if(numOfPlayers != 1) {
            m_menu->getUi()->cbox_numberOfPlayers->hide();
            m_menu->getUi()->l_numberOfPlayers->hide();
            m_menu->getUi()->cbox_gameGoal->hide();
            m_menu->getUi()->l_gameGoal->hide();
            m_menu->getUi()->le_numberOfTanks->hide();
            m_menu->getUi()->l_numberOfTanks->hide();
            m_menu->getUi()->pb_startGame->setText("Check in");
        }
        else{
            m_menu->getUi()->cbox_numberOfPlayers->show();
            m_menu->getUi()->l_numberOfPlayers->show();
            m_menu->getUi()->cbox_gameGoal->show();
            m_menu->getUi()->l_gameGoal->show();
            m_menu->getUi()->le_numberOfTanks->show();
            m_menu->getUi()->l_numberOfTanks->show();
            m_menu->getUi()->pb_startGame->setText("Start game");
        }
        m_menu->show();
    });
    connect(this, &TcpClient::connection, this, &TcpClient::sendMessage);
    connect(this, &TcpClient::start, this, &TcpClient::sendMessage);

    connect(m_menu->getUi()->pb_startGame, &QPushButton::clicked, this, [this] () {
        if (m_menu->getUi()->pb_startGame->text() == "Check in") {
            m_menu->getUi()->pb_startGame->setStyleSheet("border: 2px solid rgb(78, 154, 6);\
                                                          border-radius: 15px;\
                                                          background-color: rgba(255,0,0,0.5);");
            m_menu->getUi()->pb_startGame->setText("Checked in");
            m_menu->getUi()->pb_startGame->setDisabled(true);
        }
        emit start("/START " + m_menu->getUi()->cbox_numberOfPlayers->currentText());
    });

    connect(this, &TcpClient::badNumOfPlayers, this, [](){
        QMessageBox::information(nullptr, tr("Warning: Bad number of players!"), tr("Wait for the other players to join!"));
    });
    connect(this, &TcpClient::notReady, this, [](){
       QMessageBox::information(nullptr, tr("Warning: Players not ready!"), tr("Wait for the other players to check in!"));
    });
    connect(this, &TcpClient::preferences, this, [this](){
        auto name = m_menu->getUi()->le_playName->text();
        auto color = m_menu->getUi()->cbox_color->currentText();

        sendMessage("/PREFERENCES " + name + " " + color);
    });
    connect(this, &TcpClient::settings, this, [this](){
        auto numOfPlayers = m_menu->getUi()->cbox_numberOfPlayers->currentText();
        auto gameGoal = m_menu->getUi()->cbox_gameGoal->currentText();
        auto numOfTanks = m_menu->getUi()->le_numberOfTanks->text();

        sendMessage("/SETTINGS " + numOfPlayers + " " + gameGoal + " " + numOfTanks);
    });

    connect(this, &TcpClient::showMainWindow, this, [this](QStringList list){
        m_menu->introMusic()->stop();
        m_window->gameMusic()->play();

        int32_t numOfPlayers = list[2].toInt();

        for(int32_t j = 3; j < 2*numOfPlayers+3; j += 2){
            QString player = "l_player" + QString::number(j/2);
            auto label = m_window->findChild<QLabel*>(player);
            label->setText(list[j]);
            label->setStyleSheet("QLabel{color: rgb(255, 255,255);font-size: 16px;}");

            QImage yourImage(":/resources/images/Avatars/" + list[j+1] + ".png");
            yourImage = yourImage.scaled(CARD_WIDTH,120,Qt::KeepAspectRatio,Qt::SmoothTransformation);
            QString hl_player = "hl_player" + QString::number(j/2);
            auto hl = m_window->findChild<QHBoxLayout*>(hl_player);
            QLabel *image = new QLabel(QString(""));

            QLayoutItem* item;
            while ((item = hl->takeAt(1)) != NULL)
            {
                delete item->widget();
                delete item;
            }

            hl->addWidget(image);
            image->setPixmap(QPixmap(QPixmap::fromImage(yourImage)));
        }


        for (int i = 3+2*numOfPlayers; i < list.size() - 2; i += 3) {
            QPushButton *button = m_window->findChild<QPushButton *>(list[i]);
            TerritoryButton* tb = new TerritoryButton(list[i+1].toInt(),list[i],strToColor(list[i+2]),button);
            m_territories.push_back(tb);
        }
        m_menu->hide();
        m_window->getUi()->pb_finish->hide();
        m_window->getUi()->pb_cards->setText("Cards");
        m_window->getUi()->pb_missionCard->setText("Mission");
        if(list[1] == "WORLD")
            m_window->getUi()->pb_missionCard->hide();
        m_window->show();
        m_window->startAnimation();

        sendMessage("/DRAWNMAINWINDOW");
    });

    connect(this, &TcpClient::showFullMessage, this, []() {
       QMessageBox::information(nullptr, tr("Full lobby"), tr("The lobby is currently full"));
    });
    connect(this, &TcpClient::nameTaken, this, [this]() {
       m_menu->getUi()->pb_startGame->setStyleSheet("border: 2px solid rgb(78, 154, 6);\
                                                      border-radius: 15px;\
                                                      background-color: rgba(78, 154, 6, 0.7);");
       if (m_menu->getUi()->pb_startGame->text() != "Start game") {
            m_menu->getUi()->pb_startGame->setText("Check in");
       }
       m_menu->getUi()->pb_startGame->setDisabled(false);

       disconnect(m_menu->getUi()->pb_startGame,&QPushButton::clicked,this,nullptr);
       connect(m_menu->getUi()->pb_startGame, &QPushButton::clicked, this, [this] () {
           m_menu->getUi()->pb_startGame->setDisabled(true);
           if (m_menu->getUi()->pb_startGame->text() == "Check in") {
               m_menu->getUi()->pb_startGame->setStyleSheet("border: 2px solid rgb(78, 154, 6);\
                                                             border-radius: 15px;\
                                                             background-color: rgba(255,0,0,0.5);");
               m_menu->getUi()->pb_startGame->setText("Checked in");
           }
           auto name = m_menu->getUi()->le_playName->text();
           auto color = m_menu->getUi()->cbox_color->currentText();
           sendMessage("/PREFERENCES " + name + " " + color);
       });

       QMessageBox::information(nullptr, tr("Name taken"), tr("Name is taken, select another name"));
    });
    connect(this, &TcpClient::colorTaken, this, [this]() {
       m_menu->getUi()->pb_startGame->setStyleSheet("border: 2px solid rgb(78, 154, 6);\
                                                      border-radius: 15px;\
                                                      background-color: rgba(78, 154, 6, 0.7);");
       if (m_menu->getUi()->pb_startGame->text() != "Start game") {
            m_menu->getUi()->pb_startGame->setText("Check in");
       }
       m_menu->getUi()->pb_startGame->setDisabled(false);

       disconnect(m_menu->getUi()->pb_startGame,&QPushButton::clicked,this,nullptr);
       connect(m_menu->getUi()->pb_startGame, &QPushButton::clicked, this, [this] () {
           m_menu->getUi()->pb_startGame->setDisabled(true);
           if (m_menu->getUi()->pb_startGame->text() == "Check in") {
               m_menu->getUi()->pb_startGame->setStyleSheet("border: 2px solid rgb(78, 154, 6);\
                                                             border-radius: 15px;\
                                                             background-color: rgba(255,0,0,0.5);");
               m_menu->getUi()->pb_startGame->setText("Checked in");
           }
           auto name = m_menu->getUi()->le_playName->text();
           auto color = m_menu->getUi()->cbox_color->currentText();

           sendMessage("/PREFERENCES " + name + " " + color);
       });

       QMessageBox::information(nullptr, tr("Color taken"), tr("Color is taken, select another color"));
    });

    connect(this, &TcpClient::digitClicked, this, [this](QString territory){
        switch(m_state){
        case State::WAITING:
            break;
        case State::ADDING:
            sendMessage("/ADDED " + territory);
            break;
        case State::ATTACKFIRST:
            sendMessage("/ATTACKFIRST " + territory);
            break;
        case State::ATTACKSECOND:
            sendMessage("/ATTACKSECOND " + territory);
            break;
        case State::TRANSFERFIRST:
            sendMessage("/TRANSFERFIRST " + territory);
            break;
        case State::TRANSFERSECOND:
            sendMessage("/TRANSFERSECOND " + territory);
            break;
        }
    });
    connect(this, &TcpClient::addTank, this, [this](QString tanksLeft){
        m_state = State::ADDING;
        m_window->getUi()->pb_finish->setText("Exchange cards");
        m_window->getUi()->le_info->setText("Add bonus tanks. You have " + tanksLeft + " left");
    });
    connect(this, &TcpClient::notYourMove, this, [this](){
       m_window->getUi()->le_info->setText("Its not your move");
    });
    connect(this, &TcpClient::wrongTerritory, this, [this](){
        m_window->getUi()->le_info->setText("Its not your territory");
    });
    connect(this, &TcpClient::notEnoughTanks, this, [this](){
        m_window->getUi()->le_info->setText("You need at least 2 tanks to attack");
    });
    connect(this, &TcpClient::notAdjacent, this, [this](){
        m_window->getUi()->le_info->setText("You need to select an adjacent territory");
    });
    connect(this, &TcpClient::addedTank, this, [this](QString territory){

        for(int i = 0; i < m_territories.size(); ++i) {
            if(m_territories[i]->name() == territory) {
                auto currNumOfTanks = m_territories[i]->numOfTanks();
                m_territories[i]->setNumOfTanks(currNumOfTanks + 1);
                m_territories[i]->pushButton()->setText(QString::number(currNumOfTanks + 1));              

                QPropertyAnimation *animation = new QPropertyAnimation(m_territories[i], "opacity");
                animation->setLoopCount(2);
                animation->setDuration(3000);

                animation->setKeyValueAt(0, 1);
                animation->setKeyValueAt(0.25, 0.2);
                animation->setKeyValueAt(0.5, 1);
                animation->setKeyValueAt(0.75, 0.2);
                animation->setKeyValueAt(1, 1);

                animation->start();
                connect(animation, &QPropertyAnimation::finished, animation, &QPropertyAnimation::deleteLater);
            }
        }

        m_state = State::WAITING;
        sendMessage("/CHANGEDNUMOFTANKS");
    });
    connect(this, &TcpClient::selectFirstTerritory, this, [this](){
        m_state = State::ATTACKFIRST;
        m_window->getUi()->le_info->setText("Select the territory from which to attack");
        m_window->getUi()->pb_finish->setText("Finish Attack");
        m_window->getUi()->pb_finish->show();
    });
    connect(this, &TcpClient::selectSecondTerritory, this, [this](){
        m_state = State::ATTACKSECOND;
        m_window->getUi()->le_info->setText("Select the territory to attack");
    });
    connect(this, &TcpClient::drawAfterAttack, this, [this](QString territory1, QString numOfTanks1, QString territory2, QString numOfTanks2, QString owner2){
        for(int i = 0; i < m_territories.size(); ++i) {
            if(m_territories[i]->name() == territory1) {
                m_territories[i]->setNumOfTanks(numOfTanks1.toInt());
                m_territories[i]->pushButton()->setText(numOfTanks1);
                QPropertyAnimation *animation = new QPropertyAnimation(m_territories[i], "opacity");
                animation->setLoopCount(2);
                animation->setDuration(3000);

                animation->setKeyValueAt(0, 1);
                animation->setKeyValueAt(0.25, 0.2);
                animation->setKeyValueAt(0.5, 1);
                animation->setKeyValueAt(0.75, 0.2);
                animation->setKeyValueAt(1, 1);

                animation->start();
                connect(animation, &QPropertyAnimation::finished, animation, &QPropertyAnimation::deleteLater);
            }
            if(m_territories[i]->name() == territory2) {
                m_territories[i]->setNumOfTanks(numOfTanks2.toInt());
                m_territories[i]->pushButton()->setText(numOfTanks2);
                m_territories[i]->setColor(strToColor(owner2));

                QPropertyAnimation *animation = new QPropertyAnimation(m_territories[i], "opacity");
                animation->setLoopCount(2);
                animation->setDuration(3000);

                animation->setKeyValueAt(0, 1);
                animation->setKeyValueAt(0.25, 0.2);
                animation->setKeyValueAt(0.5, 1);
                animation->setKeyValueAt(0.75, 0.2);
                animation->setKeyValueAt(1, 1);

                animation->start();
                connect(animation, &QPropertyAnimation::finished, animation, &QPropertyAnimation::deleteLater);
            }
        }

        m_state = State::WAITING;
        sendMessage("/DRAWNAFTERATTACK");
    });

    connect(this, &TcpClient::howManyTanksAttack, this, [this](){
        bool ok;
        QString text = QInputDialog::getText(m_window, tr("How many tanks"),
                                                 tr("How many tanks do you want to transfer"), QLineEdit::Normal,
                                                 "1", &ok);
            if (ok && !text.isEmpty())
                sendMessage("/HOWMANYATTACK " + text);
    });
    connect(this, &TcpClient::howManyTanksAgainAttack, this, [this](){
        m_window->getUi()->le_info->setText("Not a valid number of tanks");
        bool ok;
        QString text = QInputDialog::getText(m_window, tr("How many tanks"),
                                                 tr("How many tanks do you want to transfer"), QLineEdit::Normal,
                                                 "1", &ok);
            if (ok && !text.isEmpty())
                sendMessage("/HOWMANYATTACK " + text);
    });
    connect(m_window->getUi()->pb_finish, &QPushButton::clicked, this, [this](){
       switch(m_state){
       case State::ADDING:
           sendMessage("/EXCHANGE");
           break;
       case State::ATTACKFIRST:
           sendMessage("/DONEATTACKING");
           break;
       case State::ATTACKSECOND:
           sendMessage("/DONEATTACKING");
           break;
       case State::TRANSFERFIRST:
           m_state = State::ADDING;
           m_window->getUi()->pb_finish->setText("Exchange cards");
           m_window->getUi()->le_info->setText("Wait for your move");
           sendMessage("/DONETRANSFERING");
           break;
       case State::TRANSFERSECOND:
           m_state = State::ADDING;
           m_window->getUi()->pb_finish->setText("Exchange cards");
           m_window->getUi()->le_info->setText("Wait for your move");
           sendMessage("/DONETRANSFERING");
           break;
       default:
           return;
       }
    });
    connect(this, &TcpClient::transferFirst, this, [this](){
        m_window->getUi()->le_info->setText("Select the territory from witch to transfer");
        m_window->getUi()->pb_finish->setText("Finish Transfer");
        m_state = State::TRANSFERFIRST;
    });
    connect(this, &TcpClient::transferSecond, this, [this](){
        m_window->getUi()->le_info->setText("Select the territory to witch to transfer");
        m_state = State::TRANSFERSECOND;
    });

    connect(this, &TcpClient::howManyTanksTransfer, this, [this](){
        bool ok;
        QString text = QInputDialog::getText(m_window, tr("How many tanks"),
                                                 tr("How many tanks do you want to transfer"), QLineEdit::Normal,
                                                 "1", &ok);
            if (ok && !text.isEmpty())
                sendMessage("/HOWMANYTRANSFER " + text);
    });
    connect(this, &TcpClient::howManyTanksAgainTransfer, this, [this](){
        m_window->getUi()->le_info->setText("Not a valid number of tanks");
        bool ok;
        QString text = QInputDialog::getText(m_window, tr("How many tanks"),
                                                 tr("How many tanks do you want to transfer"), QLineEdit::Normal,
                                                 "1", &ok);
            if (ok && !text.isEmpty())
                sendMessage("/HOWMANYTRANSFER " + text);
    });

    connect(this, &TcpClient::drawAfterTransfer, this, [this](QString territory1, QString numOfTanks1, QString territory2, QString numOfTanks2){
        for(int i = 0; i < m_territories.size(); ++i) {
            if(m_territories[i]->name() == territory1) {
                m_territories[i]->setNumOfTanks(numOfTanks1.toInt());
                m_territories[i]->pushButton()->setText(numOfTanks1);
            }
            if(m_territories[i]->name() == territory2) {
                m_territories[i]->setNumOfTanks(numOfTanks2.toInt());
                m_territories[i]->pushButton()->setText(numOfTanks2);
            }
        }

        m_state = State::WAITING;
        sendMessage("/DRAWNAFTERTRANSFER");
    });

    connect(this, &TcpClient::won, this, [this](QString winner){
        QString msg = "The winner is " + winner;
        QMessageBox::information(m_window, tr("Game over"), tr(msg.toStdString().c_str()));
        m_window->gameMusic()->stop();

        sendMessage("/WINNERDRAWN");
    });

    connect(m_window->getUi()->pb_cards, &QPushButton::clicked, this, [this](){
        sendMessage("/CARDS");
    });

    connect(m_window->getUi()->pb_missionCard, &QPushButton::clicked, this, [this](){
        sendMessage("/MISSION");
    });

    connect(this, &TcpClient::drawCards, this, [](QStringList list){
        QWidget *wrapper = new QWidget();
        wrapper->setAttribute(Qt::WA_DeleteOnClose, true);

        wrapper->setBaseSize(CARD_WIDTH,CARD_HEIGHT);
        for (int i = 0; i < list.size()-1; i++) {
            QImage yourImage(":/resources/images/Cards/" + list[i+1] + ".png");
            yourImage = yourImage.scaled(CARD_WIDTH,CARD_HEIGHT,Qt::KeepAspectRatio,Qt::SmoothTransformation);
            QLabel *image = new QLabel(wrapper);

            image->setGeometry(QRect(i*CARD_WIDTH, 0, CARD_WIDTH, CARD_HEIGHT));
            image->setPixmap(QPixmap(QPixmap::fromImage(yourImage)));
        }
        wrapper->show();
    });

    connect(this, &TcpClient::drawMission, this, [](QString path){
        QWidget *wrapper = new QWidget();
        wrapper->setAttribute(Qt::WA_DeleteOnClose, true);
        wrapper->setBaseSize(CARD_WIDTH,CARD_HEIGHT);

        QImage yourImage(path + ".png");
        yourImage = yourImage.scaled(CARD_WIDTH,CARD_HEIGHT,Qt::KeepAspectRatio,Qt::SmoothTransformation);

        QLabel *image = new QLabel(wrapper);
        image->setGeometry(QRect(0, 0, CARD_WIDTH, CARD_HEIGHT));
        image->setPixmap(QPixmap(QPixmap::fromImage(yourImage)));

        wrapper->show();
    });
    connect(this, &TcpClient::drawDice, this, [this](QStringList list){
        QWidget *wrapper = new QWidget();
        wrapper->setAttribute(Qt::WA_DeleteOnClose, true);
        wrapper->setBaseSize(CARD_HEIGHT,CARD_WIDTH);

        QTimer* timer = new QTimer(this);
        timer->start(2900);

        QMovie *diceAnim = nullptr;
        for (int i = 0; i < list.size()-1; i++) {
            diceAnim = new QMovie(":/resources/animations/" + list[i+1]);
            if(!diceAnim->isValid())
                qDebug() << "Ova animacija nije dobra";
            diceAnim->setScaledSize(QSize(CARD_HEIGHT,CARD_WIDTH));
            QLabel *image = new QLabel(wrapper);
            image->setGeometry(QRect((i%2)*CARD_HEIGHT, (i/2)*CARD_WIDTH, CARD_HEIGHT, CARD_WIDTH));
            image->setMovie(diceAnim);
            image->setScaledContents(true);
            diceAnim->start();
        }
        connect(timer, &QTimer::timeout, this, [this, diceAnim, wrapper, timer](){
            diceAnim->stop();
            sleep(2);
            wrapper->close();
            delete timer;
            delete diceAnim;

            sendMessage("/DICEDRAWN");
        });
        wrapper->show();
    });

    connect(this, &TcpClient::drawExchange, this, [this](QStringList list){
        QWidget *wrapper = new QWidget();
        wrapper->setAttribute(Qt::WA_DeleteOnClose, true);

        wrapper->setMinimumSize(CARD_WIDTH,2*CARD_HEIGHT-100);

        QGroupBox *gb = new QGroupBox(wrapper);
        for (int i = 0; i < list.size()-1; i++) {
            QImage yourImage(":/resources/images/Cards/" + list[i+1] + ".png");
            yourImage = yourImage.scaled(CARD_WIDTH,CARD_HEIGHT,Qt::KeepAspectRatio,Qt::SmoothTransformation);
            QLabel *image = new QLabel(wrapper);
            image->setGeometry(QRect(i*CARD_WIDTH, 0, CARD_WIDTH, CARD_HEIGHT));
            image->setPixmap(QPixmap(QPixmap::fromImage(yourImage)));

            QCheckBox *cb = new QCheckBox(gb);
            QString cardName = list[i+1].split('-')[0];

            cb->setObjectName(cardName);
            cb->setGeometry(QRect(i*CARD_WIDTH + 75, CARD_HEIGHT-10, 20, 20));
        }
        QPushButton *pb = new QPushButton(wrapper);
        pb->setText("Exchange");
        int x;
        if(list.size()%2)
            x = ((list.size()-1)/2)*CARD_WIDTH-40;
        else
            x = ((list.size()-1)/2)*CARD_WIDTH+35;
        pb->setGeometry(QRect(x, 300, 100, 50));
        connect(pb, &QPushButton::clicked, [this, gb, wrapper](){
            auto checkBoxes = gb->findChildren<QCheckBox*>();
            QString msg = "/EXCHANGECARDS";
            foreach(QCheckBox* curr, checkBoxes){
                if(curr->isChecked())
                    msg += " " + curr->objectName();
            }

            sendMessage(msg);
            wrapper->close();
        });

        wrapper->show();
    });

    connect(this, &TcpClient::wrongNumberOfCards, this, [this](){
        m_window->getUi()->le_info->setText("Wrong number of cards");
    });

    connect(this, &TcpClient::wrongTypeOfCards, this, [this](){
        m_window->getUi()->le_info->setText("Wrong type of cards");
    });

    connect(this, &TcpClient::currentlyMoving, this, [this](QString name){
        auto labels = m_window->findChildren<QLabel*>();
        foreach(auto &l, labels) {
            if(l->text() == name) {
                 l->setStyleSheet("QLabel{color: rgb(0,255,0);font-size: 16px;}");
            }
            else {
                l->setStyleSheet("QLabel{color: rgb(255,255,255);font-size: 16px;}");
            }
        }
    });

    connect(this->ui->connect, &QPushButton::clicked, this, &TcpClient::connectClicked);
    connect(this->ui->disconnect, &QPushButton::clicked, this, &TcpClient::disconnectClicked);

}

TcpClient::~TcpClient()
{
    delete ui;
    delete m_socket;
    delete m_signalMapper;
    qDeleteAll(m_territories);
}


void TcpClient::sendMessage(const QString &text)
{

    if (text.isEmpty()
            || m_socket->state() != QAbstractSocket::ConnectedState)
        return;

    QDataStream socketStream(m_socket);
    socketStream.setVersion(QDataStream::Qt_5_12);

    QByteArray byteArray = text.toUtf8();

    socketStream << byteArray;
}



void TcpClient::readMessage()
{
        while (m_socket->bytesAvailable() < qint64(sizeof(quint32))) {
            if (!m_socket->waitForReadyRead()) {
                return;
            }
        }

        QDataStream stream(m_socket);
        quint32 msgSize = 0;
        stream >> msgSize;

        int length = qint64(msgSize - sizeof(quint32));
        while (m_socket->bytesAvailable() < length) {
            if (!m_socket->waitForReadyRead()) {
                return;
            }
        }

        QByteArray buffer;
        buffer.resize(length - sizeof(quint32));
        stream.skipRawData(sizeof(quint32));
        stream.readRawData(buffer.data(), length - sizeof(quint32));

        auto mess = QString::fromUtf8(buffer);
        qDebug() << mess;
        parseMessage(mess);

}


void TcpClient::connectClicked()
{

    if (m_socket->state() != QAbstractSocket::ConnectedState) {
        std::cout << "== Connecting.." << std::endl;
        m_socket->connectToHost(ui->address->text(), ui->port->value());

        updateGui(QAbstractSocket::ConnectingState);
    }
}

void TcpClient::connectedToServer()
{
    std::cout << "== Connected to server." << std::endl;
    emit connection("/CONNECT");

}

void TcpClient::disconnectClicked()
{
    if (m_socket->state() != QAbstractSocket::ConnectingState) {
        std::cout << "== Abort connecting." << std::endl;
    }
    m_socket->abort();
    updateGui(QAbstractSocket::UnconnectedState);
}

void TcpClient::disconnectByServer()
{
    std::cout << "== Disconnected by server." << std::endl;
    updateGui(QAbstractSocket::UnconnectedState);
}

void TcpClient::updateGui(QAbstractSocket::SocketState state)
{
    const bool unconnected = (state == QAbstractSocket::UnconnectedState);
    ui->connect->setEnabled(unconnected);
    ui->address->setEnabled(unconnected);
    ui->port->setEnabled(unconnected);

    ui->disconnect->setEnabled(!unconnected);
}

void TcpClient::parseMessage(const QString &message)
{
    auto split_msg = message.split(' ');
    auto command = split_msg[0];

    if(command == "/BADNUMOFPLAYERS"){
        emit badNumOfPlayers();
    }
    else if (command == "/NOTREADY"){
        emit notReady();
    }
    else if (command == "/SETTINGS"){
        emit settings();
    }
    else if (command == "/PREFERENCES"){
        emit preferences();
    }
    else if (command == "/NEWCONN"){
        emit showMainMenu(split_msg[1].toInt());
    }
    else if (command == "/DRAWMAINWINDOW"){
        emit showMainWindow(split_msg);
    }
    else if (command == "/FULL"){
        emit showFullMessage();
    }
    else if (command == "/NAMETAKEN") {
        emit nameTaken();
    }
    else if (command == "/COLORTAKEN") {
        emit colorTaken();
    }
    else if (command == "/ADDTANK") {
        emit addTank(split_msg[1]);
        emit currentlyMoving(split_msg[2]);
    }
    else if (command == "/NOTYOURMOVE") {
        emit notYourMove();
    }
    else if (command == "/WRONGTERRITORY") {
        emit wrongTerritory();
    }
    else if (command == "/ADDED") {
        emit addedTank(split_msg[1]);
    }
    else if (command == "/ATTACKFIRST") {
        emit selectFirstTerritory();
        emit currentlyMoving(split_msg[1]);
    }
    else if (command == "/NOTENOUGHTANKS") {
        emit notEnoughTanks();
    }
    else if (command == "/NOTADJACENT") {
        emit notAdjacent();
    }
    else if (command == "/ATTACKSECOND") {
        emit selectSecondTerritory();
    }
    else if (command == "/ATTACKFINISHED") {
        emit drawAfterAttack(split_msg[1], split_msg[2], split_msg[3], split_msg[4], split_msg[5]);
    }
    else if (command == "/HOWMANYATTACK"){
        emit howManyTanksAttack();
    }
    else if (command == "/HOWMANYAGAINATTACK"){
        emit howManyTanksAgainAttack();
    }
    else if (command == "/TRANSFERFIRST"){
        emit transferFirst();
    }
    else if (command == "/TRANSFERSECOND"){
        emit transferSecond();
    }
    else if (command == "/HOWMANYTRANSFER"){
        emit howManyTanksTransfer();
    }
    else if (command == "/HOWMANYAGAINTRANSFER"){
        emit howManyTanksAgainTransfer();
    }
    else if (command == "/TRANSFERFINISHED") {
        emit drawAfterTransfer(split_msg[1], split_msg[2], split_msg[3], split_msg[4]);
    }
    else if (command == "/WINNER") {
        emit won(split_msg[1]);
    }
    else if (command == "/CARDS") {
        emit drawCards(split_msg);
    }
    else if (command == "/MISSION") {
        emit drawMission(split_msg[1]);
    }
    else if (command == "/DICE") {
        emit drawDice(split_msg);
    }
    else if (command == "/EXCHANGE") {
        emit drawExchange(split_msg);
    }
    else if (command == "/WRONGNUMBEROFCARDS"){
        emit wrongNumberOfCards();
    }
    else if (command == "/WRONGTYPEOFCARDS"){
        emit wrongTypeOfCards();
    }
    else if (command == "/MOVING"){
        emit currentlyMoving(split_msg[1]);
    }

}

void TcpClient::closeEvent(QCloseEvent *event){
    QWidget::closeEvent(event);
    emit exited();
}


