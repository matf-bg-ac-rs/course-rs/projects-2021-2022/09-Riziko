#include "mainmenu.h"
#include "ui_mainmenu.h"

MainMenu::MainMenu(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MainMenu)
{
    ui->setupUi(this);

    m_introPlayList = new QMediaPlaylist();
    m_introMusic = new QMediaPlayer();

    m_introPlayList->addMedia(QUrl("qrc:/resources/sound/world_domination.mp3"));
    m_introPlayList->setPlaybackMode(QMediaPlaylist::CurrentItemInLoop);
    m_introMusic->setPlaylist(m_introPlayList);
    m_introMusic->setVolume(10);

    QImage defaultCursorImage(":/resources/images/Cursors/default.png");
    defaultCursorImage = defaultCursorImage.scaled(32,32,Qt::KeepAspectRatio,Qt::SmoothTransformation);
    QCursor cursor(QPixmap::fromImage(defaultCursorImage), 0, 0);
    setCursor(cursor);

    ui->cbox_gameGoal->addItems({"WORLD_DOMINATION", "CARDS"});
    ui->cbox_color->addItems({"BLUE", "RED", "GREEN", "YELLOW", "BLACK", "GRAY", "WHITE"});
    ui->cbox_numberOfPlayers->addItems({"2", "3", "4", "5"});

    ui->le_numberOfTanks->setText("5");

    connect(ui->cbox_numberOfPlayers, QOverload<const QString &>::of(&QComboBox::currentIndexChanged),
        [=](const QString &index){
        QString s;
        QStringList list(QStringList() << "2" << "3" << "4" << "5");
        if(index == list[0]){
            s = "5";
        }else if(index == list[1]){
            s = "4";
        }else if(index == list[2]){
            s = "3";
        }else if(index == list[3]){
            s = "2";
        }
        ui->le_numberOfTanks->setText(s);
    });
}

MainMenu::~MainMenu()
{
    delete ui;
    delete m_introMusic;
    delete m_introPlayList;
}

Ui::MainMenu *MainMenu::getUi() const
{
    return ui;
}

void MainMenu::closeEvent(QCloseEvent *event){
    QWidget::closeEvent(event);
    emit exited();
}

QMediaPlayer *MainMenu::introMusic() const
{
    return m_introMusic;
}
