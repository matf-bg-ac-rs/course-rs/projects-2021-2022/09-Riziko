#ifndef MAINMENU_H
#define MAINMENU_H

#include <QDialog>
#include <QComboBox>
#include <QString>
#include <QStringList>
#include <QFontDatabase>
#include <QMessageBox>
#include <QCursor>
#include <QMediaPlaylist>
#include <QMediaPlayer>

#include "mainwindow.h"
#include "ui_mainwindow.h"

namespace Ui {
class MainMenu;
}

class MainMenu : public QDialog
{
    Q_OBJECT

public:
    explicit MainMenu(QWidget *parent = nullptr);
    ~MainMenu();

    Ui::MainMenu *getUi() const;
    QMediaPlayer *introMusic() const;

signals:
    void exited();
private:
    Ui::MainMenu *ui;
    void closeEvent(QCloseEvent*) override;

    QMediaPlayer *m_introMusic;
    QMediaPlaylist *m_introPlayList;
};

#endif // MAINMENU_H
