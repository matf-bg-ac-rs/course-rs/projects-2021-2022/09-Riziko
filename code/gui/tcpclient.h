#ifndef TCPCLIENT_H
#define TCPCLIENT_H

#include <QWidget>
#include <QAbstractSocket>
#include <iostream>
#include <QMessageBox>
#include <QMovie>
#include <QSize>
#include <QtWidgets>
#include <QtNetwork>
#include <QSignalMapper>

#include "gui/mainmenu.h"
#include "territorybutton.h"

class QTcpSocket;

namespace Ui {
class TcpClient;
}

enum class State{
    ADDING,
    ATTACKFIRST,
    ATTACKSECOND,
    TRANSFERFIRST,
    TRANSFERSECOND,
    WAITING
};

class TcpClient : public QWidget
{
    Q_OBJECT

public:
    explicit TcpClient(QWidget *parent = 0);
    ~TcpClient();

private slots:
    void sendMessage(const QString&);
    void readMessage();
    void connectClicked();
    void connectedToServer();
    void disconnectClicked();
    void disconnectByServer();

signals:
    void showMainMenu(int);
    void connection(const QString&);
    void start(const QString&);
    void badNumOfPlayers();
    void notReady();
    void settings();
    void preferences();
    void showMainWindow(const QStringList&);
    void showFullMessage();
    void nameTaken();
    void colorTaken();
    void addTank(const QString&);
    void digitClicked(const QString&);
    void notYourMove();
    void wrongTerritory();
    void addedTank(const QString&);
    void selectFirstTerritory();
    void notEnoughTanks();
    void selectSecondTerritory();
    void drawAfterAttack(const QString&, const QString&, const QString&, const QString&, const QString&);
    void notAdjacent();
    void howManyTanksAttack();
    void howManyTanksAgainAttack();
    void exited();
    void transferFirst();
    void transferSecond();
    void howManyTanksTransfer();
    void howManyTanksAgainTransfer();
    void drawAfterTransfer(const QString&, const QString&, const QString&, const QString&);
    void won(const QString&);
    void drawCards(const QStringList&);
    void drawMission(const QString&);
    void drawDice(const QStringList&);
    void drawExchange(const QStringList&);
    void wrongNumberOfCards();
    void wrongTypeOfCards();
    void currentlyMoving(const QString&);

private:
    MainMenu *m_menu = nullptr;
    MainWindow *m_window = nullptr;
    Ui::TcpClient *ui;
    QTcpSocket *m_socket;
    QSignalMapper *m_signalMapper;
    QString m_user;
    QString m_receivedData;
    QList<TerritoryButton*> m_territories;
    State m_state = State::ADDING;
    static const int32_t CARD_HEIGHT = 250;
    static const int32_t CARD_WIDTH = 150;


    void parseMessage(const QString&);
    void updateGui(QAbstractSocket::SocketState state);
    void closeEvent(QCloseEvent*) override;
};

#endif // TCPCLIENT_H
