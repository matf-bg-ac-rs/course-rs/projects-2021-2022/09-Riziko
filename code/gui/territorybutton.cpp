#include "territorybutton.h"

Color strToColor(QString color) {
    Color c = Color::BLACK;
    if(color == "BLUE")
        c = Color::BLUE;
    else if(color == "RED")
        c = Color::RED;
    else if(color == "GREEN")
        c = Color::GREEN;
    else if(color == "YELLOW")
        c = Color::YELLOW;
    else if(color == "GRAY")
        c = Color::GRAY;
    else if(color == "WHITE")
        c = Color::WHITE;

    return c;
}

const QString colorToStr(Color c)
{
    switch (c) {
        case Color::BLUE:                 return "rgba(0,0,255,";
        case Color::RED:                  return "rgba(255,0,0,";
        case Color::GREEN:                return "rgba(0,255,0,";
        case Color::YELLOW:               return "rgba(255,255,0,";
        case Color::BLACK:                return "rgba(0,0,0,";
        case Color::GRAY:                 return "rgba(180,180,180,";
        case Color::WHITE:                return "rgba(255,255,255,";
        default:
            break;
    }
    return "";
}

int32_t TerritoryButton::numOfTanks() const
{
    return m_numOfTanks;
}

void TerritoryButton::setNumOfTanks(int32_t newNumOfTanks)
{
    m_numOfTanks = newNumOfTanks;
}

const QString &TerritoryButton::name() const
{
    return m_name;
}

void TerritoryButton::setName(const QString &newName)
{
    m_name = newName;
}

Color TerritoryButton::color() const
{
    return m_color;
}

void TerritoryButton::setColor(Color newColor)
{
    m_color = newColor;
    m_pushButton->setStyleSheet("background-color: " + colorToStr(m_color) + "1);\
                                color: brown;\
                                border-style: solid;\
                                border-width:1px;\
                                border-radius:12px;\
                                max-width:25px;\
                                max-height:25px;\
                                min-width:25px;\
                                min-height:25px; ");
}

QPushButton *TerritoryButton::pushButton() const
{
    return m_pushButton;
}

double TerritoryButton::opacity() const
{
    return m_opacity;
}

void TerritoryButton::setOpacity(double newOpacity)
{
    m_opacity = newOpacity;
    m_pushButton->setStyleSheet("background-color: " + colorToStr(m_color) + QString::number(m_opacity) + ");\
                                color: brown;\
                                border-style: solid;\
                                border-width:1px;\
                                border-radius:12px;\
                                max-width:25px;\
                                max-height:25px;\
                                min-width:25px;\
                                min-height:25px;");
}

TerritoryButton::TerritoryButton()
{

}

TerritoryButton::TerritoryButton(int32_t numOfTanks, const QString &name, Color color, QPushButton *pushButton) : m_numOfTanks(std::move(numOfTanks)),
    m_name(name),
    m_color(color),
    m_pushButton(pushButton)
{
    m_pushButton->setText( QString::number(numOfTanks));
    m_pushButton->setStyleSheet("background-color: " + colorToStr(color) + "1);\
                                color: brown;\
                                border-style: solid;\
                                border-width:1px;\
                                border-radius:12px;\
                                max-width:25px;\
                                max-height:25px;\
                                min-width:25px;\
                                min-height:25px; ");
}

TerritoryButton::~TerritoryButton()
{

}
