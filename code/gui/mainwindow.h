#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPixmap>
#include <QPalette>
#include <QTimer>
#include <QLabel>
#include <QPropertyAnimation>
#include <QRect>
#include <iostream>
#include <unistd.h>
#include <QGuiApplication>
#include <QWindow>
#include <QList>
#include <QWindowList>
#include <QWidgetList>
#include <QMediaPlaylist>
#include <QMediaPlayer>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow() override;

    void setStartAnimation(bool newStartAnimation);
    void startAnimation();

    Ui::MainWindow *getUi() const;
    QMediaPlayer *gameMusic() const;

public slots:
    void hide();

signals:
    void exited();
private:
    Ui::MainWindow *ui;
    QPropertyAnimation *animation;
    void closeEvent(QCloseEvent*) override;
    QMediaPlayer *m_gameMusic;
    QMediaPlaylist *m_gamePlayList;
};
#endif // MAINWINDOW_H
