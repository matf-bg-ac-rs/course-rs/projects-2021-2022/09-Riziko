#ifndef TERRITORYBUTTON_H
#define TERRITORYBUTTON_H

#include <QObject>
#include <QWidget>
#include <QPushButton>
#include <QVariant>
#include <QDebug>

enum class Color{
    BLUE,
    RED,
    GREEN,
    YELLOW,
    BLACK,
    GRAY,
    WHITE
};

class TerritoryButton : public QWidget
{
    Q_OBJECT
    Q_PROPERTY(double opacity READ opacity WRITE setOpacity CONSTANT);
private:
    int32_t m_numOfTanks;
    QString m_name;
    Color m_color;
    QPushButton* m_pushButton;
    double m_opacity;

public:
    TerritoryButton();
    TerritoryButton(int32_t numOfTanks, const QString &name, Color color, QPushButton *pushButton);
    virtual ~TerritoryButton();

    int32_t numOfTanks() const;
    void setNumOfTanks(int32_t newNumOfTanks);
    const QString &name() const;
    void setName(const QString &newName);
    Color color() const;
    void setColor(Color newColor);
    QPushButton *pushButton() const;
    double opacity() const;
    void setOpacity(double newOpacity);
};

const QString colorToStr(Color c);
Color strToColor(QString);

#endif // TERRITORYBUTTON_H
