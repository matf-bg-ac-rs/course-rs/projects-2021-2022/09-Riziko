TEMPLATE = app
QT += core gui widgets network multimedia

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        gui/mainmenu.cpp \
        gui/territorybutton.cpp \
        main.cpp \
        gui/mainwindow.cpp \
        gui/tcpclient.cpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    gui/mainmenu.h \
    gui/mainwindow.h \
    gui/tcpclient.h \
    gui/territorybutton.h

FORMS += gui/mainwindow.ui \
         gui/mainmenu.ui   \
         gui/tcpclient.ui

RESOURCES += ../res/txt.qrc \
            ../res/images.qrc \
            ../res/animations.qrc \
            ../res/fonts.qrc \
            ../res/sound.qrc
